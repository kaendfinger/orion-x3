# Makefile for Orion-X3 and derivatives
# Written in 2011
# This makefile is licensed under the WTFPL -- Do Whatever the Fuck You Want Public License (:



X_BUILD_FILE      = "output/.bno.txt"

QEMU              = /usr/local/bin/qemu-system-x86_64
BOCHS             = /usr/local/bin/bochs
MKISOFS           = /usr/local/bin/mkisofs

CC                = LLVM/bin/clang
DC                = LLVM/bin/ldc2
CPP               = LLVM/bin/clang++
AS                = Cross64/bin/x86_64-elf-as
LD                = Cross64/bin/x86_64-elf-ld
OBJCOPY           = Cross64/bin/x86_64-elf-objcopy


CCFLAGS           = -m64 -g -Wall -integrated-as -O3 -fPIC -std=gnu11 -ffreestanding -mno-red-zone -fno-exceptions -Isrc/kernel/include -Isrc/ -target x86_64-elf -c

CXXFLAGS          = -m64 -g -Wall -integrated-as -O3 -fPIC -std=gnu++11 -ffreestanding -mno-red-zone -fno-exceptions -fno-rtti -Isrc/kernel/include -Isrc/ -target x86_64-elf -c

LDFLAGS           = -z max-page-size=0x1000 --defsym X_BUILD_NUMBER=$$(cat $(X_BUILD_FILE)) -T link.ld



MEMORY            = 512


SSRC              = $(shell find src -path "src/userspace" -prune -o -name "*.s")
CSRC              = $(shell find src -path "src/userspace" -prune -o -name "*.c")
CXXSRC            = $(shell find src -path "src/userspace" -prune -o -name "*.cpp")


SOBJ             := $(SSRC:.s=.s.o)
COBJ             := $(CSRC:.c=.c.o)
CXXOBJ           := $(CXXSRC:.cpp=.cpp.o)




OUTPUT            = output/x3.elf





all: $(OUTPUT)
	@$(QEMU) -vga std -serial file:"/Volumes/Data HD/serialout.log" -m $(MEMORY) -hda output/disk.img -hdb /Volumes/Data\ HD/data.img -hdc /Volumes/Data\ HD/LargeData.img


$(OUTPUT): rmobj $(SOBJ) $(COBJ) $(CXXOBJ) $(X_BUILD_FILE) builduserspace
	# Linking object files...
	@# TODO: Compile objects into a tree mirroring sources, so only need unique names *per folder* instead of... like everywhere.

	@$(LD) $(LDFLAGS) -o output/temp.elf $(shell find output/obj -name "*.o") -Lgcc



	# Performing objcopy...
	@$(OBJCOPY) -O elf32-i386 output/temp.elf output/x3.elf






	# Copying kernel to disk image...
	@test -d /Volumes/X3 || hdiutil mount -quiet output/disk.img
	@cp output/x3.elf /Volumes/X3/boot/
	@cp grub/menu.lst /Volumes/X3/boot/grub/



rmobj:
	-@rm output/obj/*

buildares:
	@make -C src/userspace/ares/

builduserspace:
	@make -C src/userspace/

%.s.o: %.s
	@echo $(notdir $<)
	@$(AS) $< -o output/obj/$(notdir $@)

%.c.o: %.c
	@echo $(notdir $<)
	@$(CC) $(CCFLAGS) -o output/obj/$(notdir $@) $<

%.cpp.o: %.cpp
	@echo $(notdir $<)
	@$(CXX) $(CXXFLAGS) -o output/obj/$(notdir $@) $<


$(X_BUILD_FILE):
	-@echo $$(($$(cat $(X_BUILD_FILE)) + 1)) > $(X_BUILD_FILE)

clean:
	-@rm -r output/obj/*
	-@rm output/.bno.txt
	-@rm output/*.elf
	-@rm output/*.iso

