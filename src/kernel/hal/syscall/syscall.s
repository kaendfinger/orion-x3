// SysCall.s
// Copyright (c) 2011-2013, <zhiayang@gmail.com>
// Licensed under Creative Commons Attribution 3.0 Unported.




.global HandleSyscall
.global HandleSyscallInstruction

.type HandleSyscall, @function
.type HandleSyscallInstruction, @function


.section .text

HandleSyscallInstruction:
	// this is responsible for handling the SYSCALL instruction.
	// firstly, since SYSCALL saves the return RIP to RCX, push.
	xchg %bx, %bx
	push %rcx

	// specifications are... about the same.
	// RCX is skipped.

	// so...
	/*
		%rdi
		%rsi
		%rdx
		%r8
		%r9
		--> stack


	*/

	pop %rcx
	sysret




HandleSyscall:
	/*
		Documentation:
		%rdi
		%rsi
		%rdx
		%rcx
		%r8
		%r9
		--> stack.


		SysCall number in %rdi
		Parameters in order of ABI.
	*/

	// Handle everything in ASM for now.



	// store the syscall number in %r10
	// shift everything forward by one.

	/* mov %rdi, %r10

	mov %rsi, %rdi
	mov %rdx, %rsi
	mov %rcx, %rdx
	mov %r8, %rcx
	mov %r9, %r8

	*/

	// multiply %r10 by 8
	salq $3, %r10
	movq SyscallTable(%r10), %r10
	jmp *%r10


CleanUp:
	xchg %bx, %bx
	iretq



PrintInt:
	call SC_PrintInt
	jmp CleanUp

PrintChar:
	call SC_PrintChar
	jmp CleanUp

Fail:
	jmp CleanUp



.section .data

.align 8
SyscallTable:
	.quad		Fail
	.quad		PrintInt
	.quad		PrintChar

EndSyscallTable:







