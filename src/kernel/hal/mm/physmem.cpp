// PhysMem.c
// Copyright (c) 2011-2013, <zhiayang@gmail.com>
// Licensed under Creative Commons Attribution 3.0 Unported.


// Implements functions to allocate physical memory.


#include <stdlib.h>

using namespace Kernel;
using namespace Library;
using namespace Library::Stdio;
using namespace Kernel::HAL::PMem;



extern uint64_t KernelEnd;
extern MemoryMap_type* K_MemoryMap;
uint64_t PMemManagerLocation;
bool DidInit = false;
TFPL *CurrentFPL;
uint32_t FPLBottomIndex;
uint32_t NumberOfFPLPairs;
bool HaltNextAlloc = false;


// Define a region of memory in which the VMM gets it's memory from, to create page strucures.
uint64_t ReservedRegionForVMM;
uint64_t LengthOfReservedRegion;
uint64_t ReservedRegionIndex = -0x1000;

uint64_t LastSuccessfulAlloc = 0;



uint64_t PageAlignDown(uint64_t x)
{
	return x & ~(0xFFF);
}
uint64_t PageAlignUp(uint64_t x)
{
	return PageAlignDown(x + 0xFFF);
}





namespace Kernel {
namespace HAL
{

	uint64_t AllocatePage()
	{
		// This one allocates AND maps pages.

		uint64_t p = PMem::AllocatePage_NoMap();
		HAL::VMem::MapAddr(p, p, 0x07);

		// Do us a favour and zero the memory first.
		Memory::Set((void*)p, 0x00, 0x1000);


		return p;
	}

	void FreePage(uint64_t PageAddr)
	{
		PMem::FreePage_NoUnmap(PageAddr);

		// FIXME: This assumes that the memory to free is 1-1 mapped.
		HAL::VMem::UnMapAddr(PageAddr);
	}




namespace PMem
{
	void Initialise()
	{
		if(DidInit)
		{
			HALT("Tried to initialise PMem Manager twice!");
		}

		// See HAL_InitialiseFPLs() for an in-depth explanation on this
		// FPL system.

		ReservedRegionForVMM = PageAlignUp((uint64_t)&KernelEnd);
		LengthOfReservedRegion = 0x00300000;


		PMemManagerLocation = ReservedRegionForVMM + LengthOfReservedRegion;

		InitialiseFPLs(K_MemoryMap);

		// PMemManagerLocation = PageAlignUp((uint64_t)&KernelEnd);

		DidInit = true;


	}

	uint64_t AllocateFromReserved()
	{
		// we need this 'reserved' region especially to store page tables/directories etc.
		// if not, we'll end up with a super-screwed mapping.
		// possibly some overlap as well.

		ReservedRegionIndex += 0x1000;
		memset((void*)(ReservedRegionForVMM + ReservedRegionIndex), 0x00, 0x1000);
		return ReservedRegionForVMM + ReservedRegionIndex;
	}

	bool IsOutOfMemory()
	{
		return HaltNextAlloc;
	}


	uint64_t AllocatePage_NoMap()
	{
		// If we let the PIT interrupt us during this time, we will page fault.
		asm volatile("cli");


		// This one only allocates pages, but doesn't map them.

		if(!DidInit)
		{
			return AllocateViaPlacement();
		}

		if(HaltNextAlloc)
		{
			printk("Physical Memory Exhausted: Previously allocated up to %x.\n", LastSuccessfulAlloc);
			HALT("Out of memory!");
		}

		if(CurrentFPL->Pairs[FPLBottomIndex].LengthInPages > 1)
		{
			CurrentFPL->Pairs[FPLBottomIndex].LengthInPages--;
			uint64_t raddr = CurrentFPL->Pairs[FPLBottomIndex].BaseAddr;

			CurrentFPL->Pairs[FPLBottomIndex].BaseAddr += 0x1000;

			// printk("[%x]\n", raddr);
			return raddr;
		}
		else if(CurrentFPL->Pairs[FPLBottomIndex].LengthInPages == 1)
		{
			uint64_t raddr = CurrentFPL->Pairs[FPLBottomIndex].BaseAddr;
			CurrentFPL->Pairs[FPLBottomIndex].BaseAddr = 0;
			CurrentFPL->Pairs[FPLBottomIndex].LengthInPages = 0;


			// Set the pointer to the next (B,L) pair.
			FPLBottomIndex++;

			if(CurrentFPL->Pairs[FPLBottomIndex].BaseAddr == 0 || CurrentFPL->Pairs[FPLBottomIndex].LengthInPages == 0)
			{
				HaltNextAlloc = true;
				LastSuccessfulAlloc = raddr;
			}
			// printk("[%x]\n", raddr);
			return raddr;
		}
		else if(CurrentFPL->Pairs[FPLBottomIndex].BaseAddr == 0 && CurrentFPL->Pairs[FPLBottomIndex].LengthInPages == 0)
		{
			if(CurrentFPL->Pairs[FPLBottomIndex + 1].LengthInPages > 0)
				FPLBottomIndex++;

			else if(CurrentFPL->Pairs[FPLBottomIndex - 1].LengthInPages > 0)
				FPLBottomIndex--;

			return AllocatePage_NoMap();
		}
		asm volatile("sti");
		return 0;
	}

	uint64_t AllocateViaPlacement()
	{
		// This works perfectly, except it cannot free pages once allocted.

		PMemManagerLocation += 0x1000;

		if(!(HAL::MMap::IsMemoryValid(PMemManagerLocation)))
		{
			// Check if there are any more 'Available' zones after this one.
			if(!HAL::MMap::CheckForMoreAvailableMemory(PMemManagerLocation))
			{
				printk("\n%wTried to allocate memory at: %w%x%w\nLast possible memory allocatable is: %x",
					VC_Yellow, VC_Cyan, PMemManagerLocation, VC_Silver, PMemManagerLocation - 0x1000);

				HALT("Out of Memory!");
			}
			else
			{
				PMemManagerLocation = HAL::MMap::CheckForMoreAvailableMemory(PMemManagerLocation);
			}
		}
		// Wooop de doo
		return PMemManagerLocation;
	}

	void FreePage_NoUnmap(uint64_t PageAddr)
	{
		_CLI();
		for(int i = 0; i < NumberOfFPLPairs; i++)
		{
			if(PageAddr == CurrentFPL->Pairs[i].BaseAddr + (CurrentFPL->Pairs[i].LengthInPages * 0x1000))
			{
				CurrentFPL->Pairs[i].LengthInPages++;
				_STI();
				return;
			}

			else if(PageAddr == CurrentFPL->Pairs[i].BaseAddr - 0x1000)
			{
				CurrentFPL->Pairs[i].BaseAddr -= 0x1000;
				CurrentFPL->Pairs[i].LengthInPages++;
				_STI();
				return;
			}
			else if(CurrentFPL->Pairs[i].BaseAddr == 0 && CurrentFPL->Pairs[i].LengthInPages == 0)
			{
				// Try not to create a new FPL
				CurrentFPL->Pairs[i].BaseAddr = PageAddr;
				CurrentFPL->Pairs[i].LengthInPages = 1;

				NumberOfFPLPairs++;
				_STI();
				return;
			}
			// Doesn't matter if it's not ordered, we'll get there sooner or later.
			else
			{
				CurrentFPL->Pairs[NumberOfFPLPairs].BaseAddr = PageAddr;
				CurrentFPL->Pairs[NumberOfFPLPairs].LengthInPages = 1;

				NumberOfFPLPairs++;
				_STI();
				return;
			}
		}
	}















	void InitialiseFPLs(MemoryMap_type *MemoryMap)
	{
		// Concept:

		// Pseudo-stack based structure:
		// FIFO;

		// Free frames are stored in pairs:
		// [BaseAddr, Length (in 4K)]
		// 16 bytes per segment of memory.

		// Basically;
		// On Alloc(), check the size of bottom pointer (FIFO, remember)
		// If size > 1, decrement size. Store current BaseAddr, then increment it by 0x1000.
		// Return stored BaseAddr.

		// If size == 1, the segment is about to be emptied. Store current BaseAddr.
		// Change stack bottom pointer to next pair. If none exists, set flag; HALT() on next
		// Alloc().



		// On Free(), loop through each pair entry in the FPL strucutre.
		// If Addr == CurrentEntry->BaseAddr + (Size * 0x1000),
		// It's an entry at the end of an existing pair. Increment size, then return.

		// If Addr == CurrentEntry->BaseAddr - 0x1000,
		// It's an entry at the beginning of an existing pair.
		// Decrement the current BaseAddr by 0x1000, increment the Length by 1 and return.

		// If Addr < Lowest BaseAddr, make a new (BaseAddr, Length) pair, set the BaseAddr to Addr and Length to 1.
		// Set the stack bottom pointer, and return.

		// Else... Addr must either be at the top of the stack, (past last BaseAddr + Length pair)... or,
		// Since Alloc() and Free()'s don't need to be consecutive (ie. one Alloc, one Free, one Alloc, then one Free etc), we will end up with
		// gaps in the segments (similar to disk fragmentation).

		// As such, we need to periodically run a function to clean the FPLs. Alloc() is done in O(1) [constant time], while Free() is done in
		// O(m), where (m) is the number of segments existing (B,L) pairs. This opposed to O(n), which would be relative to the amount of RAM, and would
		// possibly be hideously slow.

		// Basically, what this method does is add a (B,L) pair to the stack for every 'Available' type memory in the E820 map.



		// First, allocate a single 4K page for our FPL.
		CurrentFPL = (TFPL*)AllocatePage_NoMap();
		HAL::VMem::MapAddr(FPLAddress, (uint64_t)CurrentFPL, 0x07);

		// Make sure they're 0, so we don't get any rubbish entries. Quite a problem.
		Memory::Set((uint64_t*)FPLAddress, 0, 0x1000);

		NumberOfFPLPairs = 0;

		for(uint16_t i = 0; i < MemoryMap->NumberOfEntries; i++)
		{
			if(MemoryMap->Entries[i].MemoryType == G_MemoryTypeAvailable && MemoryMap->Entries[i].BaseAddress >= 0x00100000)
			{
				// It's available memory, add a (B,L) pair.
				CurrentFPL->Pairs[NumberOfFPLPairs].BaseAddr = PageAlignUp(MemoryMap->Entries[i].BaseAddress);
				CurrentFPL->Pairs[NumberOfFPLPairs].LengthInPages = (PageAlignDown(MemoryMap->Entries[i].Length) / 0x1000) - 1;

				NumberOfFPLPairs++;
			}
		}
		FPLBottomIndex = 0;

		// The bottom-most FPL would be from 1MB up.
		// However, we must set it to the top of our kernel.

		uint64_t OldBaseAddr = CurrentFPL->Pairs[0].BaseAddr;
		// printk("<<<%x>>>", &KernelEnd);
		// CurrentFPL->Pairs[0].BaseAddr = PageAlignUp((uint64_t)PMemManagerLocation + (uint64_t)CurrentFPL);

		CurrentFPL->Pairs[0].BaseAddr = PageAlignUp((uint64_t)PMemManagerLocation + (uint64_t)CurrentFPL);
		CurrentFPL->Pairs[0].LengthInPages = CurrentFPL->Pairs[0].LengthInPages - ((CurrentFPL->Pairs[0].BaseAddr - OldBaseAddr) / 0x1000);
	}



	void CoalesceFPLs()
	{
		if(!DidInit)
			return;

		// Start from the back

		for(uint32_t i = NumberOfFPLPairs; i > 1; i--)
		{
			// i-1 is the current FPL, i-2 is the one before it.
			// Array dynamics ):


			if((CurrentFPL->Pairs[i - 2].BaseAddr + ((CurrentFPL->Pairs[i - 2].LengthInPages - 2) * 0x1000))
				== CurrentFPL->Pairs[i - 1].BaseAddr)
			{
				CurrentFPL->Pairs[i - 2].LengthInPages += CurrentFPL->Pairs[i - 1].LengthInPages;
				NumberOfFPLPairs--;
			}
		}


		for(uint32_t i = 0; i < NumberOfFPLPairs; i++)
		{
			// Check for zero entries
			if(CurrentFPL->Pairs[i].BaseAddr == 0 || CurrentFPL->Pairs[i].LengthInPages == 0)
			{
				CurrentFPL->Pairs[i].BaseAddr = CurrentFPL->Pairs[i + 1].BaseAddr;
				CurrentFPL->Pairs[i].LengthInPages = CurrentFPL->Pairs[i + 1].LengthInPages;

				if(FPLBottomIndex == i)
					FPLBottomIndex--;
			}
		}
		if(!(CurrentFPL->Pairs[FPLBottomIndex].BaseAddr == 0 && CurrentFPL->Pairs[FPLBottomIndex].LengthInPages == 0))
		{
			HaltNextAlloc = false;
		}

	}




	void DBG_PrintFPLs()
	{
		printk("\n");
		for(int i = 0; i < NumberOfFPLPairs; i++)
		{
			printk("%d. %x -- %d\n", i, CurrentFPL->Pairs[i].BaseAddr, CurrentFPL->Pairs[i].LengthInPages);
		}
	}

}
}
}





















