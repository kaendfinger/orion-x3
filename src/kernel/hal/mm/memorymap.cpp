// MemoryMap.c
// Copyright (c) 2011-2013, <zhiayang@gmail.com>
// Licensed under Creative Commons Attribution 3.0 Unported.




#include <stdlib.h>

using namespace Kernel;
using namespace Library;


MemoryMap_type* K_MemoryMap;
extern uint64_t K_SystemMemoryInBytes;


namespace Kernel {
namespace HAL {
namespace MMap
{
	bool IsMemoryValid(uint64_t Address)
	{
		for(int i = 0; i < K_MemoryMap->NumberOfEntries; i++)
		{
			if(Address >= K_MemoryMap->Entries[i].BaseAddress && Address < (K_MemoryMap->Entries[i].BaseAddress + K_MemoryMap->Entries[i].Length))
			{
				if(K_MemoryMap->Entries[i].MemoryType == G_MemoryTypeAvailable || K_MemoryMap->Entries[i].MemoryType == G_MemoryACPI)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
		}
		return false;
	}

	void GetGlobalMemoryMap(MemoryMap_type *MemoryMap)
	{
		Memory::Copy((void*)MemoryMap, (void*)K_MemoryMap, K_MemoryMap->SizeOfThisStructure);
	}

	uint64_t GetTotalSystemMemory()
	{
		return K_SystemMemoryInBytes;
	}

	uint64_t CheckForMoreAvailableMemory(uint64_t CurrentAddr)
	{
		for(int i = 0; i < K_MemoryMap->NumberOfEntries; i++)
		{
			if(CurrentAddr >= K_MemoryMap->Entries[i].BaseAddress && CurrentAddr < K_MemoryMap->Entries[i].BaseAddress + K_MemoryMap->Entries[i].Length)
			{
				for(int f = i; f < K_MemoryMap->NumberOfEntries; f++)
				{
					if(K_MemoryMap->Entries[f].MemoryType == G_MemoryTypeAvailable)
					{
						return K_MemoryMap->Entries[f].BaseAddress;
					}
				}
			}
		}
		return 0;
	}
}
}
}








