// PIT.c
// Copyright (c) 2011-2013, <zhiayang@gmail.com>
// Licensed under Creative Commons Attribution 3.0 Unported.

#include <stdlib.h>


volatile uint64_t PassedMiliseconds = 0;

namespace Kernel {
namespace HAL {

namespace PIT
{
	void SetTimerHertz(int hz)
	{
	    int divisor = 1193180 / hz;		// Calculate our divisor
	    outb(0x43, 0x36);				// Set our command byte 0x36
	    outb(0x40, divisor & 0xFF);		// Set low byte of divisor
	    outb(0x40, divisor >> 8);		// Set high byte of divisor
	}

	extern "C" void _HAL_PITTimerHandler(HAL_ISRRegs *r)
	{
		// Library::Stdio::PrintString("A");
		// PassedMiliseconds++;

		// if(PassedMiliseconds % 17 == 0 && (Kernel::Internal::GetLFBAddr() != Kernel::Internal::_GetTrueLFBAddr()))
		// 	Kernel::HAL::Device::LFB::RefreshBuffer();

		// if(PassedMiliseconds % 5000 == 0)
		// 	HAL::PMem::CoalesceFPLs();
	}
}
}
}
