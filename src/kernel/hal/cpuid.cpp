// cpuid.c
// Copyright (c) 2011-2013, <zhiayang@gmail.com>
// Licensed under Creative Commons Attribution 3.0 Unported.

#include <stdlib.h>


using namespace Library::Stdio;
using namespace Kernel::HAL::Devices::LFB::Console;

// Intel Specific brand list
static char Intel[32][64] =
{
	"Brand ID Not Supported.",
	"Intel(R) Celeron(R) processor",
	"Intel(R) Pentium(R) III processor",
	"Intel(R) Pentium(R) III Xeon(R) processor",
	"Intel(R) Pentium(R) III processor",
	"Reserved",
	"Mobile Intel(R) Pentium(R) III processor-M",
	"Mobile Intel(R) Celeron(R) processor",
	"Intel(R) Pentium(R) 4 processor",
	"Intel(R) Pentium(R) 4 processor",
	"Intel(R) Celeron(R) processor",
	"Intel(R) Xeon(R) Processor",
	"Intel(R) Xeon(R) processor MP",
	"Reserved",
	"Mobile Intel(R) Pentium(R) 4 processor-M",
	"Mobile Intel(R) Pentium(R) Celeron(R) processor",
	"Reserved",
	"Mobile Genuine Intel(R) processor",
	"Intel(R) Celeron(R) M processor",
	"Mobile Intel(R) Celeron(R) processor",
	"Intel(R) Celeron(R) processor",
	"Mobile Geniune Intel(R) processor",
	"Intel(R) Pentium(R) M processor",
	"Mobile Intel(R) Celeron(R) processor"
};

// This table is for those brand strings that have two values depending on the processor signature. It should have the same number of entries as the above table.
static char Intel_Other[32][40] =
{
	"Reserved",
	"Reserved",
	"Reserved",
	"Intel(R) Celeron(R) processor",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Intel(R) Xeon(R) processor MP",
	"Reserved",
	"Reserved",
	"Intel(R) Xeon(R) processor",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved"
};








namespace Kernel {
namespace HAL {
namespace Devices {
namespace CPUID
{
	#define DoCPUIdent(in, a, b, c, d)		asm volatile("cpuid": "=a" (a), "=b" (b), "=c" (c), "=d" (d) : "a" (in));

	void PrintCPUInfo(bool simple)
	{
		unsigned long ebx, unused;
		DoCPUIdent(0, unused, ebx, unused, unused);
		switch(ebx)
		{
			case 0x756e6547:		// intel
			IdentIntelCPU(simple);
			break;

			case 0x68747541:		// amd
			IdentAMDCPU(simple);
			break;

			default:
			printk("Unknown x86 CPU Detected\n");
			break;
		}
	}

	// Print Registers
	void PrintCPUIDRegs(uint32_t eax, uint32_t ebx, uint32_t ecx, uint32_t edx)
	{
		int j;
		char string[17];
		string[16] = '\0';
		for(j = 0; j < 4; j++)
		{
			string[j] = eax >> (8 * j);
			string[j + 4] = ebx >> (8 * j);
			string[j + 8] = ecx >> (8 * j);
			string[j + 12] = edx >> (8 * j);
		}
		printk(string);
	}

	// Intel-specific information
	void IdentIntelCPU(bool is_simple)
	{
		printk("%wIntel Processor\n", VC_Rose);
		unsigned long eax, ebx, ecx, edx, max_eax, signature, unused;
		int model, family, type, brand, stepping, reserved;
		int extended_family = -1;
		DoCPUIdent(1, eax, ebx, unused, unused);
		model = (eax >> 4) & 0xf;
		family = (eax >> 8) & 0xf;
		type = (eax >> 12) & 0x3;
		brand = ebx & 0xff;
		stepping = eax & 0xf;
		reserved = eax >> 14;
		signature = eax;

		if(!is_simple)
			printk("\t-> %wType%r: %w%d%r - ", VC_Chartreuse, VC_Orange, type);

		printk("{");
		SetColour(VC_Violet);
		switch(type)
		{
			case 0:
				printk("Original OEM");
				break;

			case 1:
				printk("Overdrive");
				break;

			case 2:
				printk("Dual-capable");
				break;

			case 3:
				printk("Reserved");
				break;
		}
		SetColour(VC_White);
		printk("}");


		if(!is_simple)
			printk("\n\t-> %wFamily%r: %w%d%r - ", VC_DarkCyan, VC_Green, family);
		else
			printk("\n");

		printk("{");
		SetColour(VC_Silver);

		switch(family)
		{
			case 3:
				printk("i386");
				break;

			case 4:
				printk("i486");
				break;

			case 5:
				printk("Pentium");
				break;

			case 6:
				printk("Pentium Pro");
				break;

			case 15:
				printk("Pentium 4");
				break;
		}

		printk("}");
		SetColour(VC_White);


		printk("\n");
		if(family == 15)
		{
			extended_family = (eax >> 20) & 0xff;
			printk("\t-> Extended family %d\n", extended_family);
		}
		if(!is_simple)
			printk("\t-> Model %d - ", model);

		switch(family)
		{
			case 3:
			break;

			case 4:
				switch(model)
				{
					case 0:
					case 1:
						printk("DX");
						break;

					case 2:
						printk("SX");
						break;

					case 3:
						printk("487/DX2");
						break;

					case 4:
						printk("SL");
						break;

					case 5:
						printk("SX2");
						break;

					case 7:
						printk("Write-back enhanced DX2");
						break;

					case 8:
						printk("DX4");
						break;
				}
			break;

			case 5:
				switch(model)
				{
					case 1:
						printk("60/66");
						break;

					case 2:
						printk("75-200");
						break;

					case 3:
						printk("for 486 system");
						break;

					case 4:
						printk("MMX");
						break;
				}
			break;

			case 6:
			switch(model)
			{
				case 1:
					printk("Pentium Pro");
					break;

				case 3:
					printk("Pentium II Model 3");
					break;

				case 5:
					printk("Pentium II Model 5/Xeon/Celeron");
					break;

				case 6:
					printk("Celeron");
					break;

				case 7:
					printk("Pentium III/Pentium III Xeon - external L2 cache");
					break;

				case 8:
					printk("Pentium III/Pentium III Xeon - internal L2 cache");
					break;
			}
			break;

			case 15:
			break;
		}
		printk("\n");
		DoCPUIdent(0x80000000, max_eax, unused, unused, unused);

		/* Quok said: If the max extended eax value is high enough to support the processor brand string
		(values 0x80000002 to 0x80000004), then we'll use that information to return the brand information.
		Otherwise, we'll refer back to the brand tables above for backwards compatibility with older processors.
		According to the Sept. 2006 Intel Arch Software Developer's Guide, if extended eax values are supported,
		then all 3 values for the processor brand string are supported, but we'll test just to make sure and be safe. */

		if(max_eax >= 0x80000004)
		{
			printk("\t-> Brand: ");
			if(max_eax >= 0x80000002)
			{
				DoCPUIdent(0x80000002, eax, ebx, ecx, edx);
				PrintCPUIDRegs(eax, ebx, ecx, edx);
			}
			if(max_eax >= 0x80000003)
			{
				DoCPUIdent(0x80000003, eax, ebx, ecx, edx);
				PrintCPUIDRegs(eax, ebx, ecx, edx);
			}
			if(max_eax >= 0x80000004)
			{
				DoCPUIdent(0x80000004, eax, ebx, ecx, edx);
				PrintCPUIDRegs(eax, ebx, ecx, edx);
			}

			printk("\n");
		}
		else if(brand > 0)
		{
			if(!is_simple)
				printk("-> Brand %d - ", brand);

			if(brand < 0x18)
			{
				if(signature == 0x000006B1 || signature == 0x00000F13)
				{
					printk(Intel_Other[brand]);
					printk("\n");
				}
				else
				{
					printk(Intel[brand]);
					printk("\n");
				}
			}
			else
			{
				printk("Reserved\n");
			}
		}
		if(!is_simple)
			printk("\t-> Stepping: %d, Reserved: %d", stepping, reserved);
	}






	// AMD-specific information
	void IdentAMDCPU(bool is_simple)
	{
		printk("%wAMD Processor\n", VC_Orange);
		unsigned long extended, eax, ebx, ecx, edx, unused;
		int family, model, stepping, reserved;
		DoCPUIdent(1, eax, unused, unused, unused);
		model = (eax >> 4) & 0xf;
		family = (eax >> 8) & 0xf;
		stepping = eax & 0xf;
		reserved = eax >> 12;

		if(!is_simple)
			printk("\t-> %wFamily%r: %w%d%r; %wModel%r: %w%d%r <", VC_DarkCyan, VC_Green, family, VC_Magenta, VC_Azure, model);
		else
			printk("\t-> %wModel%r: <", VC_Magenta);

		SetColour(VC_Silver);

		switch(family)
		{
			case 4:
			printk("486 Model %d", model);
			break;

			case 5:
				switch(model)
				{
					case 0:
					case 1:
					case 2:
					case 3:
					case 6:
					case 7:
						printk("K6 Model %d", model);
						break;

					case 8:
						printk("K6-2 Model 8");
						break;

					case 9:
						printk("K6-III Model 9");
						break;

					default:
						printk("K5/K6 Model %d", model);
						break;
				}
			break;
			case 6:
				switch(model)
				{
					case 1:
					case 2:
					case 4:
						printk("Athlon Model %d", model);
						break;

					case 3:
						printk("Duron Model 3");
						break;

					case 6:
						printk("Athlon MP/Mobile Athlon Model 6");
						break;

					case 7:
						printk("Mobile Duron Model 7");
						break;

					default:
						printk("Duron/Athlon Model %d", model);
						break;
				}
			break;
		}
		printk(">\n");
		DoCPUIdent(0x80000000, extended, unused, unused, unused);
		if(extended == 0)
		{
			return;
		}
		if(extended >= 0x80000002)
		{
			unsigned int j;
			printk("\t-> %wProcessor Name%r: <", VC_Yellow);
			for(j = 0x80000002; j <= 0x80000004; j++)
			{
				SetColour(VC_Violet);
				DoCPUIdent(j, eax, ebx, ecx, edx);
				PrintCPUIDRegs(eax, ebx, ecx, edx);
			}
			SetColour(VC_White);
			printk(">\n");
		}
		if(extended >= 0x80000007)
		{
			DoCPUIdent(0x80000007, unused, unused, unused, edx);
			if(edx & 1)
			{
				printk("\t-> %wTemperature Sensing Diode Present\n", VC_Chartreuse);
			}
		}
		if(!is_simple)
			printk("\t-> %wStepping%r: %w%d%r, %wReserved%r: %w%d%r", VC_Blue, VC_Silver, stepping, VC_Rose, VC_Yellow, reserved);
	}
}
}
}
}









