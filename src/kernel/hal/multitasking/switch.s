// Switch.S
// Copyright (c) 2011-2013, <zhiayang@gmail.com>
// Licensed under Creative Commons Attribution 3.0 Unported.


.global Switch
.type Switch, @function
.section .text
.code64

Switch:
	push %r15
	push %r14
	push %r13
	push %r12
	push %r11
	push %r10
	push %r9
	push %r8
	push %rdx
	push %rcx
	push %rbx
	push %rax
	push %rbp
	push %rsi
	push %rdi





	// Load the kernel data segment.
	movw $0x10, %bp
	movl %ebp, %ds
	movl %ebp, %es
	movl %ebp, %fs
	movl %ebp, %gs





	// modify the TSS to change RSP0 to point here
	movq %rsp, 0x500004
	// Memory::Set32((void*)0x500004, 0x00060000, 1);


	// AMD system V calling convention mandates first integer parameter is passed in %rdi
	movq %rsp, %rdi			// stack => rdi
	movq 160(%rsp), %rsi
	call SwitchProcess
	movq %rax, %rsp


	mov $0x20, %al
	outb %al, $0x20









	cmp $0xFADE, %r11
	jne NoRing3





	// if this is ring3, we need to...
	// do ring3!

	pop %rdi
	pop %rsi
	pop %rbp
	pop %rax
	pop %rbx
	pop %rcx
	pop %rdx
	pop %r8
	pop %r9
	pop %r10
	pop %r11
	pop %r12
	pop %r13
	pop %r14
	pop %r15



	// screw around with the stack
	// change the data-segment to user-mode
	// also modify the eflags and code-segment values.

	mov $0x23, %ax
	mov %ax, %ds
	mov %ax, %es
	mov %ax, %fs
	mov %ax, %gs

	xchg %bx, %bx
	movq $0x23, 32(%rsp)	// Push the new stack segment with an RPL of 3.
	// mov %rsp, 24(%rsp)		// Push what the was RSP before pushing anything.
	movq $0x0202, 16(%rsp)	// Push RFLAGS

	movq $0x1B, 8(%rsp)		// Push the new code segment with an RPL of 3.
	// mov %r11, %rax		// RIP is the first parameter
	// mov %r11, (%rsp)		// Push the RIP to IRET to.


	iretq


NoRing3:
	pop %rdi
	pop %rsi
	pop %rbp
	pop %rax
	pop %rbx
	pop %rcx
	pop %rdx
	pop %r8
	pop %r9
	pop %r10
	pop %r11
	pop %r12
	pop %r13
	pop %r14
	pop %r15
	iretq










