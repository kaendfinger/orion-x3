// Task.c
// Copyright (c) 2011-2013, <zhiayang@gmail.com>
// Licensed under Creative Commons Attribution 3.0 Unported.

#include <stdlib.h>

using namespace Kernel;
using namespace Library;

namespace Kernel
{
	namespace Multitasking
	{
		void CreateTask(const char name[64], void (*thread)(), uint8_t attrib)
		{
			CreateTask(GetNumberOfTasks(), name, thread, attrib);
		}

		void CreateTask(int id, const char name[64], void (*thread)(), uint8_t attrib)
		{
			uint64_t* stack;
			uint64_t rbp = (uint64_t)__builtin_return_address(0);
			Task_type *Task = (Task_type*)HAL::DMem::AllocateChunk(sizeof(Task_type));

			Task->StackPointer = HAL::AllocatePage() + 0x2000;       // Allocate 4 kilobytes of space
			HAL::AllocatePage();
			Memory::Set((void*)((uint64_t)Task->StackPointer - 0x2000), 0x00, 0x2000);

			stack = (uint64_t*)Task->StackPointer;

			// Expand down stack
			// processor data
			*--stack = 0x10;				// SS
			*--stack = Task->StackPointer - (15 * 8);	// UserESP
			*--stack = 0x202;				// RFLAGS
			*--stack = 0x08;				// CS
			*--stack = (uint64_t)thread;	// RIP



			*--stack = 15;					// R15
			*--stack = 14;					// R14
			*--stack = 13;					// R13
			*--stack = 12;					// R12
			*--stack = 11;					// R11
			*--stack = 10;					// R10
			*--stack = 9;					// R9
			*--stack = 8;					// R8

			*--stack = 0xD;					// RDX
			*--stack = 0xC;					// RCX
			*--stack = 0xB;					// RBX
			*--stack = 0xA;					// RAX


			*--stack = rbp;					// RBP
			*--stack = 0xC0;				// RSI
			*--stack = 0xD0;				// RDI





			Task->State = 1;
			Task->StackPointer = (uint64_t)stack;
			Task->Thread = thread;
			Task->ProcessID = id;
			Task->Sleep = 0;
			Task->Flags = attrib;

			String::Copy((char*)Task->Name, (char*)name);

			SetTaskInList(id, Task);
		}
	}
}












