// GPT.cpp
// Copyright (c) 2011-2013, <zhiayang@gmail.com>
// Licensed under Creative Commons Attribution 3.0 Unported.

#include <namespace.h>
#include <stdlib.h>

using namespace Kernel::HAL::Devices::ATA;
using namespace Library::Stdio;

namespace Kernel {
namespace HAL {
namespace FS {
namespace GPT
{
	void ReadPartitions(Devices::ATA::ATADevice* atadev)
	{
		// normally MBR::ReadPartitions will call this... so we can assume the list is uninitialised.
		using FS::Partition;
		atadev->Partitions = new Library::Types::List();
		// warning: will trash whatever the ATADevice's data buffer contains.

		// read the gpt
		PIO::ReadSector(atadev, 1);


		uint8_t* gpt = (uint8_t*)atadev->Data;

		// 0x5452415020494645

		if(*((uint64_t*)gpt) != 0x5452415020494645)
			printk("Invalid GPT disk signature: expected [%x], got [%x] instead -- Check your disk.\n", 0x5452415020494645, *((uint64_t*)gpt));


		// uint32_t number = *((uint32_t*)(gpt + 80));

		uint32_t number = 4;
		atadev->SetIsGPT(true);


		// if the header is fine, read LBA 2 to parse the partition table.


		PIO::ReadSector(atadev, 2);

		uint64_t* buffer = (uint64_t*)HAL::DMem::AllocateChunk(512);

		Library::Memory::Copy((void*)buffer, atadev->Data, 512);

		uint8_t* table = (uint8_t*)buffer;
		for(uint64_t p = 0; p < number; p++)
		{
			if(*((uint64_t*)(table + (p * 128) + 8)) != 0 && *((uint64_t*)(table + (p * 128) + 0)) != 0)
			{
				atadev->Partitions->Add(new Partition(atadev, p, *((uint64_t*)(table + (p * 128) + 32)), *((uint64_t*)(table + (p * 128) + 40)) - *((uint64_t*)(table + (p * 128) + 32)) + 1, FSTypes::hfsplus, *((uint64_t*)(table + (p * 128) + 24)), *((uint64_t*)(table + (p * 128) + 16)), *((uint64_t*)(table + (p * 128) + 0)), *((uint64_t*)(table + (p * 128) + 8)), (char*)"", false));
			}
		}
	}
}
}
}
}








