// MBR.h
// Copyright (c) 2011-2013, <zhiayang@gmail.com>
// Licensed under Creative Commons Attribution 3.0 Unported.

#include <namespace.h>
#include <stdlib.h>

using namespace Library::Stdio;

namespace Kernel {
namespace HAL {
namespace FS
{
	Partition::Partition(Devices::ATA::ATADevice* Drive, uint8_t num, uint64_t StartLBA, uint64_t LBALength, uint16_t Type, uint64_t PartitionGUID_high, uint64_t PartitionGUID_low, uint64_t TypeGUID_high, uint64_t TypeGUID_low, char Name[37], bool Bootable)
	{
		this->Drive = Drive;

		this->StartLBA = StartLBA;
		this->LBALength = LBALength;
		this->PartitionType = Type;



		this->PartitionGUID_high = PartitionGUID_high;
		this->PartitionGUID_low = PartitionGUID_low;

		this->PartitionTypeGUID_high = TypeGUID_high;
		this->PartitionTypeGUID_low = TypeGUID_low;

		// Library::String::Copy(this->Name, Name);

		this->Bootable = Bootable;

		// if we find a partition of type 0xEE, the drive is assumed to be a GPT drive.
		if(Type == 0xEE)
			this->Drive->SetIsGPT(true);

		this->PartitionNumber = num;

		// 48465300-0000-11AA-AA11-00306543ECAC

		if(PartitionType == 0xC ||
			PartitionType == 0xB ||
			(this->Drive->GetIsGPT() &&
			this->GetTypeGUID_S1() == 0xEBD0A0A2 &&
			this->GetTypeGUID_S2() == 0xB9E5 &&
			this->GetTypeGUID_S3() == 0x4433 &&
			this->GetTypeGUID_S4() == 0x87C0 &&
			this->GetTypeGUID_S5() == 0x68B6B72699C7))
		{
			this->Filesystem = new FAT32(this);
		}

		if(this->Drive->GetIsGPT() &&
			this->GetTypeGUID_S1() == 0x48465300 &&
			this->GetTypeGUID_S2() == 0x0000 &&
			this->GetTypeGUID_S3() == 0x11AA &&
			this->GetTypeGUID_S4() == 0xAA11 &&
			this->GetTypeGUID_S5() == 0x00306543ECAC)
		{
			// this->Filesystem = new HFSPlus(this);
		}
	}

	void Partition::PrintInfo()
	{
		int index = Kernel::ReduceBinaryUnits(this->LBALength * this->Drive->GetSectorSize());
		uint64_t mem = Kernel::GetReducedMemory(this->LBALength * this->Drive->GetSectorSize());

		printk("\t-> %wPartition%r%k[%d%k] %won%r %wHDD%r%k[%d%k]: %k{Type: %x, Start: LBA %d, Length: %d Blocks, Size: %w%d %s%k}", VC_DarkCyan, VC_Silver, this->PartitionNumber, VC_Silver, VC_Silver, VC_Violet, VC_Silver, this->Drive->GetDriveNumber(), VC_Silver, VC_Silver, (uint64_t)this->PartitionType, this->StartLBA, this->LBALength, VC_DarkRed, mem, Kernel::K_BinaryUnits[index], VC_Silver);
	}

	uint64_t				Partition::GetStartLBA(){ return this->StartLBA; }
	uint64_t				Partition::GetLBALength(){ return this->LBALength; }
	uint16_t				Partition::GetType(){ return this->PartitionType; }
	uint64_t				Partition::GetGUID_High(){ return this->PartitionGUID_high; }
	uint64_t				Partition::GetGUID_Low(){ return this->PartitionGUID_low; }
	uint64_t				Partition::GetTypeGUID_High(){ return this->PartitionTypeGUID_high; }
	uint64_t				Partition::GetTypeGUID_Low(){ return this->PartitionTypeGUID_low; }
	char*					Partition::GetName(){ return this->Name; }
	bool					Partition::IsBootable(){ return this->Bootable; }
	Devices::ATA::ATADevice*Partition::GetDrive(){ return this->Drive; }
	uint8_t					Partition::GetPartitionNumber(){ return this->PartitionNumber; }
	Filesystem*				Partition::GetFilesystem(){ return this->Filesystem; }


	uint64_t				Partition::GetGUID_S1(){ return this->PartitionGUID_high & 0x00000000FFFFFFFF; }
	uint64_t				Partition::GetGUID_S2(){ return this->PartitionGUID_high & 0x0000FFFF00000000; }
	uint64_t				Partition::GetGUID_S3(){ return ((this->PartitionGUID_high & 0xFFFF000000000000) >> 48); }
	uint64_t				Partition::GetGUID_S4()
	{
		return (((this->PartitionTypeGUID_low & 0xFFFF) & 0xFF00) >> 8) | (((this->PartitionGUID_low & 0xFFFF) & 0xFF) << 8);
	}
	uint64_t Partition::GetGUID_S5()
	{
		uint64_t s1 = (this->PartitionGUID_low & 0xFF00000000000000) >> 56;
		uint64_t s2 = (this->PartitionGUID_low & 0x00FF000000000000) >> 40;
		uint64_t s3 = (this->PartitionGUID_low & 0x0000FF0000000000) >> 24;
		uint64_t s4 = (this->PartitionGUID_low & 0x000000FF00000000) >> 8;
		uint64_t s5 = (this->PartitionGUID_low & 0x00000000FF000000) << 8;
		uint64_t s6 = (this->PartitionGUID_low & 0x0000000000FF0000) << 24;
		return s1 | s2 | s3 | s4 | s5 | s6;
	}


	uint64_t				Partition::GetTypeGUID_S1(){ return this->PartitionTypeGUID_high & 0x00000000FFFFFFFF; }
	uint64_t				Partition::GetTypeGUID_S2(){ return this->PartitionTypeGUID_high & 0x0000FFFF00000000; }
	uint64_t				Partition::GetTypeGUID_S3(){ return ((this->PartitionTypeGUID_high & 0xFFFF000000000000) >> 48); }
	uint64_t				Partition::GetTypeGUID_S4()
	{
		return (((this->PartitionTypeGUID_low & 0xFFFF) & 0xFF00) >> 8) | (((this->PartitionTypeGUID_low & 0xFFFF) & 0xFF) << 8);
	}


	uint64_t Partition::GetTypeGUID_S5()
	{
		uint64_t s1 = (this->PartitionTypeGUID_low & 0xFF00000000000000) >> 56;
		uint64_t s2 = (this->PartitionTypeGUID_low & 0x00FF000000000000) >> 40;
		uint64_t s3 = (this->PartitionTypeGUID_low & 0x0000FF0000000000) >> 24;
		uint64_t s4 = (this->PartitionTypeGUID_low & 0x000000FF00000000) >> 8;
		uint64_t s5 = (this->PartitionTypeGUID_low & 0x00000000FF000000) << 8;
		uint64_t s6 = (this->PartitionTypeGUID_low & 0x0000000000FF0000) << 24;
		return s1 | s2 | s3 | s4 | s5 | s6;
	}





	void Partition::ReadSector(uint64_t LBA)
	{
		// these functions are special, they address the LBA in partition-relative terms.
		this->Drive->ReadSector(this->StartLBA + LBA);
		// Library::Memory::Copy((void*)(this->Data), (void*)(this->Drive->Data), 512);
	}

	void Partition::WriteSector(uint64_t LBA)
	{
		// Library::Memory::Copy((void*)this->Drive->Data, (void*)this->Data, 512);
		this->Drive->WriteSector(this->StartLBA + LBA);
	}
}
}
}
