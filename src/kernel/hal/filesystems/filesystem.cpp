// Filesystem.cpp
// Copyright (c) 2011-2013, <zhiayang@gmail.com>
// Licensed under Creative Commons Attribution 3.0 Unported.

#include <namespace.h>
#include <stdlib.h>

using namespace Library::Stdio;

namespace Kernel {
namespace HAL {
namespace FS
{
	// Internal FS types.
	/*
		// FAT family
		0xF1EF: exFAT
		0xF132: FAT32
		0xF116: FAT16
		0xF112: FAT12

		// Apple
		0x11F5: HFS+
		0x11F0: HFS

		// Linux
		0xEB12: ext2
		0xEB13: ext3
		0xEB14: ext4
	*/

	Filesystem::Filesystem(Partition* parent, FSTypes type)
	{
		this->ParentPartition = parent;
		this->Type = type;
	}

	void Filesystem::PrintInfo()
	{
		switch(this->Type)
		{
			case fat32:
				printk("\t-> FAT32 Filesystem on /dev/hdd%dp%d: {}", this->ParentPartition->GetDrive()->GetDriveNumber(), this->ParentPartition->GetPartitionNumber());
				break;

			case fat12:
			case fat16:
			case exFAT:
			case hfs:
			case hfsplus:
				printk("\t-> HFS+ Filesystem on /dev/hdd%dp%d: {}", this->ParentPartition->GetDrive()->GetDriveNumber(), this->ParentPartition->GetPartitionNumber());
			case ext2:
			case ext3:
			case ext4:
				break;

			default:
				printk("\t-> Unknown Filesystem");
		}
	}

	Partition* Filesystem::GetPartition()
	{
		return this->ParentPartition;
	}
}
}
}
