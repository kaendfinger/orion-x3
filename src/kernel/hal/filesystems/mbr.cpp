// MBR.cpp
// Copyright (c) 2011-2013, <zhiayang@gmail.com>
// Licensed under Creative Commons Attribution 3.0 Unported.

#include <namespace.h>
#include <stdlib.h>

using namespace Library::Stdio;
using namespace Kernel::HAL::Devices::ATA;

namespace Kernel {
namespace HAL {
namespace FS {
namespace MBR
{
	void ReadPartitions(Devices::ATA::ATADevice* atadev)
	{
		using FS::Partition;
		// warning: will trash whatever the ATADevice's data buffer contains.

		// read the mbr
		PIO::ReadSector(atadev, 0);

		uint64_t* buffer = (uint64_t*)HAL::DMem::AllocateChunk(512);
		Library::Memory::Copy((void*)buffer, atadev->Data, 512);

		// verify the MBR signature: 0x55, 0xAA
		// but since x86 is little-endian, it would really be 0xAA55
		if(atadev->Data[255] != 0xAA55)
			printk("\t\t\t- Invalid MBR signature! Expected [%x], got [%x] instead -- Check your disk.\n", 0xAA55, atadev->Data[255]);


		// read the partition table at the 446'th byte
		uint8_t* mbr = (uint8_t*)buffer;



		// check for a GPT partition
		for(uint16_t o = 0x1BE; o < 0x1BE + 0x40; o += 0x10)
		{
			if(*((uint32_t*)(mbr + o + 12)) > 0)
			{
				// loop through each partition, looking for one with type = 0xEE. If this is so, we pass the
				// device to GPT::ReadPartitions instead (to intialise the list there) and return immediately.

				if(*(mbr + o + 4) == 0xEE)
				{
					GPT::ReadPartitions(atadev);
					return;
				}
			}
		}

		// if we got here, there should be no GPT partitions -- init the list.
		atadev->Partitions = new Library::Types::List();
		// read the partition table
		for(uint16_t o = 0x1BE; o < 0x1BE + 0x40; o += 0x10)
		{
			if(*((uint32_t*)(mbr + o + 12)) > 0)
			{
				atadev->Partitions->Add(new Partition(atadev, (o - 0x1BE) / 0x10, *((uint32_t*)(mbr + o + 8)), *((uint32_t*)(mbr + o + 12)), *(mbr + o + 4), 0, 0, 0, 0, (char*)"", *(mbr + o) == 0x80));
			}
		}
	}
}
}
}
}
