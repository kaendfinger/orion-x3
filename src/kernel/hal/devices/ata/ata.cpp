// ATA.cpp
// Copyright (c) 2011-2013, <zhiayang@gmail.com>
// Licensed under Creative Commons Attribution 3.0 Unported.


#include <namespace.h>
#include <stdlib.h>

using namespace Kernel;
using namespace Kernel::HAL;
using namespace Library::Stdio;

namespace Kernel {
namespace HAL {
namespace Devices {
namespace ATA
{
	ATADevice::ATADevice(uint8_t Bus, uint8_t Drive)
	{
		this->Bus = Bus;
		this->Drive = Drive;
		this->DMA = false;
		this->SectorSize = 512;				// TODO: detect sector size
		this->BaseIO = (Bus == 0 ? PrimaryBaseIO : SecondaryBaseIO);
		this->DriveNumber = (Bus ? 2 : 0) + (Drive ? 1 : 0);
	}


	Library::Types::List* ATADevice::ATADevices;


	bool ATADevice::GetIsGPT(){ return this->IsGPT; }
	void ATADevice::SetIsGPT(bool IsGPT){ this->IsGPT = IsGPT; }
	uint8_t ATADevice::GetDriveNumber(){ return this->DriveNumber; }
	bool ATADevice::IsDMA(){ return this->DMA; }
	uint8_t ATADevice::GetBus(){ return this->Bus; }
	uint8_t ATADevice::GetDrive(){ return this->Drive; }
	void ATADevice::SetSectors(uint64_t s){ this->MaxSectors = s; }
	uint64_t ATADevice::GetSectors(){ return this->MaxSectors; }
	void ATADevice::SetSectorSize(uint32_t s){ this->SectorSize = s; }
	uint32_t ATADevice::GetSectorSize(){ return this->SectorSize; }
	uint16_t ATADevice::GetBaseIO(){ return this->BaseIO; }
	void ATADevice::ReadSector(uint64_t LBA){ PIO::ReadSector(this, LBA); }
	void ATADevice::WriteSector(uint64_t LBA){ PIO::WriteSector(this, LBA); }



	void PrintATAInfo(ATADevice* ata)
	{
		int index = Kernel::ReduceBinaryUnits(ata->GetSectors() * ata->GetSectorSize());
		uint64_t mem = Kernel::GetReducedMemory(ata->GetSectors() * ata->GetSectorSize());


		// printk("\t-> %w%s%r %wBus%r, %w%s%r %wDrive%r: [%w%d%r sectors, sector-size: %w%d%r bytes, size: %w%d %s%r%w, %s]", VC_Green, !ata->GetBus() ? "Primary" : "Secondary", VC_Orange, VC_Magenta, !ata->GetDrive() ? "Master" : "Slave", VC_Blue, VC_Violet, ata->GetSectors(), VC_Yellow, ata->GetSectorSize(), VC_Magenta, mem, Kernel::K_BinaryUnits[index], VC_Cyan, ata->GetIsGPT() ? "GPT Drive" : "MBR Drive");

		printk("\t-> %w%s%r %wBus%r, %w%s%r %wDrive%r: %k[%w%d%r %wsectors%r, %wsize%r: %w%d %s%r, %w%s%r%k]", VC_Green, !ata->GetBus() ? "Primary" : "Secondary", VC_Orange, VC_Magenta, !ata->GetDrive() ? "Master" : "Slave", VC_Orange, VC_Rose, VC_Cyan, ata->GetSectors(), VC_Silver, VC_Silver, VC_Yellow, mem, Kernel::K_BinaryUnits[index], VC_DarkCyan, ata->GetIsGPT() ? "GPT Drive" : "MBR Drive", VC_Rose);
	}

	void IdentifyAll()
	{
		if(ATA::IdentifyDevice(PrimaryBaseIO, true))
		{
			ATADevice* d = ATA::IdentifyDevice(PrimaryBaseIO, true);
			if(d)
				ATADevice::ATADevices->Add(d);
		}

		if(ATA::IdentifyDevice(PrimaryBaseIO, false))
		{
			ATADevice* d = ATA::IdentifyDevice(PrimaryBaseIO, false);
			if(d)
				ATADevice::ATADevices->Add(d);
		}

		if(ATA::IdentifyDevice(SecondaryBaseIO, true))
		{
			ATADevice* d = ATA::IdentifyDevice(SecondaryBaseIO, true);
			if(d)
				ATADevice::ATADevices->Add(d);
		}

		if(ATA::IdentifyDevice(SecondaryBaseIO, false))
		{
			ATADevice* d = ATA::IdentifyDevice(SecondaryBaseIO, false);
			if(d)
				ATADevice::ATADevices->Add(d);
		}
	}

	ATADevice* IdentifyDevice(uint16_t BaseIO, bool IsMaster)
	{
		using Kernel::HAL::Devices::ATA::ATADevice;
		// Identify the drives on the primary bus

		// Do master drive on primary
		outb(BaseIO + 6, (IsMaster ? 0xA0 : 0xB0));			// drive select
		outb(BaseIO + 2, 0);								// sector count
		outb(BaseIO + 3, 0);								// lba low
		outb(BaseIO + 4, 0);								// lba mid
		outb(BaseIO + 5, 0);								// lba high

		outb(BaseIO + 7, ATA_Identify);	// send the identify command

		// read status port
		uint16_t exist = inb(BaseIO + 7);
		uint8_t pollcount = 0;
		if(exist)
		{
			while(pollcount < (uint8_t)(-1))
			{
				if(inb(BaseIO + 7) & (1 << 7))
					pollcount++;

				else
					break;
			}
			if(inb(BaseIO + 4) != 0 || inb(BaseIO + 5) != 0)
			{
				// non-compliant ATA device, most likely ATAPI
				return 0;
			}

			pollcount = 0;
			while(pollcount < (uint8_t)(-1))
			{
				if(inb(BaseIO + 7) & (1 << 3))
					break;

				else if(inb(BaseIO + 7) & (1 << 0))
					return 0;
			}
			// init the ATA object
			ATADevice* ata = new ATADevice((BaseIO == PrimaryBaseIO ? 0 : 1), (IsMaster ? 0 : 1));
			for(size_t i = 0; i < 256; i++)
			{
				ata->Data[i] = inw(BaseIO + 0);
			}

			// read the identify data -- disk size in sectors.
			if(ata->Data[83] & (1 << 10))
			{
				ata->SetSectors(*((uint64_t*)(ata->Data + 100)));
			}
			else
			{
				ata->SetSectors(*(uint32_t*)(ata->Data + 60));
			}

			return ata;
		}
		return 0;
	}
}
}
}
}



















