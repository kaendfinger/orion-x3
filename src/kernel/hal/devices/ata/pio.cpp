// PIO.cpp
// Copyright (c) 2011-2013, <zhiayang@gmail.com>
// Licensed under Creative Commons Attribution 3.0 Unported.


#include <namespace.h>
#include <stdlib.h>

using namespace Kernel;
using namespace Kernel::HAL;
using namespace Library::Stdio;

namespace Kernel {
namespace HAL {
namespace Devices {
namespace ATA
{





	namespace PIO
	{
		static bool IsWaiting14 = false;
		static bool IsWaiting15 = false;

		void ReadSector(ATADevice* dev, uint64_t Sector)
		{
			uint16_t timeout = 0;
			// read the alt. status port to check if the drive is ready
			uint8_t stat = inb(dev->GetBaseIO() + 7);

			// check for error
			if(stat & (1 << 0) || stat & (1 << 5))
				printk("\n\n\t-> ATA Drive ERR or DF bit set.\n\n");

			// wait until we're ready
			while(!(inb(dev->GetBaseIO() + 7) & (1 << 6)) && (inb(dev->GetBaseIO() + 7) & (1 << 7)) && (inb(dev->GetBaseIO() + 7) & (1 << 3)) && (stat & (1 << 0) || stat & (1 << 5)) && timeout < 65535)
			{
				timeout++;
			}

			if(timeout == 65535)
				printk("\n\n\t-> Disk reading sector [%d] failed, BSY bit stuck or RDY bit not set.\n\n", Sector);


			// don't use LBA48 unless required
			if(Sector > 0x0FFFFFFF)
			{
				// init
				outb(dev->GetBaseIO() + 6, (dev->GetDrive() ? 0x50 : 0x40));

				// high sector count (0)
				outb(dev->GetBaseIO() + 2, 0);

				// send high bytes of LBA
				outb(dev->GetBaseIO() + 3, (uint8_t)(Sector >> 24));
				outb(dev->GetBaseIO() + 4, (uint8_t)(Sector >> 32));
				outb(dev->GetBaseIO() + 5, (uint8_t)(Sector >> 40));
			}
			else
			{
				// init
				outb(dev->GetBaseIO() + 6, (dev->GetDrive() ? 0xF0 : 0xE0) | ((Sector >> 24) & 0x0F));
			}

			// sector count
			outb(dev->GetBaseIO() + 2, 1);

			// send low bytes of LBA
			outb(dev->GetBaseIO() + 3, (uint8_t)Sector);
			outb(dev->GetBaseIO() + 4, (uint8_t)(Sector >> 8));
			outb(dev->GetBaseIO() + 5, (uint8_t)(Sector >> 16));



			IsWaiting14 = !dev->GetBus();
			IsWaiting15 = dev->GetBus();

			outb(dev->GetBaseIO() + 7, (Sector > 0x0FFFFFFF ? ATA_ReadSectors48 : ATA_ReadSectors28));

			asm volatile("sti");
			if(IsWaiting14)
			{
				while(IsWaiting14)
					asm volatile("hlt");
			}
			else
			{
				while(IsWaiting15)
					asm volatile("hlt");
			}

			// for(size_t i = 0; i < 256; i++)
			// {
			// 	dev->Data[i] = 0;
			// }

			// read data
			for(size_t i = 0; i < 256; i++)
			{
				dev->Data[i] = inw(dev->GetBaseIO() + 0);
			}
		}

		void WriteSector(ATADevice* dev, uint64_t Sector)
		{
			uint16_t timeout = 0;
			// read the alt. status port to check if the drive is ready
			uint8_t stat = inb(dev->GetBaseIO() + 7);

			// check for error
			if(stat & (1 << 0) || stat & (1 << 5))
				printk("\n\n\t-> ATA Drive ERR or DF bit set.\n\n");


			// wait until we're ready
			while(!(inb(dev->GetBaseIO() + 7) & (1 << 6)) && (inb(dev->GetBaseIO() + 7) & (1 << 7)) && (inb(dev->GetBaseIO() + 7) & (1 << 3)) && (stat & (1 << 0) || stat & (1 << 5)) && timeout < 65535)
			{
				timeout++;
			}

			if(timeout == 65535)
				printk("\n\n\t-> Disk reading sector [%d] failed, BSY bit stuck or RDY bit not set.\n\n", Sector);


			// don't use LBA48 unless required
			if(Sector > 0x0FFFFFFF)
			{
				// init
				outb(dev->GetBaseIO() + 6, dev->GetDrive() ? 0x50 : 0x40);

				// high sector count (0)
				outb(dev->GetBaseIO() + 2, 0);

				// send high bytes of LBA
				outb(dev->GetBaseIO() + 3, (uint8_t)(Sector >> 24));
				outb(dev->GetBaseIO() + 4, (uint8_t)(Sector >> 32));
				outb(dev->GetBaseIO() + 5, (uint8_t)(Sector >> 40));
			}
			else
			{
				// init
				outb(dev->GetBaseIO() + 6, (dev->GetDrive() ? 0xF0 : 0xE0) | ((Sector >> 24) & 0x0F));
			}

			// sector count
			outb(dev->GetBaseIO() + 2, 1);

			// send low bytes of LBA
			outb(dev->GetBaseIO() + 3, (uint8_t)Sector);
			outb(dev->GetBaseIO() + 4, (uint8_t)(Sector >> 8));
			outb(dev->GetBaseIO() + 5, (uint8_t)(Sector >> 16));

			// IsWaiting14 = !dev->GetBus();
			// IsWaiting15 = dev->GetBus();

			outb(dev->GetBaseIO() + 7, (Sector > 0x0FFFFFFF ? ATA_WriteSectors48 : ATA_WriteSectors28));


			// if(IsWaiting14)
			// {
			// 	while(IsWaiting14) asm volatile("hlt");
			// }
			// else
			// {
			// 	while(IsWaiting15) asm volatile("hlt");
			// }


			// write data
			for(size_t i = 0; i < 256; i++)
			{
				outw(dev->GetBaseIO() + 0, dev->Data[i]);
			}

		}


		extern "C" void IRQHandler14(HAL_ISRRegs* r)
		{
			IsWaiting14 = false;
		}

		extern "C" void IRQHandler15(HAL_ISRRegs* r)
		{
			IsWaiting15 = false;
		}
	}
}
}
}
}



















