// SerialPort.c
// Copyright (c) 2011-2013, <zhiayang@gmail.com>
// Licensed under Creative Commons Attribution 3.0 Unported.



#include <stdlib.h>


namespace Kernel {
namespace HAL {
namespace Devices {
namespace COM1
{

	#define Port		0x3F8		// Serial port 1
	void WriteString(const char *String)
	{
		unsigned int i;

		for(i = 0; String[i] != '\0'; i++)
		{
			WriteChar(String[i]);
		}
	}

	void Initialise()
	{
		outb(Port + 1, 0x00);    // Disable all interrupts
		outb(Port + 3, 0x80);    // Enable DLAB (set baud rate divisor)
		outb(Port + 0, 0x03);    // Set divisor to 3 (lo byte) 38400 baud
		outb(Port + 1, 0x00);    //                  (hi byte)
		outb(Port + 3, 0x03);    // 8 bits, no parity, one stop bit
		outb(Port + 2, 0xC7);    // Enable FIFO, clear them, with 14-byte threshold
		outb(Port + 4, 0x0B);    // IRQs enabled, RTS/DSR set
	}

	void WriteChar(char Ch)
	{
		while(!(inb(Port + 5) & 0x20));

		outb(Port, Ch);
	}

}
}
}
}




