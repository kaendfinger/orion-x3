// PS2.cpp
// Copyright (c) 2011-2013, <zhiayang@gmail.com>
// Licensed under Creative Commons Attribution 3.0 Unported.

#include <namespace.h>
#include <stdlib.h>

using namespace Library::Stdio;

namespace Kernel {
namespace HAL {
namespace Devices {
namespace PS2
{
	uint8_t PS2Controller::Device1Buffer = 0;
	uint8_t PS2Controller::Device2Buffer = 0;

	void PS2Controller::HandleIRQ1(Kernel::HAL::HAL_ISRRegs* r)
	{
		Kernel::Internal::GetGlobalKeyboard()->HandleKeypress();
	}

	void PS2Controller::HandleIRQ12(Kernel::HAL::HAL_ISRRegs* r)
	{
		Device2Buffer = inb(DataPort);
	}

	PS2Controller::PS2Controller()
	{
		// flush the buffer.
		inb(this->DataPort);

		// read the config byte.
		uint8_t ConfigByte = 0;
		outb(this->CommandPort, 0x20);
		while(!(inb(this->CommandPort) & 0x1));
		ConfigByte = inb(this->DataPort);

		ConfigByte &= ~(1 << 6);

		// write the value back
		outb(this->CommandPort, 0x60);
		while(inb(this->CommandPort) & 0x2);
		outb(this->DataPort, ConfigByte);



		// while(inb(this->CommandPort) & (1 << 1));
		// outb(this->DataPort, 0xF3);
		// outb(this->DataPort, 0x00);




		Kernel::HAL::Interrupts::InstallIRQHandler(1, HandleIRQ1);
		Kernel::HAL::Interrupts::InstallIRQHandler(12, HandleIRQ12);

		// should be ready to use.
	}

}
}
}
}
