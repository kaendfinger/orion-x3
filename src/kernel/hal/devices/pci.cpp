// PCI.cpp
// Copyright (c) 2011-2013, <zhiayang@gmail.com>
// Licensed under Creative Commons Attribution 3.0 Unported.


// Functions to read PCI devices, check for their existence and read their values.
// Defines a PCI device on the bus as an object. Useful.

#include <stdlib.h>

using namespace Kernel;
using namespace Library::Stdio;


namespace Kernel {
namespace HAL {
namespace Devices {
namespace PCI
{
	PCIDevice* SearchByVendorDevice(uint16_t VendorID, uint16_t DeviceID)
	{
		uint8_t bus = 0, slot = 0, func = 0;
		uint16_t vendor = 0, device = 0;


		for(uint16_t i = 0; i < PCIDevice::PCIDevices->Size(); i++)
		{
			bus = ((PCIDevice*)(*PCIDevice::PCIDevices)[i])->GetBus();
			slot = ((PCIDevice*)(*PCIDevice::PCIDevices)[i])->GetSlot();
			func = ((PCIDevice*)(*PCIDevice::PCIDevices)[i])->GetFunction();


			vendor = PCI::ReadConfig16(PCI::MakeAddr(bus, slot, func), 0);
			device = (uint16_t)(PCI::ReadConfig32(PCI::MakeAddr(bus, slot, func), 0) >> 16);

			if(vendor == VendorID && device == DeviceID)
				return (PCIDevice*)(*PCIDevice::PCIDevices)[i];
		}
		return 0;
	}

	PCIDevice* SearchByClassSubclass(uint8_t Class, uint8_t Subclass)
	{
		uint8_t bus = 0, slot = 0, func = 0;
		uint16_t tClass = 0, tSubclass = 0;


		for(uint16_t i = 0; i < PCIDevice::PCIDevices->Size(); i++)
		{
			bus = ((PCIDevice*)(*PCIDevice::PCIDevices)[i])->GetBus();
			slot = ((PCIDevice*)(* PCIDevice::PCIDevices)[i])->GetSlot();
			func = ((PCIDevice*)(*PCIDevice::PCIDevices)[i])->GetFunction();


			tClass = (uint8_t)(ReadConfig32(MakeAddr(bus, slot, func), 0x08) >> 24);
			tSubclass = (uint8_t)(ReadConfig32(MakeAddr(bus, slot, func), 0x08) >> 16);

			if(tClass == Class && tSubclass == Subclass)
				return (PCIDevice*)(*PCIDevice::PCIDevices)[i];
		}
		return 0;
	}

	bool MatchVendorDevice(PCIDevice* dev, uint16_t VendorID, uint16_t DeviceID)
	{
		return (dev->GetVendorID() == VendorID && dev->GetDeviceID() == DeviceID);
	}

	bool MatchClassSubclass(PCIDevice* dev, uint8_t Class, uint8_t Subclass)
	{
		return (dev->GetClass() == Class && dev->GetSubclass() == Subclass);
	}




	uint32_t MakeAddr(uint16_t Bus, uint16_t Slot, uint16_t Function)
	{
		return (uint32_t)((Bus << 16) | (Slot << 11) | (Function << 8) | 1 << 31);
	}

	// READ REGISTERS

	uint32_t ReadConfig32(uint32_t Address, uint16_t Offset)
	{
		outl(0xCF8, Address + Offset);
		return (inl(0xCFC));
	}

	uint16_t ReadConfig16(uint32_t Address, uint16_t Offset)
	{
		outl(0xCF8, Address + Offset);
		return (inl(0xCFC));
	}

	uint8_t ReadConfig8(uint32_t Address, uint16_t Offset)
	{
		outl(0xCF8, Address + Offset);
		return (inl(0xCFC));
	}






	// WRITE REGISTERS

	void WriteConfig32(uint32_t Address, uint16_t Offset, uint32_t Data)
	{
		// Write the address to read.
		outl(0xCF8, Address + Offset);
		outl(0xCFC, Data);
	}

	void WriteConfig16(uint32_t Address, uint16_t Offset, uint16_t Data)
	{
		outl(0xCF8, Address + Offset);
		outl(0xCFC, (uint32_t)Data);
	}

	void WriteConfig8(uint32_t Address, uint16_t Offset, uint8_t Data)
	{
		outl(0xCF8, Address + Offset);
		outl(0xCFC, (uint32_t)Data);
	}

	uint16_t CheckDeviceExistence(uint16_t Bus, uint16_t Slot)
	{
		if(ReadConfig16(MakeAddr(Bus, Slot, 0), 0) == 0xFFFF)
		{
			// Non-existent devices return 0xFFFF on read.
			return 0;
		}
		else
			return 1;
	}







	Library::Types::List* PCIDevice::PCIDevices;

	PCIDevice::PCIDevice(uint16_t Bus, uint16_t Slot, uint8_t Function)
	{
		this->Bus			= Bus;
		this->Slot			= Slot;
		this->Function		= Function;

		this->VendorID		= ReadConfig16(MakeAddr(Bus, Slot, Function), 0x00);
		this->DeviceID		= (uint16_t)(ReadConfig32(MakeAddr(Bus, Slot, Function), 0x00) >> 16);


		this->Class			= (uint8_t)(ReadConfig32(MakeAddr(Bus, Slot, Function), 0x08) >> 24);
		this->Subclass		= (uint8_t)(ReadConfig32(MakeAddr(Bus, Slot, Function), 0x08) >> 16);

		this->Address		= MakeAddr(Bus, Slot, Function);
	}






	uint8_t PCIDevice::GetBus() { return this->Bus;	}
	uint8_t PCIDevice::GetSlot() { return this->Slot; }
	uint8_t PCIDevice::GetFunction() { return this->Function; }

	uint8_t PCIDevice::GetClass() { return this->Class; }
	uint8_t PCIDevice::GetSubclass() { return this->Subclass; }
	uint16_t PCIDevice::GetVendorID() { return this->VendorID; }
	uint16_t PCIDevice::GetDeviceID() { return this->DeviceID; }
	uint32_t PCIDevice::GetAddress() { return this->Address; }

	bool PCIDevice::GetIsMultifunction() { return GetHeaderType() & (1 << 7); }

	uint8_t PCIDevice::GetHeaderType()
	{
		return (uint8_t)(HAL::Devices::PCI::ReadConfig32(this->Address, 0x0C) >> 16);
	}

	uint32_t PCIDevice::GetRegisterData(uint16_t Offset, uint8_t FirstBit, uint8_t Length)
	{
		// Write the address to read.
		outl(0xCF8, this->Address + Offset);

		// Read the shit inside.

		uint32_t Data = inl(0xCFC);
		Data >>= FirstBit;

		// Truncate accordingly
		switch(Length)
		{
			case sizeof(uint8_t):
				return (uint8_t)Data;

			case sizeof(uint16_t):
				return (uint16_t)Data;

			case sizeof(uint32_t):
				return Data;
		}

		return (inl(0xCFC));
	}
};
}
}
}




