// NameSpace.h
// Copyright (c) 2011-2013, <zhiayang@gmail.com>
// Licensed under Creative Commons Attribution 3.0 Unported.

#pragma once
#ifndef namespace_h
#define namespace_h

#include <stdint.h>
#include <stdarg.h>
#include <stdbool.h>



#define size_t		uint64_t
typedef __uint128_t uint128_t;
typedef __int128_t int128_t;


// A bunch of error codes.

#define ERR_Undefined			"Undefined error"
#define ERR_AssertFailed		"ASSERT Failed"
#define ERR_OutOfMemory			"Out of Memory"
#define ERR_NoMemoryMap			"No Memory Map"


#define HeapAddress				0xFFFFFFFFB0000000
#define FPLAddress				0xFFFFFFFF00000000
#define DMABufferStart			0x60000



#define SERIALOUT				1
#define NOOUT					0
#define DEBUG					1
#define SKIPREGDMP				0
#define USEHEAPCHUNK			0



#define BSwap16(x)				__builtin_bswap16(x)
#define BSwap32(x)				__builtin_bswap32(x)
#define BSwap64(x)				__builtin_bswap64(x)




namespace Multiboot
{

	// The Multiboot information.
	struct __attribute__((packed)) Info_type
	{
		uint32_t flags, mem_lower, mem_upper, boot_device, cmdline, mods_count, mods_addr;
		uint32_t num, size, addr, shndx;
		uint32_t mmap_length, mmap_addr;
	};

	struct __attribute__((packed)) Module_type { uint32_t mod_start, mod_end, string, reserved; };
	struct __attribute__((packed)) MemoryMap_type
	{
		uint32_t Size;
		uint32_t BaseAddr_Low;
		uint32_t BaseAddr_High;
		uint32_t Length_Low;
		uint32_t Length_High;
		uint32_t Type;
	};
}

namespace Library { namespace Types { class List; class Queue; } }
namespace Kernel
{
	constexpr const char* K_BinaryUnits[] =
	{
		"Bytes",
		"KB",
		"MB",
		"GB",
		"TB",
		"PB",
		"EB",
		"ZB",
		"YB"
	};
	struct __attribute__((packed)) MemoryMapEntry_type
	{
		uint64_t BaseAddress;
		uint64_t Length;
		uint8_t MemoryType;

	};

	struct __attribute__((packed)) MemoryMap_type
	{
		uint16_t NumberOfEntries;
		uint32_t SizeOfThisStructure;

		MemoryMapEntry_type Entries[512];
	};

	namespace HAL
	{
		struct __attribute__((packed)) HAL_ISRRegs
		{
			uint64_t cr2;
			uint64_t rdi, rsi, rbp, rsp;
			uint64_t rax, rbx, rcx, rdx, r8, r9, r10, r11, r12, r13, r14, r15;

			uint64_t InterruptID, ErrorCode;
			uint64_t rip, cs, rflags, useresp, ss;
		};

		namespace Interrupts
		{

			void SetGate(uint8_t num, uint64_t base, uint16_t sel, uint8_t flags);
			void Initialise();
			void InstallDefaultHandlers();
			void InstallIRQHandler(int irq, void (*Handler)(HAL_ISRRegs *r));
			void UninstallIRQHandler(int irq);
		}



		namespace PMem
		{
			struct __attribute__((packed)) TBLPair
			{
				uint64_t BaseAddr;
				uint64_t LengthInPages;

			};

			struct __attribute__((packed)) TFPL
			{
				TBLPair Pairs[256];

			};
			void Initialise();
			void DBG_PrintFPLs();
			void InitialiseFPLs(MemoryMap_type *MemoryMap);

			uint64_t AllocateViaPlacement();
			uint64_t AllocatePage_NoMap();

			void FreePage_NoUnmap(uint64_t Page);

			bool IsOutOfMemory();
			void CoalesceFPLs();
			uint64_t AllocateFromReserved();
		}

		namespace VMem
		{
			struct __attribute__((packed)) TPML
			{
				uint64_t Entry[512];

			};


			#define I_Present				0x01
			#define I_ReadWrite				0x02
			#define I_UserAccess			0x04
			#define I_AlignMask				(~0xFFF)




			void Initialise();
			void SwitchPML4T(TPML *PML4T);
			bool MapAddr(uint64_t VirtAddr, uint64_t PhysAddr, uint64_t Flags);
			void UnMapAddr(uint64_t VirtAddr);
			void MapRegion(uint64_t VirtAddr, uint64_t PhysAddr, uint64_t LengthInPages, uint64_t Flags);
			void UnMapRegion(uint64_t VirtAddr, uint64_t LengthInPages);
			uint64_t GetMapping(uint64_t VirtAddr);
			uint64_t SearchPhysicalMapping(uint64_t PhysAddr);

			bool GetPagingFlag();
			void ActivateDebug();

			void MapAddr(uint64_t virtual_start, uint64_t virtual_end, uint64_t physical_start, uint32_t x);
		}

		namespace DMem
		{
			struct __attribute__((packed)) HeapHeader_type
			{
				// Magic first, so it's easier to spot
				uint32_t Magic;

				union
				{
					uint32_t IsFree : 1;
					uint32_t Flags : 31;
				};
				uint64_t Size;
				HeapHeader_type* PreviousHeader;

			};


			void Initialise();
			void* AllocateChunk(uint64_t Size);
			void FreeChunk(void* Pointer);


			namespace HeapChunk
			{
				void InitialiseChunks();
				uint64_t FindFirstFreeSlot(uint64_t Size);
				uint64_t ExpandHeap(uint64_t PagesToAdd);
				uint64_t ContractHeap(uint64_t PagesToContract);
				uint64_t GetHeapSizeInPages();
				uint64_t SplitChunk(HeapHeader_type* Header, uint64_t Size);
				HeapHeader_type* GetFirstHeader();
				void DBG_PrintChunks();
				uint64_t AlignToPowerOfTwo(uint64_t num);
				uint64_t QuerySize(void* Address);
			}

			namespace BinaryTree
			{
				void InitialiseTree();
				uint64_t AllocateBlock(uint64_t Size);
			}
		}

		namespace MMap
		{
			#define G_MemoryTypeAvailable		1
			#define G_MemoryTypeReserved		2
			#define G_MemoryACPI				3
			#define G_MemoryNVS					4
			#define G_MemoryBadRam				5

			bool IsMemoryValid(uint64_t Address);
			void GetGlobalMemoryMap(MemoryMap_type *MemoryMap);
			uint64_t GetTotalSystemMemory();
			uint64_t CheckForMoreAvailableMemory(uint64_t CurrentAddr);
		}

		namespace PIT
		{
			void SetTimerHertz(int hz);
			extern "C" void _HAL_PITTimerHandler(HAL_ISRRegs *r);
		}

		namespace Drivers
		{
			class Keyboard
			{
				public:
					virtual uint64_t ReadChar() = 0;
					virtual void HandleKeypress() = 0;
					virtual void DecodeScancode() = 0;
					virtual void Disable() = 0;
					virtual void Enable() = 0;
					virtual bool GetState() = 0;
			};

			class PS2Keyboard : public Keyboard
			{
				public:
					Keyboard::PS2Keyboard();

					// PS/2 Scancodes
					static constexpr uint32_t ScanCode2_US[2 * 132] =
					{
						0,	0,		//			0
						0,	0,		// f9		1
						0,	0,		// 			2
						0,	0,		// f5		3
						0,	0,		// f3		4
						0,	0,		// f1		5
						0,	0,		// f2		6
						0,	0,		// f12		7
						0,	0,		// 			8
						0,	0,		// f10		9
						0,	0,		// f8		10
						0,	0,		// f6		11
						0,	0,		// f4		12
						'\t', '\t',	// \t		13
						'`', '`',	// `		14
						0,	0,		//			15
						0,	0,		//			16
						0,	0,		// lalt		17
						0,	0,		// lshft	18
						0,	0,		// 			19
						0,	0,		// lctrl	20
						'q', 'Q',	// q		21
						'1', '!',	// 1		22
						0,	0,		// 			23
						0,	0,		//			24
						0,	0,		//			25
						'z', 'Z',	// z		26
						's', 'S',	// s		27
						'a', 'A',	 // a		28
						'w', 'W',	// w		29
						'2', '@',	// 2		30
						0,	0,		// 			31
						0,	0,		//			32
						'c', 'C',	// c		33
						'x', 'X',	// x		34
						'd', 'D',	// d		35
						'e', 'E',	// e		36
						'4', '$',	// 4		37
						'3', '#',	// 3		38
						0,	0,		//			39
						0,	0,		//			40
						' ', ' ',	// spce		41
						'v', 'V',	// v		42
						'f', 'F',	// f		43
						't', 'T',	// t		44
						'r', 'R',	// r		45
						'5', '%',	// 5		46
						0,	0,		//			47
						0,	0,		//			48
						'n', 'N',	// n		49
						'b', 'B',	// b		50
						'h', 'H',	// h		51
						'g', 'G',	// g		52
						'y', 'Y',	// y		53
						'6', '^',	// 6		54
						0,	0,		// 			55
						0,	0,		// 			56
						0,	0,		//			57
						'm', 'M',	// m		58
						'j', 'J',	// j		59
						'u', 'U',	// u		60
						'7', '&',	// 7		61
						'8', '*',	// 8		62
						0,	0,		// 			63
						0,	0,		// 			64
						',', '<',	// ,		65
						'k', 'K',	// k		66
						'i', 'I',	// i		67
						'o', 'O',	// o		68
						'0', ')',	// 0		69
						'9', '(',	// 9		70
						0,	0,		// 			71
						0,	0,		// 			72
						'.', '>',	// .		73
						'/', '?',	// /		74
						'l', 'L',	// l		75
						';', ':',	// ;		76
						'p', 'P',	// p		77
						'-', '_',	// -		78
						0,	0,		//			79
						0,	0,		//			80
						0,	0,		//			81
						'\'', '"',	// '		82
						0,	0,		//			83
						'[', '{',	// [		84
						'=', '+',	// =		85
						0,	0,		//			86
						0,	0,		//			87
						0,	0,		// caps		88
						0,	0,		// rshft	89
						'\n', '\n',	// enter	90
						']', '}',	// ]		91
						0,	0,		// 			92
						'\\', '|',	// \		93
						0,	0,		//			94
						0,	0,		//			95
						0,	0,		//			96
						0,	0,		//			97
						0,	0,		//			98
						0,	0,		//			99
						0,	0,		//			100
						0,	0,		//			101
						'\b', '\b',	// back		102
						0,	0,		//			103
						0,	0,		//			104
						'1', '!',	// k1		105
						0,	0,		//			106
						'4', '$',	// k4		107
						'7', '&',	// k7		108
						0,	0,		//			109
						0,	0,		//			110
						0,	0,		//			111
						'0', ')',	// k0		112
						'.', '>',	// k.		113
						'2', '@',	// k2		114
						'5', '%',	// k5		115
						'6', '^',	// k6		116
						'8', '*',	// k8		117
						0,	0,		// esc		118
						0,	0,		// nlock	119
						0,	0,		// f11		120
						'+', '+',	// k+		121
						'3', '#',	// k3		122
						'-', '_',	// k-		123
						'*', '*',	// k*		124
						'9', '(',	// k9		125
						0,	0,		// slock	126
						0,	0,		//			127
						0,	0,		//			128
						0,	0,		//			129
						0,	0,		// 			130
						0,	0		// f7		131
					};

					// PS/2 Scancodes, with 0xE0 prefix
					static constexpr uint32_t ScanCode2_US_E0[2 * 128] =
					{
						0,		//			1
						0,		//			2
						0,		//			3
						0,		//			4
						0,		//			5
						0,		//			6
						0,		//			7
						0,		//			8
						0,		//			9
						0,		//			A
						0,		//			B
						0,		//			C
						0,		//			D
						0,		//			E
						0,		//			F
						0,		// MWsearch	10
						0,		// ralt		11
						0,		//			12
						0,		//			13
						0,		// rctrl	14
						0,		// Mprev	15
						0,		// 			16
						0,		// 			17
						0,		// MWfav	18
						0,		// 			19
						0,		// 			1A
						0,		// 			1B
						0,		// 			1C
						0,		// 			1D
						0,		// 			1E
						0,		// lsuper	1F
						0,		// MWrefrsh	20
						0,		// Mvoldown	21
						0,		// 			22
						0,		// Mmute	23
						0,		// 			24
						0,		// 			25
						0,		// 			26
						0,		// rsuper	27
						0,		// MWstop	28
						0,		// 			29
						0,		// 			2A
						0,		// Mcalc	2B
						0,		// 			2C
						0,		// 			2D
						0,		// 			2E
						0,		// apps		2F
						0,		// MWfwd	30
						0,		// 			31
						0,		// Mvolup	32
						0,		// 			33
						0,		// Mplaypse	34
						0,		// 			35
						0,		// 			36
						0,		// Apower	37
						0,		// MWback	38
						0,		// 			39
						0,		// MWhome	3A
						0,		// Mstop	3B
						0,		// 			3C
						0,		// 			3D
						0,		// 			3E
						0,		// Asleep	3F
						0,		// Mmcom	40
						0,		// 			41
						0,		// 			42
						0,		// 			43
						0,		// 			44
						0,		// 			45
						0,		// 			46
						0,		// 			47
						0,		// Memail	48
						0,		// 			49
						0,		// k/		4A
						0,		// 			4B
						0,		// 			4C
						0,		// Mnext	4D
						0,		// 			4E
						0,		// 			4F
						0,		// Mselect	50
						0,		// 			51
						0,		// 			52
						0,		// 			53
						0,		// 			54
						0,		// 			55
						0,		// 			56
						0,		// 			57
						0,		// 			58
						0,		// 			59
						0,		// kenter	5A
						0,		// 			5B
						0,		// 			5C
						0,		// 			5D
						0,		// Awake	5E
						0,		// 			5F
						0,		// 			60
						0,		// 			61
						0,		// 			62
						0,		// 			63
						0,		// 			64
						0,		// 			65
						0,		// 			66
						0,		// 			67
						0,		// 			68
						0,		// end		69
						0,		// 			6A
						0,		// left		6B
						0,		// home		6C
						0,		// 			6D
						0,		// 			6E
						0,		// 			6F
						0,		// insert	70
						0,		// delete	71
						0,		// down		72
						0,		// 			73
						0,		// right	74
						0,		// up		75
						0,		// 			76
						0,		// 			77
						0,		// 			78
						0,		// 			79
						0,		// pagedown	7A
						0,		// 			7B
						0,		// 			7C
						0,		// pageup	7D
					};

					// Ordered array to translate PS/2 Scancodes to USB HID Scancodes
					static constexpr uint32_t ScanCode2_US_PS2_HID[2 * 128] =
					{
						0x00,	// NULL
						0x42,	// F9

						0x00,	// NULL

						0x3E,	// F5
						0x3C,	// F3
						0x3A,	// F1
						0x3B,	// F2
						0x45,	// F12

						0x00,	// NULL

						0x43,	// F10
						0x41,	// F8
						0x3F,	// F6
						0x3D,	// F4

						0x2B,	// Tab
						0x35,	// `

						0x00,	// NULL
						0x00,	// NULL

						0xE1,	// LeftShift
						0xE2,	// LeftAlt

						0x00,	// NULL

						0xE0,	// LeftControl
						0x14,	// Q
						0x1E,	// 1

						0x00,	// NULL
						0x00,	// NULL
						0x00,	// NULL
						0x1D,	// Z
						0x16,	// S
						0x04,	// A
						0x1A,	// W
						0x1F,	// 2

						0x00,	// NULL
						0x00,	// NULL

						0x06,	// C
						0x1B,	// X
						0x07,	// D
						0x08,	// E
						0x21,	// 4
						0x20,	// 3

						0x00,	// NULL
						0x00,	// NULL

						0x2C,	// Space
						0x19,	// V
						0x09,	// F
						0x17,	// T
						0x15,	// R
						0x22,	// 5

						0x00,	// NULL
						0x00,	// NULL

						0x11,	// N
						0x05,	// B
						0x0B,	// H
						0x0A,	// G
						0x1C,	// Y
						0x23,	// 6

						0x00,	// NULL
						0x00,	// NULL
						0x00,	// NULL

						0x10,	// M
						0x0D,	// J
						0x18,	// U
						0x24,	// 7
						0x25,	// 8

						0x00,	// NULL
						0x00,	// NULL

						0x36,	// ,
						0x0E,	// K
						0x0C,	// I
						0x12,	// O
						0x27,	// 0
						0x26,	// 9

						0x00,	// NULL
						0x00,	// NULL

						0x37,	// .
						0x38,	// /
						0x0F,	// L
						0x33,	// ;
						0x13,	// P
						0x2D,	// -

						0x00,	// NULL
						0x00,	// NULL
						0x00,	// NULL

						0x34,	// '

						0x00,	// NULL

						0x2F,	// [
						0x2E,	// =

						0x00,	// NULL
						0x00,	// NULL

						0x39,	// CapsLock
						0xE5,	// RightShift
						0x28,	// Enter
						0x30,	// ]

						0x00,	// NULL

						0x31,	// '\'

						0x00,	// NULL
						0x00,	// NULL
						0x00,	// NULL
						0x00,	// NULL
						0x00,	// NULL
						0x00,	// NULL
						0x00,	// NULL
						0x00,	// NULL

						0x2A,	// Backspace

						0xA5,	// NULL
						0xA6,	// NULL

						0x59,	// Keypad 1

						0xA7,	// NULL

						0x5C,	// Keypad 4
						0x5F,	// Keypad 7

						0xA8,	// NULL
						0xA9,	// NULL
						0xAA,	// NULL

						0x62,	// Keypad 0
						0x63,	// Keypad .
						0x5A,	// Keypad 2
						0x5D,	// Keypad 5
						0x60,	// Keypad 8
						0x5E,	// Keypad 6
						0x29,	// Escape
						0x53,	// NumLock
						0x44,	// F11
						0x57,	// Keypad +
						0x5B,	// Keypad 3
						0x56,	// Keypad -
						0x55,	// Keypad *
						0x61,	// Keypad 9
						0x47,	// ScrollLock

						0xAB,	// NULL
						0xAC,	// NULL
						0xAD,	// NULL
						0xAE,	// NULL

						0x40	// F7
					};

					// Ordered array to translate PS/2 Scancodes (With 0xE0 prefix) to USB HID Scancodes
					static constexpr uint32_t ScanCode2_US_E0_HID[2 * 256] =
					{
						0x00,	// NULL
						0x00,	// NULL
						0x00,	// NULL
						0x00,	// NULL
						0x00,	// NULL
						0x00,	// NULL
						0x00,	// NULL
						0x00,	// NULL
						0x00,	// NULL
						0x00,	// NULL
						0x00,	// NULL
						0x00,	// NULL
						0x00,	// NULL
						0x00,	// NULL
						0x00,	// NULL
						0x00,	// NULL

						0x00,	// MultimediaSearch
						0xE6,	// RightAlt

						0x00,	// NULL
						0x00,	// NULL

						0xE4,	// RightControl
						0x00,	// MultimediaPrev

						0x00,	// NULL
						0x00,	// NULL

						0x00,	// MultimediaWebFav

						0x00,	// NULL
						0x00,	// NULL
						0x00,	// NULL
						0x00,	// NULL
						0x00,	// NULL
						0x00,	// NULL

						0xE3,	// LeftSuper
						0x00,	// MultimediaWebRefresh
						0x00,	// MultimediaVolDown

						0x00,	// NULL

						0x00,	// MultimediaMute

						0x00,	// NULL
						0x00,	// NULL
						0x00,	// NULL

						0xE7,	// RightSuper
						0x00,	// MultimediaWebStop

						0x00,	// NULL
						0x00,	// NULL

						0x00,	// MultimediaCalc

						0x00,	// NULL
						0x00,	// NULL
						0x00,	// NULL

						0x65,	// App
						0x00,	// MultimediaWebForward

						0x00,	// NULL

						0x00,	// MultimediaVolUp

						0x00,	// NULL

						0x00,	// MultimediaPlayPause

						0x00,	// NULL
						0x00,	// NULL

						0x00,	// ACPIPower
						0x00,	// MultimediaWebBack

						0x00,	// NULL

						0x00,	// MultimediaWebHome
						0x00,	// MultimediaStop

						0x00,	// NULL
						0x00,	// NULL
						0x00,	// NULL

						0x00,	// ACPISleep
						0x00,	// MultimediaMyComputer

						0x00,	// NULL
						0x00,	// NULL
						0x00,	// NULL
						0x00,	// NULL
						0x00,	// NULL
						0x00,	// NULL
						0x00,	// NULL

						0x00,	// MultimediaEmail

						0x00,	// NULL

						0x54,	// Keypad /

						0x00,	// NULL
						0x00,	// NULL

						0x00,	// MultimediaNext

						0x00,	// NULL
						0x00,	// NULL

						0x00,	// MultimediaSelect

						0x00,	// NULL
						0x00,	// NULL
						0x00,	// NULL
						0x00,	// NULL
						0x00,	// NULL
						0x00,	// NULL
						0x00,	// NULL
						0x00,	// NULL
						0x00,	// NULL

						0x58,	// Keypad Enter

						0x00,	// NULL
						0x00,	// NULL
						0x00,	// NULL

						0x00,	// ACPIWake

						0x00,	// NULL
						0x00,	// NULL
						0x00,	// NULL
						0x00,	// NULL
						0x00,	// NULL
						0x00,	// NULL
						0x00,	// NULL
						0x00,	// NULL
						0x00,	// NULL
						0x00,	// NULL

						0x4D,	// End

						0x00,	// NULL

						0x50,	// LeftArrow
						0x4A,	// Home

						0x00,	// NULL
						0x00,	// NULL
						0x00,	// NULL

						0x49,	// Insert
						0x4C,	// Delete
						0x51,	// DownArrow

						0x00,	// NULL

						0x4F,	// RightArrow
						0x52,	// UpArrow

						0x00,	// NULL
						0x00,	// NULL
						0x00,	// NULL
						0x00,	// NULL

						0x4E,	// PageDown

						0x00,	// NULL
						0x00,	// NULL

						0x4B,	// PageUp
					};

					static constexpr uint32_t ScanCode2_US_E0_F0[2 * 128] =
					{
						0,		// 				0
						0,		// 				1
						0,		// 				2
						0,		// 				3
						0,		// 				4
						0,		// 				5
						0,		// 				6
						0,		// 				7
						0,		// 				8
						0,		// 				9
						0,		// 				A
						0,		// 				B
						0,		// 				C
						0,		// 				D
						0,		// 				E
						0,		// 				F
						0,		// R-MWsearch	10
						0,		// R-ralt		11
						0,		// 				12
						0,		// 				13
						0,		// R-rctrl		14
						0,		// R-Mprev		15
						0,		// 				16
						0,		// 				17
						0,		// R-MWfav		18
						0,		// 				19
						0,		// 				1A
						0,		// 				1B
						0,		// 				1C
						0,		// 				1D
						0,		// 				1E
						0,		// R-lsuper		1F
						0,		// R-MWrefrsh	20
						0,		// R-Mvoldown	21
						0,		// 				22
						0,		// R-Mmute		23
						0,		// 				24
						0,		// 				25
						0,		// 				26
						0,		// 				27
						0,		// R-MWstop		28
						0,		// 				29
						0,		// 				2A
						0,		// R-Mcalc		2B
						0,		// 				2C
						0,		// 				2D
						0,		// 				2E
						0,		// R-apps		2F
						0,		// R-MWfwd		30
						0,		// 				31
						0,		// R-Mvolup		32
						0,		// 				33
						0,		// R-Mply/pse	34
						0,		// 				35
						0,		// 				36
						0,		// R-Apower		37
						0,		// R-MWback		38
						0,		// 				39
						0,		// R-MWhome		3A
						0,		// R-Mstop		3B
						0,		// 				3C
						0,		// 				3D
						0,		// 				3E
						0,		// R-Asleep		3F
						0,		// R-Mmycom		40
						0,		// 				41
						0,		// 				42
						0,		// 				43
						0,		// 				44
						0,		// 				45
						0,		// 				46
						0,		// 				47
						0,		// R-Memail		48
						0,		// 				49
						0,		// R-k/			4A
						0,		// 				4B
						0,		// 				4C
						0,		// R-Mnext		4D
						0,		// 				4E
						0,		// 				4F
						0,		// R-Mselect	50
						0,		// 				51
						0,		// 				52
						0,		// 				53
						0,		// 				54
						0,		// 				55
						0,		// 				56
						0,		// 				57
						0,		// 				58
						0,		// 				59
						0,		// R-kenter		5A
						0,		// 				5B
						0,		// 				5C
						0,		// 				5D
						0,		// R-Awake		5E
						0,		// 				5F
						0,		// 				60
						0,		// 				61
						0,		// 				62
						0,		// 				63
						0,		// 				64
						0,		// 				65
						0,		// 				66
						0,		// 				67
						0,		// 				68
						0,		// R-end		69
						0,		// 				6A
						0,		// R-left		6B
						0,		// R-home		6C
						0,		// 				6D
						0,		// 				6E
						0,		// 				6F
						0,		// R-insert		70
						0,		// R-delete		71
						0,		// R-down		72
						0,		// 				73
						0,		// R-right		74
						0,		// R-up			75
						0,		// 				76
						0,		// 				77
						0,		// 				78
						0,		// 				79
						0,		// R-pagedown	7A
						0,		//				7B
						0,		// 				7C
						0		// R-pageup		7D
					};

					static constexpr uint32_t USB_HID[4 * 0xFF] =
					{
						0x81,	0,		0,		0,		// SystemPower
						0x82,	0,		0,		0,		// SystemSleep
						0x83,	0,		0,		0,		// SystemWake
						0x00,	0,		0,		0,		// Nothing
						0x01,	0,		0,		0,		// OverrunError
						0x02,	0,		0,		0,		// POSTError
						0x03,	0,		0,		0,		// Undefined
						0x04,	'a',	'A',	0,		// A
						0x05,	'b',	'B',	0,		// B
						0x06,	'c',	'C',	0,		// C
						0x07,	'd',	'D',	0,		// D
						0x08,	'e',	'E',	0,		// E
						0x09,	'f',	'F',	0,		// F
						0x0A,	'g',	'G',	0,		// G
						0x0B,	'h',	'H',	0,		// H
						0x0C,	'i',	'I',	0,		// I
						0x0D,	'j',	'J',	0,		// J
						0x0E,	'k',	'K',	0,		// K
						0x0F,	'l',	'L',	0,		// L
						0x10,	'm',	'M',	0,		// M
						0x11,	'n',	'N',	0,		// N
						0x12,	'o',	'O',	0,		// O
						0x13,	'p',	'P',	0,		// P
						0x14,	'q',	'Q',	0,		// Q
						0x15,	'r',	'R',	0,		// R
						0x16,	's',	'S',	0,		// S
						0x17,	't',	'T',	0,		// T
						0x18,	'u',	'U',	0,		// U
						0x19,	'v',	'V',	0,		// V
						0x1A,	'w',	'W',	0,		// W
						0x1B,	'x',	'X',	0,		// X
						0x1C,	'y',	'Y',	0,		// Y
						0x1D,	'z',	'Z',	0,		// Z
						0x1E,	'1',	'!',	0,		// 1
						0x1F,	'2',	'@',	0,		// 2
						0x20,	'3',	'#',	0,		// 3
						0x21,	'4',	'$',	0,		// 4
						0x22,	'5',	'%',	0,		// 5
						0x23,	'6',	'^',	0,		// 6
						0x24,	'7',	'&',	0,		// 7
						0x25,	'8',	'*',	0,		// 8
						0x26,	'9',	'(',	0,		// 9
						0x27,	'0',	')',	0,		// 0
						0x28,	'\n',	'\n',	0,		// Enter
						0x29,	0,		0,		0,		// Escape
						0x2A,	'\b',	'\b',	0,		// Backspace
						0x2B,	'\t',	'\t',	0,		// Tab
						0x2C,	' ',	' ',	0,		// Space
						0x2D,	'-',	'_',	0,		// -
						0x2E,	'=',	'+',	0,		// =
						0x2F,	'[',	'{',	0,		// [
						0x30,	']',	'}',	0,		// ]
						0x31,	'\\',	'|',	0,		// '\'
						0x32,	0,		0,		0,		// Europe1
						0x33,	';',	':',	0,		// ;
						0x34,	'\'',	'"',	0,		// '
						0x35,	'`',	'~',	0,		// `
						0x36,	',',	'<',	0,		// ,
						0x37,	'.',	'>',	0,		// .
						0x38,	'/',	'?',	0,		// /
						0x39,	0,		0,		0,		// CapsLock
						0x3A,	0,		0,		0,		// F1
						0x3B,	0,		0,		0,		// F2
						0x3C,	0,		0,		0,		// F3
						0x3D,	0,		0,		0,		// F4
						0x3E,	0,		0,		0,		// F5
						0x3F,	0,		0,		0,		// F6
						0x40,	0,		0,		0,		// F7
						0x41,	0,		0,		0,		// F8
						0x42,	0,		0,		0,		// F9
						0x43,	0,		0,		0,		// F10
						0x44,	0,		0,		0,		// F11
						0x45,	0,		0,		0,		// F12
						0x46,	0,		0,		0,		// PrintScreen
						0x47,	0,		0,		0,		// ScrollLock
						0x48,	0,		0,		0,		// Break/Pause

						0x49,	0,		0,		0,		// Insert
						0x4A,	0,		0,		0,		// Home
						0x4B,	0,		0,		0,		// PageUp
						0x4C,	0,		0,		0,		// Delete
						0x4D,	0,		0,		0,		// End
						0x4E,	0,		0,		0,		// PageDown
						0x4F,	0,		0,		0,		// RightArrow
						0x50,	0,		0,		0,		// LeftArrow
						0x51,	0,		0,		0,		// DownArrow
						0x52,	0,		0,		0,		// UpArrow
						0x53,	0,		0,		0,		// NumLock
						0x54,	'/',	'/',	0,		// Keypad /
						0x55,	'*',	'*',	0,		// Keypad *
						0x56,	'-',	'-',	0,		// Keypad -
						0x57,	'+',	'+',	0,		// Keypad +
						0x58,	'\n',	'\n',	0,		// Keypad Enter
						0x59,	'1',	'1',	0,		// Keypad 1
						0x5A,	'2',	'2',	0,		// Keypad 2
						0x5B,	'3',	'3',	0,		// Keypad 3
						0x5C,	'4',	'4',	0,		// Keypad 4
						0x5D,	'5',	'5',	0,		// Keypad 5
						0x5E,	'6',	'6',	0,		// Keypad 6
						0x5F,	'7',	'7',	0,		// Keypad 7
						0x60,	'8',	'8',	0,		// Keypad 8
						0x61,	'9',	'9',	0,		// Keypad 9
						0x62,	'0',	'0',	0,		// Keypad 0
						0x63,	'.',	'.',	0,		// Keypad .
						0x64,	0,		0,		0,		// Europe 2
						0x65,	0,		0,		0,		// App
						0x66,	0,		0,		0,		// KeyboardPower
						0x67,	'=',	'=',	0,		// Keypad =
						0x68,	0,		0,		0,		// F13
						0x69,	0,		0,		0,		// F14
						0x6A,	0,		0,		0,		// F15
						0x6B,	0,		0,		0,		// F16
						0x6C,	0,		0,		0,		// F17
						0x6D,	0,		0,		0,		// F18
						0x6E,	0,		0,		0,		// F19
						0x6F,	0,		0,		0,		// F20
						0x70,	0,		0,		0,		// F21
						0x71,	0,		0,		0,		// F22
						0x72,	0,		0,		0,		// F23
						0x73,	0,		0,		0,		// F24

						0x74,	0,		0,		0,		// KeyboardExe
						0x75,	0,		0,		0,		// KeyboardHelp
						0x76,	0,		0,		0,		// KeyboardMenu
						0x77,	0,		0,		0,		// KeyboardSelect
						0x78,	0,		0,		0,		// KeyboardStop
						0x79,	0,		0,		0,		// KeyboardAgain
						0x7A,	0,		0,		0,		// KeyboardUndo
						0x7B,	0,		0,		0,		// KeyboardCut
						0x7C,	0,		0,		0,		// KeyboardCopy
						0x7D,	0,		0,		0,		// KeyboardPaste
						0x7E,	0,		0,		0,		// KeyboardFind
						0x7F,	0,		0,		0,		// KeybordMute
						0x80,	0,		0,		0,		// KeyboardVolUp
						0x81,	0,		0,		0,		// KeyboardVolDown
						0x82,	0,		0,		0,		// KeyboardCapsLock
						0x83,	0,		0,		0,		// KeyboardNumLock
						0x84,	0,		0,		0,		// KeyboardScrollLock
						0x85,	',',	',',	0,		// Keypad ,
						0x86,	'=',	'=',	0,		// Keyboard =
						0x87,	0,		0,		0,		// NULL
						0x88,	0,		0,		0,		// NULL
						0x89,	0,		0,		0,		// NULL
						0x8A,	0,		0,		0,		// NULL
						0x8B,	0,		0,		0,		// NULL
						0x8C,	0,		0,		0,		// NULL
						0x8D,	0,		0,		0,		// NULL
						0x8E,	0,		0,		0,		// NULL
						0x8F,	0,		0,		0,		// NULL
						0x90,	0,		0,		0,		// NULL
						0x91,	0,		0,		0,		// NULL
						0x92,	0,		0,		0,		// NULL
						0x93,	0,		0,		0,		// NULL
						0x94,	0,		0,		0,		// NULL
						0x95,	0,		0,		0,		// NULL
						0x96,	0,		0,		0,		// NULL
						0x97,	0,		0,		0,		// NULL
						0x98,	0,		0,		0,		// NULL
						0x99,	0,		0,		0,		// KeyboardAltErase
						0x9A,	0,		0,		0,		// KeyboardSysReq
						0x9B,	0,		0,		0,		// KeyboardCancel
						0x9C,	0,		0,		0,		// KeyboardClear
						0x9D,	0,		0,		0,		// KeyboardPrior
						0x9E,	0,		0,		0,		// KeyboardReturn
						0x9F,	0,		0,		0,		// KeyboardSeparator
						0xA0,	0,		0,		0,		// KeyboardOut
						0xA1,	0,		0,		0,		// KeyboardOperate
						0xA2,	0,		0,		0,		// KeyboardClear
						0xA3,	0,		0,		0,		// KeyboardProps
						0xA4,	0,		0,		0,		// KeyboardExSel
						0xA5,	0,		0,		0,		// NULL
						0xA6,	0,		0,		0,		// NULL
						0xA7,	0,		0,		0,		// NULL
						0xA8,	0,		0,		0,		// NULL
						0xA9,	0,		0,		0,		// NULL
						0xAA,	0,		0,		0,		// NULL
						0xAB,	0,		0,		0,		// NULL
						0xAC,	0,		0,		0,		// NULL
						0xAD,	0,		0,		0,		// NULL
						0xAE,	0,		0,		0,		// NULL
						0xAF,	0,		0,		0,		// NULL
						0xB0,	0,		0,		0,		// NULL
						0xB1,	0,		0,		0,		// NULL
						0xB2,	0,		0,		0,		// NULL
						0xB3,	0,		0,		0,		// NULL
						0xB4,	0,		0,		0,		// NULL
						0xB5,	0,		0,		0,		// NULL
						0xB6,	0,		0,		0,		// NULL
						0xB7,	0,		0,		0,		// NULL
						0xB8,	0,		0,		0,		// NULL
						0xB9,	0,		0,		0,		// NULL
						0xBA,	0,		0,		0,		// NULL
						0xBB,	0,		0,		0,		// NULL
						0xBC,	0,		0,		0,		// NULL
						0xBD,	0,		0,		0,		// NULL
						0xBE,	0,		0,		0,		// NULL
						0xBF,	0,		0,		0,		// NULL
						0xC0,	0,		0,		0,		// NULL
						0xC1,	0,		0,		0,		// NULL
						0xC2,	0,		0,		0,		// NULL
						0xC3,	0,		0,		0,		// NULL
						0xC4,	0,		0,		0,		// NULL
						0xC5,	0,		0,		0,		// NULL
						0xC6,	0,		0,		0,		// NULL
						0xC7,	0,		0,		0,		// NULL
						0xC8,	0,		0,		0,		// NULL
						0xC9,	0,		0,		0,		// NULL
						0xCA,	0,		0,		0,		// NULL
						0xCB,	0,		0,		0,		// NULL
						0xCC,	0,		0,		0,		// NULL
						0xCD,	0,		0,		0,		// NULL
						0xCE,	0,		0,		0,		// NULL
						0xCF,	0,		0,		0,		// NULL
						0xD0,	0,		0,		0,		// NULL
						0xD1,	0,		0,		0,		// NULL
						0xD2,	0,		0,		0,		// NULL
						0xD3,	0,		0,		0,		// NULL
						0xD4,	0,		0,		0,		// NULL
						0xD5,	0,		0,		0,		// NULL
						0xD6,	0,		0,		0,		// NULL
						0xD7,	0,		0,		0,		// NULL
						0xD8,	0,		0,		0,		// NULL
						0xD9,	0,		0,		0,		// NULL
						0xDA,	0,		0,		0,		// NULL
						0xDB,	0,		0,		0,		// NULL
						0xDC,	0,		0,		0,		// NULL
						0xDD,	0,		0,		0,		// NULL
						0xDE,	0,		0,		0,		// NULL
						0xDF,	0,		0,		0,		// NULL
						0xE0,	0,		0,		0,		// LeftControl
						0xE1,	0,		0,		0,		// LeftShift
						0xE2,	0,		0,		0,		// LeftAlt
						0xE3,	0,		0,		0,		// LeftSuper
						0xE4,	0,		0,		0,		// RightControl
						0xE5,	0,		0,		0,		// RightShift
						0xE6,	0,		0,		0,		// RightAlt
						0xE7,	0,		0,		0		// RightSuper
					};

					uint64_t Translate(uint64_t kbkey, bool IsE0 = false, bool IsF0 = false);
					uint64_t ReadChar();
					void HandleKeypress();
					void DecodeScancode();
					void Disable() { this->State = false; }
					void Enable() { this->State = true; }
					bool GetState() { return this->State; }

				private:
					bool State;
					Library::Types::Queue* CharBuffer;
					Library::Types::Queue* ByteBuffer;
			};

			class PS2Mouse
			{
			};
		}

		namespace Devices
		{

			namespace BGA
			{

				#define VBE_DISPI_IOPORT_INDEX			0x01CE
				#define VBE_DISPI_IOPORT_DATA			0x01CF
				#define VBE_DISPI_INDEX_ID				0x0
				#define VBE_DISPI_INDEX_XRES			0x1
				#define VBE_DISPI_INDEX_YRES			0x2
				#define VBE_DISPI_INDEX_BPP				0x3
				#define VBE_DISPI_INDEX_ENABLE			0x4
				#define VBE_DISPI_INDEX_BANK			0x5
				#define VBE_DISPI_INDEX_VIRT_WIDTH		0x6
				#define VBE_DISPI_INDEX_VIRT_HEIGHT		0x7
				#define VBE_DISPI_INDEX_X_OFFSET		0x8
				#define VBE_DISPI_INDEX_Y_OFFSET		0x9

				#define VBE_DISPI_DISABLED				0x00
				#define VBE_DISPI_ENABLED				0x01
				#define VBE_DISPI_GETCAPS				0x02
				#define VBE_DISPI_8BIT_DAC				0x20
				#define VBE_DISPI_LFB_ENABLED			0x40
				#define VBE_DISPI_NOCLEARMEM			0x80

				void WriteRegister(unsigned short IndexValue, uint16_t DataValue);
				uint16_t ReadRegister(unsigned short IndexValue);
				uint8_t IsAvailable();
				void SetVideoMode(unsigned int Width, unsigned int Height, unsigned int BitDepth, uint8_t ClearVideoMemory, uint8_t GetCaps);
				uint16_t GetResX();
				uint16_t GetResY();
				void Disable();
			}

			namespace COM1
			{
				void WriteString(const char *String);
				void Initialise();
				void WriteChar(char Ch);
			}

			namespace CPUID
			{
				void PrintCPUInfo(bool simple);
				void IdentIntelCPU(bool simple);
				void IdentAMDCPU(bool simple);
				void PrintCPUIDRegs(uint32_t eax, uint32_t ebx, uint32_t ecx, uint32_t edx);
			}

			namespace LFB
			{
				// Standard colour defines

				// B/W
				#define VC_White		0xFFFFFFFF
				#define VC_Black		0xFF000000

				// Greys
				#define VC_LightGrey	0xFFD3D3D3
				#define VC_Silver		0xFFC0C0C0
				#define VC_DarkGrey		0xFFA9A9A9
				#define VC_Grey			0xFF808080
				#define VC_DimGrey		0xFF696969


				// R/G/B
				#define VC_Red			0xFFFF0000
				#define VC_Green		0xFF00FF00
				#define VC_Blue			0xFF0000FF


				// C/Y/M
				#define VC_Cyan			VC_Green | VC_Blue
				#define VC_Yellow		VC_Green | VC_Red
				#define VC_Magenta		VC_Blue | VC_Red


				// Tertiary colours -- A/V/R/O/C/SG
				#define VC_Azure		0x0007FFFF
				#define VC_Violet		0xFF7F00FF
				#define VC_Rose			0xFFFF007F
				#define VC_Orange		0xFFFF7F00
				#define VC_Chartreuse	0xFF7FFF00
				#define VC_SpringGreen	0xFF00FF7F


				// VGA 16-colour palette
				#define VC_DarkCyan		0xFF00AAAA
				#define VC_Brown		0xFFAA5500
				#define VC_DarkRed		0xFFAA3333





				#define _RED(color) ((color & 0x00FF0000) / 0x10000)
				#define _GRE(color) ((color & 0x0000FF00) / 0x100)
				#define _BLU(color) ((color & 0x000000FF) / 0x1)
				#define _ALP(color) ((color & 0xFF000000) / 0x1000000)



				void Initialise();
				void PutPixelAtAddr(uint32_t pos, uint32_t Colour);
				void PutPixel(uint16_t x, uint16_t y, uint32_t Colour);
				uint32_t BlendPixels(uint32_t bottom, uint32_t top);
				uint32_t GetRGBA(uint8_t r, uint8_t g, uint8_t b, uint8_t a);
				void DrawStraightLine(uint16_t StartX, uint16_t StartY, uint16_t EndX, uint16_t EndY, uint32_t Colour);
				void DrawBox(uint16_t x, uint16_t y, uint16_t width, uint16_t height, uint32_t Colour);
				void DrawFilledBox(uint16_t x, uint16_t y, uint16_t width, uint16_t height, uint32_t Colour);
				void DrawChar(char c, uint16_t x, uint16_t y, uint32_t Colour);
				void DrawLine(double StartX, double StartY, double EndX, double EndY, uint32_t Colour);
				void RefreshBuffer();
				uint16_t GetResX();
				uint16_t GetResY();

				namespace Console
				{
					void Initialise();
					bool IsInitialised();
					char PrintChar(char c);
					void ClearScreen();
					void Scroll();
					void ScrollDown(int lines);
					void Backspace(int characters);
					void SetColour(uint32_t Colour);
					void MoveCursor(int x, int y);
					int GetCharsPerLine();
					int GetCharsPerColumn();
					int GetCharsPerPage();
					int GetCursorX();
					int GetCursorY();
				}
			}

			namespace PCI
			{
				class __attribute__((packed)) PCIDevice
				{

				public:
					PCIDevice(uint16_t Bus, uint16_t Slot, uint8_t Function);

					uint8_t GetBus();
					uint8_t GetSlot();
					uint8_t GetFunction();

					uint8_t GetClass();
					uint8_t GetSubclass();
					uint16_t GetVendorID();
					uint16_t GetDeviceID();
					uint32_t GetAddress();

					bool GetIsMultifunction();

					uint8_t GetHeaderType();
					uint32_t GetRegisterData(uint16_t Offset, uint8_t FirstBit, uint8_t Length);



					static Library::Types::List* PCIDevices;


				private:
					uint8_t Bus;
					uint8_t Slot;
					uint8_t Function;

					uint8_t Class;
					uint8_t Subclass;
					uint16_t VendorID;
					uint16_t DeviceID;

					uint32_t Address;
				};


				#define MAXBUS		255
				#define MAXSLOT		32
				#define MAXFUNC		8

				uint32_t MakeAddr(uint16_t Bus, uint16_t Slot, uint16_t Function);


				uint32_t ReadConfig32(uint32_t Address, uint16_t Offset);
				uint16_t ReadConfig16(uint32_t Address, uint16_t Offset);
				uint8_t ReadConfig8(uint32_t Address, uint16_t Offset);
				void WriteConfig32(uint32_t Address, uint16_t Offset, uint32_t Data);
				void WriteConfig16(uint32_t Address, uint16_t Offset, uint16_t Data);
				void WriteConfig8(uint32_t Address, uint16_t Offset, uint8_t Data);
				uint16_t CheckDeviceExistence(uint16_t Bus, uint16_t Slot);

				PCIDevice* SearchByVendorDevice(uint16_t VendorID, uint16_t DeviceID);
				bool MatchVendorDevice(PCIDevice* dev, uint16_t VendorID, uint16_t DeviceID);
				bool MatchClassSubclass(PCIDevice* dev, uint8_t Class, uint8_t Subclass);
			}

			namespace PIT
			{
				void SetTimerHertz(int hz);
				extern "C" void _HAL_PITTimerHandler(HAL_ISRRegs *r);
			}

			namespace ATA
			{
				class __attribute__((packed)) ATADevice
				{
					public:
						ATADevice(uint8_t Bus, uint8_t Drive);

						bool		IsDMA();
						uint8_t		GetBus();
						uint8_t		GetDrive();

						void		SetSectors(uint64_t s);
						uint64_t	GetSectors();

						void		SetSectorSize(uint32_t SectorSize);
						uint32_t	GetSectorSize();

						uint16_t	GetBaseIO();
						uint8_t		GetDriveNumber();

						bool		GetIsGPT();
						void		SetIsGPT(bool IsGPT);

						void		ReadSector(uint64_t LBA);
						void		WriteSector(uint64_t LBA);

						uint16_t	Data[256];
						Library::Types::List* Partitions;

						static Library::Types::List* ATADevices;

					private:
						bool		DMA;
						uint8_t		Bus;
						uint8_t		Drive;
						uint64_t	MaxSectors;
						uint16_t	SectorSize;
						uint16_t	BaseIO;
						uint8_t		DriveNumber;
						bool		IsGPT;
				};



				namespace PIO
				{

					// go modify dev->Data before calling this
					void ReadSector(ATADevice* dev, uint64_t Sector);
					void WriteSector(ATADevice* dev, uint64_t Sector);

					extern "C" void IRQHandler14(HAL_ISRRegs* r);
					extern "C" void IRQHandler15(HAL_ISRRegs* r);

					bool GetIsWaiting14();
					bool GetIsWaiting15();
				}

				void PrintATAInfo(ATADevice* ata);
				void IdentifyAll();
				ATADevice* IdentifyDevice(uint16_t BaseIO, bool IsMaster);



				#define PrimaryBaseIO			0x1F0
				#define SecondaryBaseIO			0x170
				#define TertiaryBaseIO			0x1E8
				#define QuaternaryBaseIO		0x168

				#define PrimaryControl			0x3F6
				#define SecondaryControl		0x376
				#define TertiaryControl			0x3E6
				#define QuaternaryControl		0x366

				#define PrimaryCommand			PrimaryBaseIO + 7
				#define SecondaryCommand		SecondaryBaseIO + 7
				#define TertiaryCommand			TertiaryBaseIO + 7
				#define QuaternaryCommand		QuaternaryBaseIO + 7


				#define ATA_Identify			0xEC
				#define ATA_ReadSectors28		0x20
				#define ATA_ReadSectors48		0x24

				#define ATA_WriteSectors28		0x30
				#define ATA_WriteSectors48		0x34
			}

			namespace PS2
			{
				class __attribute__((packed)) PS2Controller
				{
					public:
						PS2Controller();

						static const uint8_t DataPort = 0x60;
						static const uint8_t CommandPort = 0x64;

						static uint8_t Device1Buffer;
						static uint8_t Device2Buffer;


					private:
						static void HandleIRQ1(Kernel::HAL::HAL_ISRRegs* r);
						static void HandleIRQ12(Kernel::HAL::HAL_ISRRegs* r);
				};
			}
		}


		namespace FS
		{
			enum FSTypes
			{
				// FAT family
				exFAT		= 0xF1EF,
				fat32		= 0xF132,
				fat16		= 0xF116,
				fat12		= 0xF112,

				// Apple
				hfsplus		= 0x11F5,
				hfs			= 0x11F0,

				// Linux
				ext2		= 0xEB12,
				ext3		= 0xEB13,
				ext4		= 0xEB14
			};


			class Filesystem;
			class __attribute__((packed)) Partition
			{
				public:
					Partition(Devices::ATA::ATADevice* Drive, uint8_t num, uint64_t StartLBA, uint64_t LBALength, uint16_t Type, uint64_t PartitionGUID_high, uint64_t PartitionGUID_low, uint64_t TypeGUID_high, uint64_t TypeGUID_low, char Name[37], bool Bootable);
					void		PrintInfo();

					uint64_t	GetStartLBA();
					uint64_t	GetLBALength();
					uint16_t	GetType();

					uint64_t	GetGUID_S1();
					uint64_t	GetGUID_S2();
					uint64_t	GetGUID_S3();
					uint64_t	GetGUID_S4();
					uint64_t	GetGUID_S5();


					uint64_t	GetGUID_High();
					uint64_t	GetGUID_Low();



					uint64_t	GetTypeGUID_S1();
					uint64_t	GetTypeGUID_S2();
					uint64_t	GetTypeGUID_S3();
					uint64_t	GetTypeGUID_S4();
					uint64_t	GetTypeGUID_S5();



					uint64_t	GetTypeGUID_High();
					uint64_t	GetTypeGUID_Low();
					char*		GetName();
					bool		IsBootable();
					Devices::ATA::ATADevice* GetDrive();
					uint8_t		GetPartitionNumber();
					Filesystem*	GetFilesystem();

					void		ReadSector(uint64_t LBA);
					void		WriteSector(uint64_t LBA);

				private:
					uint64_t	StartLBA;
					uint64_t	LBALength;
					uint16_t	PartitionType;		// Our internal ones are of 0xZZZZ
					uint64_t	PartitionGUID_high;
					uint64_t	PartitionGUID_low;

					uint64_t	PartitionTypeGUID_high;
					uint64_t	PartitionTypeGUID_low;


					char		Name[37];
					bool		Bootable;
					Devices::ATA::ATADevice* Drive;
					uint8_t		PartitionNumber;

					Filesystem*	Filesystem;
			};

			class __attribute__((packed)) Filesystem
			{
				public:
					Filesystem(Partition* parent, FSTypes type);
					void		PrintInfo();
					FSTypes		Type;
					Partition*	GetPartition();

				protected:
					Partition*	ParentPartition;
			};

			class __attribute__((packed)) FAT32 : public Filesystem
			{
				public:
					struct __attribute__((packed)) File_type
					{
						char Name[8];
						char Extension[3];

						union
						{
							struct
							{
								uint8_t Reserved : 1;
								uint8_t Device : 1;
								uint8_t Archive : 1;
								uint8_t IsSubdirectory : 1;
								uint8_t IsVolumeLabel : 1;
								uint8_t IsSystemFile : 1;
								uint8_t IsHiddenFile : 1;
								uint8_t IsReadOnly : 1;
							};
							uint8_t RawAttrib : 8;
						};

						uint8_t UserAttributes;
						uint8_t CreationTimeMiliseconds;

						union
						{
							struct
							{
								uint16_t CTimeHours : 5;
								uint16_t CTimeMinutes : 6;
								uint16_t CTimeDoubleSec : 5;
							};
							uint16_t RawCTime : 16;
						};

						union
						{
							struct
							{
								// epoch for windows is 1980
								uint16_t CDateYear : 7;
								uint16_t CDateMonth : 4;
								uint16_t CDateDate : 5;
							};
							uint16_t RawCDate : 16;
						};

						uint16_t Unused;
						uint16_t HighCluster;

						union
						{
							struct
							{
								uint16_t MTimeHours : 5;
								uint16_t MTimeMinutes : 6;
								uint16_t MTimeDoubleSec : 5;
							};
							uint16_t RawMTime : 16;
						};

						union
						{
							struct
							{
								// epoch for windows is 1980
								uint16_t MDateYear : 7;
								uint16_t MDateMonth : 4;
								uint16_t MDateDate : 5;
							};
							uint16_t RawMDate : 16;
						};

						uint16_t LowCluster;
						uint32_t FileSize;

					};

					FAT32(Partition* Parent);

					Partition*	GetPartition();
					uint16_t	GetBytesPerSector();
					uint8_t		GetSectorsPerCluster();
					uint16_t	GetReservedSectors();
					uint8_t		GetNumberOfFATS();
					uint16_t	GetNumberOfDirectories();

					uint32_t	GetTotalSectors();
					uint32_t	GetHiddenSectors();
					uint32_t	GetFATSectorSize();
					uint32_t	GetRootDirectoryCluster();

					uint16_t	GetFSInfoCluster();
					uint16_t	GetBackupBootCluster();
					uint32_t	GetFirstUsableCluster();

					void		ListFiles();
					File_type*	SearchFile(uint32_t Cluster, const char* name, bool Recursive = true, bool Verbose = false);
					char*		GetFileName(char* filename);
					char*		GetFolderName(char* foldername);

					void		ReadFile(File_type* File, uint64_t Address);

				private:
					Partition* partition;
					uint16_t BytesPerSector;
					uint8_t SectorsPerCluster;
					uint16_t ReservedSectors;
					uint8_t NumberOfFATs;
					uint16_t NumberOfDirectories;

					uint32_t TotalSectors;
					uint32_t HiddenSectors;

					uint32_t FATSectorSize;
					uint32_t RootDirectoryCluster;
					uint16_t FSInfoCluster;
					uint16_t BackupBootCluster;


					uint64_t FirstUsableCluster;

			};
			/*
			class __attribute__((packed)) HFSPlus : public Filesystem
			{
				public:
					HFSPlus(Partition* parent);


				private:
					struct __attribute__((packed)) HFSPlusExtentDescriptor
					{
						uint32_t				StartBlock;
						uint32_t				BlockCount;
					};

					typedef HFSPlusExtentDescriptor HFSPlusExtentRecord[8];

					struct __attribute__((packed)) HFSPlusForkData
					{
						uint64_t				LogicalSize;
						uint32_t				ClumpSize;
						uint32_t				TotalBlocks;
						HFSPlusExtentRecord		Extents;
					};
					struct __attribute__((packed)) HFSPlusBSDInfo
					{
						uint32_t				ownerID;
						uint32_t				groupID;
						uint8_t					adminFlags;
						uint8_t					ownerFlags;
						uint16_t				fileMode;
						union
						{
							uint32_t			iNodeNum;
							uint32_t			linkCount;
							uint32_t			rawDevice;
						} special;
					};
					struct __attribute__((packed)) Point
					{
						int16_t					v;
						int16_t					h;
					};
					typedef uint32_t			FourCharCode;
					typedef FourCharCode		OSType;
					struct __attribute__((packed)) FileInfo
					{
						OSType					fileType;			// The type of the file
						OSType					fileCreator;		// The file's creator
						uint16_t				finderFlags;
						Point					location;			// File's location in the folder.
						uint16_t				reservedField;
					};

					struct __attribute__((packed)) ExtendedFileInfo
					{
						int16_t					reserved1[4];
						uint16_t				extendedFinderFlags;
						int16_t					reserved2;
						int32_t					putAwayFolderID;
					};

					struct __attribute__((packed)) HFSPlusCatalogFile
					{
						int16_t					recordType;
						uint16_t				flags;
						uint32_t				reserved1;
						uint32_t				fileID;
						uint32_t				createDate;
						uint32_t				contentModDate;
						uint32_t				attributeModDate;
						uint32_t				accessDate;
						uint32_t				backupDate;
						HFSPlusBSDInfo			permissions;
						FileInfo				userInfo;
						ExtendedFileInfo		finderInfo;
						uint32_t				textEncoding;
						uint32_t				reserved2;

						HFSPlusForkData			dataFork;
						HFSPlusForkData			resourceFork;
					};

					struct __attribute__((packed)) BTHeaderRec
					{
						uint16_t				treeDepth;
						uint32_t				rootNode;
						uint32_t				leafRecords;
						uint32_t				firstLeafNode;
						uint32_t				lastLeafNode;
						uint16_t				nodeSize;
						uint16_t				maxKeyLength;
						uint32_t				totalNodes;
						uint32_t				freeNodes;
						uint16_t				reserved1;
						uint32_t				clumpSize;			// misaligned
						uint8_t			 		btreeType;
						uint8_t					keyCompareType;
						uint32_t				attributes;			// long aligned again
						uint32_t				reserved3[16];
					};

					struct __attribute__((packed)) HFSPlusVolumeHeader
					{
						uint16_t			Signature;
						uint16_t			Version;
						uint32_t			Attributes;
						uint32_t			LastMountedVersion;
						uint32_t			JournalInfoBlock;

						uint32_t			CreateDate;
						uint32_t			ModifyDate;
						uint32_t			BackupDate;
						uint32_t			CheckedDate;

						uint32_t			FileCount;
						uint32_t			FolderCount;

						uint32_t			BlockSize;
						uint32_t			TotalBlocks;
						uint32_t			FreeBlocks;

						uint32_t			NextAllocation;
						uint32_t			RsrcClumpSize;
						uint32_t			DataClumpSize;
						uint32_t			nextCatalogID;

						uint32_t			WriteCount;
						uint64_t			EncodingsBitmap;

						uint32_t			FinderInfo[8];

						HFSPlusForkData		allocationFile;
						HFSPlusForkData		extentsFile;
						HFSPlusForkData		catalogFile;
						HFSPlusForkData		attributesFile;
						HFSPlusForkData		startupFile;
					};

					uint32_t Attributes;
					uint32_t LastMountedVersion;
					uint32_t JournalInfoBlock;

					uint32_t FileCount;
					uint32_t FolderCount;

					uint32_t BlockSize;
					uint32_t TotalBlocks;
					uint32_t FreeBlocks;

					uint32_t NextAllocation;
					uint32_t DataClumpSize;
					uint32_t NextCatalogID;

					uint32_t WriteCount;
					uint64_t EncodingBitmap;

					uint32_t FinderInfo[8];

					HFSPlusForkData	AllocationFile;
					HFSPlusForkData	ExtentsFile;
					HFSPlusForkData	CatalogFile;
					HFSPlusForkData	AttributesFile;
					HFSPlusForkData	StartupFile;
					Partition* partition;
			};
			*/

			namespace MBR
			{
				void ReadPartitions(Devices::ATA::ATADevice* atadev);
			}
			namespace GPT
			{
				void ReadPartitions(Devices::ATA::ATADevice* atadev);
			}

		}




		// physmem.c
		uint64_t AllocatePage();
		void FreePage(uint64_t PageAddr);
	}

	namespace Internal
	{
		// main.c
		void PrintVersion();
		void DoMultiboot1MemoryMap(Multiboot::Info_type* MBTStruct);
		void PrintKernelMemoryMap();
		void PrintPCIDeviceInfo(uint8_t bus, uint8_t slot, uint16_t vendor, uint16_t device, uint8_t cl, uint8_t subclass, uint8_t headertype, uint8_t func = 0);
		bool PrintPCIDeviceInfo(Kernel::HAL::Devices::PCI::PCIDevice* PCIDevice);
		void StopUsingOffscreenBuffer();
		void StartUsingOffscreenBuffer();

		uint64_t GetLFBAddr();
		uint64_t _GetTrueLFBAddr();
		uint32_t GetLFBLengthInPages();

		HAL::Drivers::Keyboard* GetGlobalKeyboard();
		void WriteStringToAddr(const char* str, uint64_t StartingAddr);

		namespace Tests
		{
			bool PrintTest(int t);
			bool MemoryTest(int t);
			bool StringTest(int t);
		}
	}
	namespace Multitasking
	{
		struct Task_type
		{
			uint64_t ProcessID;       // Process ID
			uint64_t StackPointer;     // Task's stack
			uint8_t State;
			uint8_t Flags;
			uint32_t Sleep;
			char Name[64];   // Task's name

			void (*Thread)(); // Actual thread entry

		};

		void CreateTask(int id, const char name[64], void (*thread)(), uint8_t attrib = 0);
		void CreateTask(const char name[64], void (*thread)(), uint8_t attrib = 0);

		void SetCurrentProcessID(uint64_t pid);
		void SetTaskInList(uint64_t pid, Task_type* Task);
		uint64_t GetNumberOfTasks();
		Multitasking::Task_type GetTask(uint64_t id);
		extern "C" uint64_t SwitchProcess(uint64_t context, uint64_t old_rip);

		void Init();

		void Sleep(uint64_t Miliseconds);
		void Yield();
	}

	namespace __cxxabiv1
	{
		/* guard variables */

		/* The ABI requires a 64-bit type.  */
		__extension__ typedef int __guard __attribute__((mode(__DI__)));

		extern "C" int __cxa_guard_acquire (__guard* g);
		extern "C" void __cxa_guard_release (__guard* g);
		extern "C" void __cxa_guard_abort (__guard* g);
	}





	// stdio.c
	void PrintOkayOrFail(bool condition);
	uint64_t ReduceBinaryUnits(uint64_t bytes);
	uint64_t GetReducedMemory(uint64_t bytes);
	bool AssertCondition(bool condition, const char* filename, uint64_t line, const char* function, const char* reason = 0);
	void HaltSystem(const char* message, const char* filename, uint64_t line, const char* function, const char* reason = 0);

	void KernelCore();
}


inline void* operator new(unsigned long size)
{
	return (void*)Kernel::HAL::DMem::AllocateChunk(size);
}

inline void* operator new[](unsigned long size)
{
	return (void*)Kernel::HAL::DMem::AllocateChunk(size);
}

inline void* operator new(unsigned long, void* addr)
{
	return addr;
}




#endif






















