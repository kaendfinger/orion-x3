// Stdio.h
// Copyright (c) 2011-2013, <zhiayang@gmail.com>
// Licensed under Creative Commons Attribution 3.0 Unported.


// Defines standard I/O functions
// Header file.

// Referenced Files:
// src/kernel/include/hal/vgatextmode.h
// Freestanding <stdint.h>
// Freestanding <stdarg.h>


#pragma once
#ifndef stdlib_h
#define stdlib_h







#include <namespace.h>



namespace Library
{
	namespace Types
	{
		class String
		{
			public:
				String(const char* str = "");
				String(const char c = 0);

				operator const char*();
				operator char*();
				String& operator= (const String& a);
				char& operator[] (uint64_t b);
				bool operator== (String s);
				String operator+ (const String& a);
				String& operator+= (const String& a);
				String& operator+= (const char a);

				uint64_t Length();
				const char* CString();
				String Clone();
				String Reverse();
				char CharAt(uint64_t index);
				String SetChar(uint64_t index, const char c);
				bool IsEmpty();
				void Clear();

			private:
				char* TheString;
				uint64_t TheLength;
		};

		class __attribute__((packed)) listobject
		{
			public:
				listobject(const void* object = 0, listobject* prev = 0, listobject* next = 0);
				~listobject();

				listobject* next;
				listobject* prev;
				void* object;
		};

		class __attribute__((packed)) List
		{
			public:
				// this needs to be already allocated; give a head pointer.
				List(void* type = 0);
				~List();


				List& operator= (const List& a);
				void* operator[] (uint64_t b) const;
				bool operator== (List s);
				List operator+ (const List& a);
				List& operator+= (const List& a);


				uint64_t Size();
				void* GetType();
				void Add(const void* n);
				uint64_t Remove(void* n);
				void* Get(uint64_t index) const;

				void RemoveAt(uint64_t index);
				uint64_t RemoveAll(void* n);

				uint64_t Replace(void* target, void* replacement);
				uint64_t ReplaceAll(void* target, void* replacement);
				void ReplaceAt(uint64_t index);

				uint64_t IndexOf(void* n);

			private:
				uint64_t length;
				listobject* head;
				listobject* tail;
		};

		class __attribute__((packed)) Queue
		{
			public:
				Queue(void* type = 0, bool IsCircular = false, uint64_t length = 16);
				~Queue();
				void Push(void* thing);
				void* Pop();
				uint64_t Size();

			private:
				listobject* head;
				listobject* tail;
				uint64_t length;
				uint64_t maxlength;
				bool IsCircular;
		};
	}


	namespace Stdio
	{
		void PrintString(const char *string, int64_t length = -1);
		void PrintHex_NoPrefix(uint64_t n, bool ReverseEndianness = false);
		void PrintHex_Signed_NoPrefix(int64_t n);
		void PrintHex(uint64_t n);
		void PrintHex_Precision_NoPrefix(uint64_t n, int8_t leadingzeroes, bool ReverseEndianness = false);
		void PrintHex_Precision(uint64_t n, uint8_t leadingzeroes);
		void PrintInteger(uint64_t num, int8_t Width = -1);
		void PrintInteger_Signed(int64_t num, int8_t Width = -1);
		void PrintBinary(uint64_t n);
		uint8_t PrintFloat(double fl, int8_t precision);

		void PrintGUID(uint64_t High64, uint64_t Low64);

		void printk(const char* string, ...);
		char* printk_sl(const char* string, ...);
		char* vprintk(const char* string, ...);
	}

	namespace String
	{
		uint64_t Length(const char *str);
		char* Copy(char* dest, const char* src);
		char* CopyLength(char* dest, const char* src, uint64_t Length);
		char* Concatenate(char* dest, const char* src);
		char* ConcatenateLength(char* dest, const char* src, unsigned long n);
		char* ConcatenateChar(char* dest, const char c);
		bool Compare(const char* a, const char* b);
		char* Reverse(char* str, uint64_t length);

		char* TrimWhitespace(char* str);
		char* SubString(char* src, uint64_t offset, uint64_t length = 0);
		char* Clear(char* src);

		char* Truncate(char* str, uint64_t Length);
	}

	namespace Memory
	{
		void* Set(void* dst, int ch, unsigned long len);
		void* Set32(void* dst, uint32_t val, unsigned long len);
		void* Set64(void* dst, uint64_t val, unsigned long len);
		void* Copy(void* dest, const void* src, unsigned long len);
		void* Copy32(void* dest, const void* src, unsigned long len);
		void* Copy64(void* dest, const void* src, unsigned long len);
		void* CopyOverlap(void* dst, const void* src, unsigned long n);
		void* CopyOverlap32(void* dst, const void* src, unsigned long n);
		void* CopyOverlap64(void* dst, const void* src, unsigned long n);
	}

	namespace Math
	{
		uint64_t Power(uint32_t num, uint32_t pow);
		int64_t Round(double number);
		uint64_t AbsoluteValue(int64_t num);
		uint64_t Log2(uint64_t num);
	}

	namespace Utility
	{
		int64_t ConvertToInt(char* str, uint8_t base = 10);
		char* ConvertToString(int64_t n);
	}
}
#endif























#define HALT(x)				Kernel::HaltSystem(x, __FILE__, __LINE__, __PRETTY_FUNCTION__, 0)


#define ___ASSERT_1(A)			Kernel::AssertCondition(A, __FILE__, __LINE__, __PRETTY_FUNCTION__, "")
#define ___ASSERT_2(A, B)		Kernel::AssertCondition(A, __FILE__, __LINE__, __PRETTY_FUNCTION__, B)


// The interim macro that simply strips the excess and ends up with the required macro
#define ___ASSERT_X(x, A, B, FUNC, ...) FUNC

// The macro that the programmer uses
#define assert(...)						___ASSERT_X(,##__VA_ARGS__,		\
										___ASSERT_2(__VA_ARGS__),		\
										___ASSERT_1(__VA_ARGS__),		\
																		)




extern "C" void* memset(void *dst, int ch, unsigned long len);
extern "C" void* memcpy(void *dest, const void *src, unsigned long len);
extern "C" void* memmove(void* dst, const void* src, unsigned long n);


extern "C" void SC_PrintChar(const char c);
extern "C" void SC_PrintInt(uint64_t);

#define invlpg(x)			asm volatile("invlpg (%0)" : : "a" (x))

#define UHALT()				asm volatile("cli; hlt")
#define BBPNT()				asm volatile("xchg %bx, %bx")

#define BOpt_Likely(x)		__builtin_expect((x), 1)
#define BOpt_UnLikely(x)	__builtin_expect((x), 0)

#define NULL				(void*)0
#define asm					__asm__


extern "C" void _STI();
extern "C" void _CLI();





// lib/stdio.c
extern uint8_t inb(unsigned short port);
extern uint16_t inw(unsigned short port);
extern uint32_t inl(unsigned short port);
extern void outb(unsigned short port, uint8_t val);
extern void outw(unsigned short port, uint16_t val);
extern void outl(unsigned short port, uint32_t val);














