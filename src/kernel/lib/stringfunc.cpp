// String.c
// Copyright (c) 2011-2013, <zhiayang@gmail.com>
// Licensed under Creative Commons Attribution 3.0 Unported.


// Defines HAL_* interface functions for VGA 80x25 display mode.



#include <stdlib.h>


// String Functions

namespace Library
{
	namespace String
	{
		uint64_t Length(const char *str)
		{
			uint64_t retval = 0;
			for(retval = 0; *str != 0; str++)
			{
				retval++;
			}
			return retval;
		}


		char* Copy(char* dest, const char* src)
		{
			char* origdest = dest;
			while(*src)
			{
				*dest++ = *src++;
			}

			*dest = '\0';
			return origdest;
		}

		char* CopyLength(char *dest, const char *src, uint64_t Length)
		{
			char* origdest = dest;
			while(Length)
			{
				*dest++ = *src++;
				Length--;
			}

			*dest = '\0';
			return origdest;
		}

		char* Concatenate(char* dest, const char* src)
		{
			return ConcatenateLength(dest, src, Length(dest) > Length(src) ? Length(dest) : Length(src));
		}

		char* ConcatenateLength(char* dest, const char* src, unsigned long n)
		{
			size_t dest_len = Length(dest);
			size_t i;
			for(i = 0; i < n && src[i] != '\0'; i++)
				dest[dest_len + i] = src[i];

			dest[dest_len + i] = '\0';
			return dest;
		}

		char* Clear(char* src)
		{
			for(int i = 0; i < Length(src); i++)
			{
				src[i] = 0;
			}
			return src;
		}

		char* ConcatenateChar(char* dest, const char c)
		{
			size_t dest_len = Length(dest);
			dest[dest_len] = c;
			dest[dest_len + 1] = '\0';
			return dest;
		}

		bool Compare(const char* a, const char* b)
		{
			if(Length(a) != Length(b))
			{
				return false;
			}

			for(uint64_t l = (Length(a) > Length(b)) ? Length(a) : Length(b); l > 0; l--)
			{
				if(*(a + l) != *(b + l))
					return false;
			}
			return true;
		}

		char* Reverse(char* str, uint64_t length)
		{
			uint64_t i = 0, j = length - 1;
			char tmp;
			while(i < j)
			{
				tmp = str[i];
				str[i] = str[j];
				str[j] = tmp;
				i++;
				j--;
			}

			return str;
		}


		char* TrimWhitespace(char *str)
		{
			char *end;

			// Trim leading space
			while(*str == ' ')
				str++;

			if(*str == 0)  // All spaces?
				return str;

			// Trim trailing space
			end = str + Length(str) - 1;
			while(end > str && *end == ' ')
				end--;

			// Write new null terminator
			*(end + 1) = 0;

			return str;
		}

		char* SubString(char* src, uint64_t offset, uint64_t length)
		{
			if(length == 0)
			{
				length = Length(src) - offset;
			}

			char* dest = new char[length + 1];
			if(!dest){ return 0; }
			Memory::Copy((void*)dest, (void*)((uint64_t)src + offset), length * sizeof(char));
			dest[length] = 0;
			return dest;
		}

		char* Truncate(char* str, uint64_t length)
		{
			if(Length(str) <= length)
				return str;

			str[length] = 0;
			return str;
		}
	}
}






























