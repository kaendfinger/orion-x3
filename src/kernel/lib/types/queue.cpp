// Queue.cpp
// Copyright (c) 2011-2013, <zhiayang@gmail.com>
// Licensed under Creative Commons Attribution 3.0 Unported.

#include <namespace.h>
#include <stdlib.h>

using namespace Library::Stdio;
namespace Library {
namespace Types
{
	Queue::Queue(void* type, bool IsCircular, uint64_t length)
	{
		this->length = (type == 0 ? 0 : 1);
		this->head = (type == 0 ? 0 : new listobject(type));
		this->tail = this->head;

		this->IsCircular = IsCircular;
		this->maxlength = length;
	}
	Queue::~Queue()
	{
		listobject* obj = 0;
		for(uint64_t i = this->length; i > 0; i--)
		{
			obj = this->tail;
			this->tail = obj->prev;
			delete obj;
		}
	}

	void Queue::Push(void* object)
	{
		// special case -- if head and tail are 0, that means it's an empty list without a known type.
		if(this->head == 0 && this->tail == 0 && this->length == 0)
		{
			this->tail = new listobject(object);
			this->head = this->tail;
			this->length = 1;
		}
		else if(!this->IsCircular || this->length < this->maxlength)
		{
			this->head->prev = new listobject(object, 0, this->head);
			this->head->prev->next = this->head;
			this->head = this->head->prev;
			this->length++;
		}
		else if(this->IsCircular)
		{
			// we've reached the maximum length...
			// pop the oldest value, then push another one.
			this->Pop();
			this->Push(object);
		}
	}

	void* Queue::Pop()
	{
		if(this->length == 0)
		{
			Stdio::printk("\n\n|> Don't pop an empty queue you bloody fool! <|\n\n");
			return 0;
		}

		void* ret = this->tail->object;
		if(this->length == 1)
		{
			this->tail = 0;
			this->head = 0;
		}
		else
		{
			this->tail = this->tail->prev;
		}

		this->length--;
		delete this->tail;

		return ret;
	}

	uint64_t Queue::Size()
	{
		return this->length;
	}
}
}









