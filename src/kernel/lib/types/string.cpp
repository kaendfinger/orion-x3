// StdType.cpp
// Copyright (c) 2011-2013, <zhiayang@gmail.com>
// Licensed under Creative Commons Attribution 3.0 Unported.


#include <stdlib.h>



namespace Library {
namespace Types
{
	String::String(const char* str)
	{
		this->TheString = (char*)Kernel::HAL::DMem::AllocateChunk(sizeof(char) * Library::String::Length(str));
		this->TheLength = Library::String::Length(str);
		Library::String::Copy(this->TheString, str);
	}
	String::String(const char c)
	{
		this->TheString = (char*)Kernel::HAL::DMem::AllocateChunk(sizeof(char) * 1);
		this->TheLength = 1;
		this->TheString[0] = c;
	}


	String::operator const char*() { return (const char*)TheString; }
	String::operator char*() { return TheString; }

	String& String::operator =(const String& a)
	{
		if(&a == this)
			return *this;

		Library::String::Copy(this->TheString, a.TheString);
		this->TheLength = a.TheLength;
		return *this;
	}

	char& String::operator[] (uint64_t b)
	{
		if(b < this->TheLength)
			return this->TheString[b];

		else
			return this->TheString[this->TheLength - 1];
	}

	bool String::operator== (String s)
	{
		return Library::String::Compare(this->TheString, s.TheString);
	}

	String String::operator+ (const String& a)
	{
		String s = this->Clone();
		return Library::String::Concatenate(s, a.TheString);
	}

	String& String::operator+= (const String& a)
	{
		if(this->TheLength < a.TheLength)
		{
			Library::String::Copy((char*)Kernel::HAL::DMem::AllocateChunk(sizeof(char) * a.TheLength), this->TheString);
			Kernel::HAL::DMem::FreeChunk((uint64_t*)this->TheString);
		}

		Library::String::Concatenate(this->TheString, a.TheString);
		return *this;
	}

	String& String::operator+= (const char a)
	{
		Library::String::Copy((char*)Kernel::HAL::DMem::AllocateChunk(sizeof(char) * this->TheLength + 1), this->TheString);
		Kernel::HAL::DMem::FreeChunk((uint64_t*)this->TheString);

		this->TheString[this->TheLength] = a;
		this->TheLength += 1;
		return *this;
	}




	uint64_t String::Length()
	{
		return this->TheLength;
	}

	const char* String::CString()
	{
		return this->TheString;
	}

	String String::Clone()
	{
		return String(this->TheString);
	}

	String String::Reverse()
	{
		return String(Library::String::Reverse(this->TheString, this->TheLength));
	}

	char String::CharAt(uint64_t index)
	{
		return this->TheString[index];
	}

	String String::SetChar(uint64_t index, const char c)
	{
		this->TheString[index] = c;
		return *this;
	}

	bool String::IsEmpty()
	{
		return Library::String::Compare(this->TheString, "");
	}

	void String::Clear()
	{
		*this = "";
	}
}
}
