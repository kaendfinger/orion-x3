// List.cpp
// Copyright (c) 2011-2013, <zhiayang@gmail.com>
// Licensed under Creative Commons Attribution 3.0 Unported.

#include <stdlib.h>
#include <namespace.h>

namespace Library {
namespace Types
{
	listobject::listobject(const void* object, listobject* prev, listobject* next)
	{
		this->next = next;
		this->prev = prev;
		this->object = (void*)object;
	}
	listobject::~listobject()
	{
		this->next = 0;
		this->prev = 0;
		// delete complains, cannot delete void*
		// Kernel::HAL::DMem::FreeChunk((uint64_t*)this->object);
	}



	List::List(void* first)
	{
		this->length = (first == 0 ? 0 : 1);
		this->head = (first == 0 ? 0 : new listobject(first));
		this->tail = this->head;
	}
	List::~List()
	{
		listobject* obj = 0;
		for(uint64_t i = this->length; i > 0; i--)
		{
			obj = this->tail;
			this->tail = obj->prev;
			delete obj;
		}
	}

	List& List::operator= (const List& a)
	{
		if(this != &a)
		{
			for(uint64_t i = 0; i < this->length; i++)
			{
				delete this;
			}
			for(uint64_t i = 0; i < a.length; i++)
			{
				this->Add(a.Get(i));
			}
		}
		return *this;
	}

	void* List::operator[] (uint64_t b) const
	{
		// since this is a doubly-linked list, traverse
		// from the back if the index is in the rear half.
		listobject* obj = 0;
		if(b >= this->length / 2)
		{
			obj = this->tail;
			for(uint64_t i = this->length - 1; i > b; i--)
			{
				obj = obj->prev;
			}
		}
		else
		{
			obj = this->head;
			for(uint64_t i = 0; i < b; i++)
			{
				obj = obj->next;
			}
		}

		return obj->object;
	}




	uint64_t List::Size(){ return this->length; }
	void* List::GetType(){ return this->head->object; }
	void* List::Get(uint64_t i) const{ return (*this)[i]; }

	void List::Add(const void* object)
	{
		// special case -- if head and tail are 0, that means it's an empty list without a known type.
		if(this->head == 0 && this->tail == 0 && this->length == 0)
		{
			this->head = new listobject(object);
			this->tail = this->head;
			this->length = 1;
		}
		else
		{
			this->tail->next = new listobject(object, this->tail);
			this->tail = this->tail->next;
			this->length++;
		}
	}
}
}














