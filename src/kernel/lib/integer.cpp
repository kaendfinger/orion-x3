// integer.c
// Copyright (c) 2011-2013, <zhiayang@gmail.com>
// Licensed under Creative Commons Attribution 3.0 Unported.

#include <stdlib.h>


namespace Library {
namespace Utility
{
	static int Debase(char c)
	{
		if ( '0' <= c && c <= '9' ) { return c - '0'; }
		if ( 'a' <= c && c <= 'z' ) { return 10 + c - 'a'; }
		if ( 'A' <= c && c <= 'Z' ) { return 10 + c - 'A'; }
		return -1;
	}

	int64_t ParseInteger(const char* str, char** endptr, int base)
	{
		const char* origstr = str;
		int origbase = base;

		while(*str == ' ')
		{
			str++;
		}

		if(base < 0 || 36 < base)
		{
			if(endptr)
			{
				*endptr = (char*)str;
			}
			return 0;
		}

		int64_t result = 0;
		bool negative = false;
		char c = *str;
		if(c == '-')
		{
			str++;
			negative = true;
		}
		if(c == '+')
		{
			str++;
			negative = false;
		}
		if(!base && str[0] == '0')
		{
			if(str[1] == 'x' || str[1] == 'X')
			{
				str += 2;
				base = 16;
			}

			else if(0 <= Debase(str[1]) && Debase(str[1]) < 8)
			{
				str++;
				base = 8;
			}
		}
		if(!base)
		{
			base = 10;
		}
		if(origbase == 16 && str[0] == '0' && (str[1] == 'x' || str[1] == 'X'))
		{
			str += 2;
		}

		size_t numconvertedchars = 0;
		while((c = *str))
		{
			int val = Debase(c);
			if(val < 0)
				break;

			if(base <= val)
				break;
			if(negative)
				val = -val;

			// TODO: Detect overflow!
			result = result * (int64_t)base + (int64_t)val;
			str++;
			numconvertedchars++;
		}
		if(!numconvertedchars)
		{
			str = origstr;
			result = 0;
		}
		if(endptr)
		{
			*endptr = (char*)str;
		}

		return result;
	}

	char* ConvertToString(int64_t num)
	{
		char* dest = new char[32];
		int result = 0;
		int64_t copy = num;

		if ( num < 0 )
		{
			*dest++ = '-';
			result++;

			int offset = 0;
			while ( copy < -9 ) { copy /= 10; offset++; }
			result += offset;
			while ( offset >= 0 )
			{
				dest[offset] = '0' - num % 10; num /= 10; offset--;
			}
		}
		else
		{
			int offset = 0;
			while ( copy > 9 ) { copy /= 10; offset++; }
			result += offset;
			while ( offset >= 0 )
			{
				dest[offset] = '0' + num % 10; num /= 10; offset--;
			}
		}
		return dest;
	}









	int64_t ConvertToInt(char* str, uint8_t base)
	{
		return ParseInteger(str, (char**)NULL, base);
	}
}
}


































