// math.c
// Copyright (c) 2011-2013, <zhiayang@gmail.com>
// Licensed under Creative Commons Attribution 3.0 Unported.

#include <stdlib.h>

namespace Library {
namespace Math
{
	uint64_t Power(uint32_t num, uint32_t pow)
	{
		uint64_t ret = 1;

		if(pow == 0)
			return 1;

		for(; pow > 0; pow--)
		{
			ret *= num;
		}
		return ret;
	}

	int64_t Round(double number)
	{
	    return (number >= 0) ? (uint64_t)(number + 0.5) : (uint64_t)(number - 0.5);
	}

	uint64_t AbsoluteValue(int64_t num)
	{
		if(num < 0)
			num = -num;

		return num;
	}

	uint64_t Log2(uint64_t num)
	{
		uint64_t ret = 0;
		while(num >>= 1)
		{
			ret++;
		}
		return ret;
	}
}
}
