// kernel.c
// Copyright (c) 2011-2013, <zhiayang@gmail.com>
// Licensed under Creative Commons Attribution 3.0 Unported.


// Main kernel file.



#include <stdlib.h>
#include <elf.h>


using namespace Kernel;
using namespace Library;
using namespace Library::Stdio;
using namespace Kernel::HAL::Devices;
using namespace Kernel::HAL::DMem;

#define VER_MAJOR		"4"
#define VER_MINOR		"19"
#define VER_REVSN		"7"



// An array containing a list of memory types.
const char *K_MemoryTypes[6] =
{
	"[Invalid Type]",
	"[Available]",
	"[Reserved]",
	"[ACPI Reclaimable]",
	"[ACPI NVM]",
	"[Bad Memory]"
};


extern MemoryMap_type *K_MemoryMap;







extern uint32_t X_BUILD_NUMBER;

extern "C" void Switch();
extern "C" void _HAL_PITTimerHandler(HAL::HAL_ISRRegs *r);
extern "C" void DoUsermode();
extern "C" void HandleSyscall();


uint8_t BootLoaderType;
uint64_t K_SystemMemoryInBytes = 0;

uint16_t ResX = 0;
uint16_t ResY = 0;

uint64_t LFBAddr = 0;
uint64_t LFBBufferAddr = 0;
uint64_t TrueLFBAddr = 0;
uint32_t LFBLengthInPages = 0;

uint32_t MBTAddr = 0;
bool RunKernelInThreadedMode = true;









// Flags gotten from System.cfg:
bool PRINTDSK = true;
bool PRINTMEM = true;
bool PRINTPCI = true;
bool PRINTCPU = true;
bool RUNTESTS = true;


extern uint64_t KernelEnd;




namespace Kernel {


Kernel::HAL::Drivers::Keyboard* GlobalKeyboard;


void KernelMain(uint32_t MultibootMagic, uint32_t MBTAddr)
{
	// Initalise the IDT
	HAL::Interrupts::Initialise();

	// Start COM1 serial-output
	HAL::Devices::COM1::Initialise();

	// Fix the virtual memory system
	HAL::VMem::Initialise();

	// Initialise the syscall subsystem
	Kernel::HAL::Interrupts::SetGate(0xAE, (uint64_t)HandleSyscall, 0x8, 0xEE);

	// start parsing the multiboot structure.
	Multiboot::Info_type* MBTStruct = (Multiboot::Info_type*)((uint64_t)MBTAddr);
	Internal::DoMultiboot1MemoryMap(MBTStruct);

	::MBTAddr = MBTAddr;

	// Initialise the memory systems: Physical and Heap.
	HAL::PMem::Initialise();
	HAL::PMem::CoalesceFPLs();
	HAL::DMem::Initialise();

	// Initialise the keyboard, but disable it.
	GlobalKeyboard = new HAL::Drivers::PS2Keyboard();
	GlobalKeyboard->Disable();

	// elaborate ruse such that making PCI objects doesn't fuck over our memory map
	// Since we have a heap, might as well use it.
	// just make sure we don't 'free' that area.
	uint64_t a = (uint64_t)K_MemoryMap;
	uint32_t s = K_MemoryMap->SizeOfThisStructure;
	K_MemoryMap = (MemoryMap_type*)HAL::DMem::AllocateChunk(K_MemoryMap->SizeOfThisStructure + sizeof(uint32_t));

	Library::Memory::CopyOverlap((void*)K_MemoryMap, (void*)a, s);


	// setup the TSS for later.

	// kernel stack at 0x8000
	Memory::Set32((void*)0x500004, 0x00060000, 1);
	Memory::Set32((void*)0x500008, 0x00000000, 1);
	Memory::Set32((void*)0x500024, 0x00010000, 1);
	Memory::Set32((void*)0x500028, 0x00000000, 1);
	asm volatile("mov $0x28, %ax; ltr %ax");





	// print the loading message.
	memset((void*)0xB8000, 0, 80 * 25 * 2);
	Kernel::Internal::WriteStringToAddr("Loading Orion-X3...", 0xB8000);







	// Start enumerating (shallow level only, we don't follow bridges yet) PCI devices.

	uint16_t vendor = 0;
	uint8_t headertype = 0;

	PCI::PCIDevice::PCIDevices = new Types::List();
	for(int bus = 0; bus < 256; bus++)
	{
		for(int slot = 0; slot < 32; slot++)
		{
			if(PCI::CheckDeviceExistence(bus, slot))
			{
				vendor = PCI::ReadConfig16(PCI::MakeAddr(bus, slot, 0), 0);
				headertype = (uint8_t)(PCI::ReadConfig32(PCI::MakeAddr(bus, slot, 0), 0x0C) >> 16);


				using PCI::PCIDevice;
				PCIDevice::PCIDevices->Add(new PCI::PCIDevice(bus, slot, 0));


				if((headertype) & (1 << 7))
				{
					for(uint8_t func = 1; func < 8; func++)
					{
						vendor = PCI::ReadConfig16(PCI::MakeAddr(bus, slot, func), 0);
						if(vendor != 0xFFFF)
						{
							PCIDevice::PCIDevices->Add(new PCI::PCIDevice(bus, slot, func));
						}
					}
				}
			}
		}
	}
	uint32_t lfb = 0;


	// Get the PCI device representing the BGA, which is essential for output.

	PCI::PCIDevice* BGADevice;

	BGADevice = PCI::SearchByVendorDevice(0x1234, 0x1111);
	if(!BGADevice)
	{
		BGADevice = PCI::SearchByVendorDevice(0x80EE, 0xBEEF);

		if(!BGADevice)
		{
			// we can't work without one.
			// halt.

			Kernel::Internal::WriteStringToAddr("Error: No BGA-compatible video card found.", 0xB80A0);
			Kernel::Internal::WriteStringToAddr("Orion-X3 requires such a device to work.", 0xB8140);
			Kernel::Internal::WriteStringToAddr("Check your system and try again.", 0xB8280);
			UHALT();
		}
	}

	lfb = PCI::ReadConfig32(BGADevice->GetAddress(), 0x10);
	LFBAddr = lfb & 0xFFFFFFF0;


























	TrueLFBAddr = LFBAddr;

	if(RunKernelInThreadedMode)
	{
		// run the kernel's core processing as a thread.
		void Idle();
		Kernel::Multitasking::CreateTask(0, (char*)"Kernel", KernelCore);
		Kernel::Multitasking::CreateTask(1, (char*)"Idle", Idle);
		HAL::Interrupts::SetGate(32, (uint64_t)Switch, 0x08, 0x8E);
		HAL::PIT::SetTimerHertz(1000);


		// An Idle task is needed for the scheduler to defer to when only one other task is running.

		asm volatile("sti");

	}
	else
	{
		printk("%wALERT%r: %wRunning Orion-X3 in non-threaded mode, %wexpect bugs%r %wwhere Idle processes are expected/required.%r\n", VC_Red, VC_Silver, VC_Rose, VC_Silver);
		KernelCore();
	}
}




void KernelCore()
{
	// identify ATA devices

	// we need interrupts on to know when the ATA device is ready.
	Kernel::HAL::Interrupts::InstallIRQHandler(14, HAL::Devices::ATA::PIO::IRQHandler14);
	Kernel::HAL::Interrupts::InstallIRQHandler(15, HAL::Devices::ATA::PIO::IRQHandler15);
	asm volatile("sti");


	// print the ATA devce info
	using Kernel::HAL::Devices::ATA::ATADevice;
	using Kernel::HAL::FS::Partition;
	using Kernel::HAL::FS::FAT32;

	ATADevice::ATADevices = new Types::List();


	ATA::IdentifyAll();
	for(int i = 0; i < ATADevice::ATADevices->Size(); i++)
	{
		HAL::FS::MBR::ReadPartitions((ATADevice*)(*ATADevice::ATADevices)[i]);
	}























	ATADevice* SystemATA = ((ATADevice*)(*ATADevice::ATADevices)[0]);
	FAT32* SystemFS = (FAT32*)((Partition*)((*SystemATA->Partitions)[0]))->GetFilesystem();
	FAT32::File_type* ConfigFile = SystemFS->SearchFile(2, "SYSTEM.CFG");
	uint64_t ConfigAddr = (uint64_t)HAL::DMem::AllocateChunk(ConfigFile->FileSize);
	SystemFS->ReadFile(ConfigFile, ConfigAddr);





	// TODO: make this more robust.

	char* CfgXres = (char*)HAL::DMem::AllocateChunk(4);
	char* CfgYres = (char*)HAL::DMem::AllocateChunk(4);

	char* CfgLine = (char*)HAL::DMem::AllocateChunk(8);

	uint8_t* ConfigStr = (uint8_t*)ConfigAddr;

	int i = 0, xi = 0, yi = 0, vi = 0;
	while(ConfigStr[i] != 0xA && ConfigStr[i] != 0)
	{
		CfgXres[xi] = ConfigStr[i];
		i++;
		xi++;
	}
	i++;

	while(ConfigStr[i] != 0xA && ConfigStr[i] != 0)
	{
		CfgYres[yi] = ConfigStr[i];
		i++;
		yi++;
	}
	i++;


	char* CfgVar;
	do
	{
		while(ConfigStr[i] != 0xA && ConfigStr[i] != 0)
		{
			CfgLine[vi] = ConfigStr[i];
			i++;
			vi++;
		}

		CfgVar = String::SubString(CfgLine, 0, 8);
		char* value = String::SubString(CfgLine, 9);
		if(String::Compare(CfgVar, "PRINTDSK"))
		{
			if(String::Compare(value, "true"))
				PRINTDSK = true;

			else
				PRINTDSK = false;
		}
		else if(String::Compare(CfgVar, "PRINTMEM"))
		{
			if(String::Compare(value, "true"))
				PRINTMEM = true;

			else
				PRINTMEM = false;
		}
		else if(String::Compare(CfgVar, "PRINTPCI"))
		{
			if(String::Compare(value, "true"))
				PRINTPCI = true;

			else
				PRINTPCI = false;
		}
		else if(String::Compare(CfgVar, "PRINTCPU"))
		{
			if(String::Compare(value, "true"))
				PRINTCPU = true;

			else
				PRINTCPU = false;
		}
		else if(String::Compare(CfgVar, "RUNTESTS"))
		{
			if(String::Compare(value, "true"))
				RUNTESTS = true;

			else
				RUNTESTS = false;
		}

		vi = 0;
		i++;
		Memory::Set((void*)CfgLine, 0, HAL::DMem::HeapChunk::QuerySize(CfgLine));

	} while(!String::Compare(CfgVar, "     END"));












	uint16_t PrefResX = Library::Utility::ConvertToInt(CfgXres);
	uint16_t PrefResY = Library::Utility::ConvertToInt(CfgYres);



	// Set video mode
	HAL::Devices::BGA::SetVideoMode(PrefResX, PrefResY, 32, true, false);
	HAL::Devices::LFB::Initialise();


	// Get the set resolution (may be different than our preferred)
	ResX = LFB::GetResX();
	ResY = LFB::GetResY();
	// printk("(%d, %d)", ResX, ResY);


	// Calculate how many bytes we need. (remember, 4 bytes per pixel)
	uint32_t bytes = (ResX * ResY) * 4;

	// Get that rounded up to the nearest page
	bytes = (bytes + (4096 - 1)) / 4096;
	HAL::VMem::MapRegion(LFBAddr, LFBAddr, bytes, 0x07);
	LFBLengthInPages = bytes;
	HAL::Devices::LFB::Console::Initialise();











	Internal::PrintVersion();
	printk("\nGRUB Multiboot Struct at: %wphysical%r %w%x%r; Relocating to %wvirtual%r %w%x\n", VC_Silver, VC_Orange, (uint64_t)MBTAddr, VC_Silver, VC_Violet, (uint64_t)K_MemoryMap);

	if(PRINTMEM)
	{
		printk("\n\nMemory map from GRUB, printing...\n");
		Internal::PrintKernelMemoryMap();
	}

	char *memunit = (char*)HAL::DMem::AllocateChunk(5 * sizeof(char));
	int index = ReduceBinaryUnits(K_SystemMemoryInBytes);
	String::Copy(memunit, K_BinaryUnits[index]);
	int mem = GetReducedMemory(K_SystemMemoryInBytes);


	printk("\n%sTotal Detected Memory: %w%d %s%r\n%s", PRINTMEM ? "\n" : "", VC_Rose, mem, memunit, PRINTMEM ? "\n\n" : "");


	if(PRINTCPU)
	{
		printk("Detecting Processor Capabilities...\n");
		HAL::Devices::CPUID::PrintCPUInfo(false);
	}


	printk("\n%sProbing PCI devices...\n", PRINTCPU ? "\n\n" : "");
	using PCI::PCIDevice;


	if(PRINTPCI)
	{
		for(uint16_t num = 0; num < PCIDevice::PCIDevices->Size(); num++)
		{
			Internal::PrintPCIDeviceInfo((PCIDevice*)(*PCIDevice::PCIDevices)[num]);

			if(((PCIDevice*)(*PCIDevice::PCIDevices)[num])->GetIsMultifunction())
				printk(" ==>%w Multifunction Device", VC_Yellow);

			if(PCI::MatchVendorDevice((PCIDevice*)(*PCIDevice::PCIDevices)[num], 0x1234, 0x1111) || PCI::MatchVendorDevice((PCIDevice*)(*PCIDevice::PCIDevices)[num], 0x80EE, 0xBEEF))
				printk(" ==>%w BGA Compatible Video Card:%w %x", VC_Cyan, VC_Orange, Internal::_GetTrueLFBAddr());



			printk("\n");
		}
	}
	else
	{
		printk("%w%d PCI devices%r probed.\n", VC_Orange, PCIDevice::PCIDevices->Size());
	}







	// print the ATA devce info
	using Kernel::HAL::Devices::ATA::ATADevice;
	using Kernel::HAL::FS::Partition;
	using Kernel::HAL::FS::FAT32;
	// Print the partitions we found

	if(PRINTDSK)
	{
		printk("\n\nPrinting ATA Drives on the bus...\n");

		// read all info, *then* print stuff
		for(int i = 0; i < ATADevice::ATADevices->Size(); i++)
		{
			ATA::PrintATAInfo((ATADevice*)(*ATADevice::ATADevices)[i]);
			printk("\n");
			ATADevice* dev = (ATADevice*)(*ATADevice::ATADevices)[i];

			for(int z = 0; z < dev->Partitions->Size(); z++)
			{
				printk("\t");
				((Partition*)((*dev->Partitions)[z]))->PrintInfo();
				printk("\n\t\t");
				((Partition*)((*dev->Partitions)[z]))->GetFilesystem()->PrintInfo();
				printk("\n\n");
			}
			printk("\n");
		}
	}

	printk("\n\n");














	ATADevice* dev = (ATADevice*)(*ATADevice::ATADevices)[0];
	FAT32* fs = (FAT32*)((Partition*)((*dev->Partitions)[0]))->GetFilesystem();
	FAT32::File_type* file = fs->SearchFile(2, "HELLO.ELF");
	uint64_t addr = (uint64_t)HAL::DMem::AllocateChunk(file->FileSize);
	fs->ReadFile(file, addr);


	// read the ELF header at the address.
	ELF64FileHeader_type* FileHeader = (ELF64FileHeader_type*)addr;
	assert(FileHeader->ElfIdentification[0] == ELF_MAGIC0);
	assert(FileHeader->ElfIdentification[1] == ELF_MAGIC1);
	assert(FileHeader->ElfIdentification[2] == ELF_MAGIC2);
	assert(FileHeader->ElfIdentification[3] == ELF_MAGIC3);
	assert(FileHeader->ElfIdentification[4] == 2);
	assert(FileHeader->ElfIdentification[5] == 1);
	assert(FileHeader->ElfType == 2);


{
	// print header
	// printk("Identification:                      ");
	// // lazy lol
	// for(int i = 0; i < 16; i++)
	// 	printk("%w%#2x ", i % 2 ? VC_DarkCyan : VC_Orange, FileHeader->ElfIdentification[i]);

	// printk("\n");

	// // print header info
	// printk("File Class:                          %w%s\n", VC_Silver, FileHeader->ElfIdentification[4] == 1 ? "ELF32" : (FileHeader->ElfIdentification[4] == 2 ? "ELF64" : "Unknown"));
	// printk("File Data:                           %w%s\n", VC_Silver, FileHeader->ElfIdentification[5] == 1 ? "Little-endian" : "Big-endian");
	// printk("File Version:                        %w%d (%s)\n", VC_Silver, FileHeader->ElfIdentification[6], FileHeader->ElfIdentification[6] == 1 ? "current" : "unknown");
	// printk("OS ABI:                              %w%s\n", VC_Silver, FileHeader->ElfIdentification[7] == 0 ? "Unix System V" : (FileHeader->ElfIdentification[7] == 1 ? "HP-UX" : "Unknown/Embedded"));
	// printk("OS ABI Version:                      %w%d\n", VC_Silver, FileHeader->ElfIdentification[8]);
	// printk("Program Type:                        %w%s\n", VC_Silver, ElfFileType[FileHeader->ElfType]);
	// printk("Machine Architecture:                %w%s\n", VC_Silver, FileHeader->ElfMachine == 0 ? "None" : (FileHeader->ElfMachine == 3 ? "Intel 386" : (FileHeader->ElfMachine == 62 ? "AMD64" : "Unknown")));
	// printk("Entry Point:                         %w%x\n", VC_Silver, FileHeader->ElfEntry);
	// printk("Program Headers:                     %w%x\t%rbytes from start of file\n", VC_Silver, FileHeader->ElfProgramHeaderOffset);
	// printk("Section Headers:                     %w%x\t%rbytes from start of file\n", VC_Silver, FileHeader->ElfSectionHeaderOffset);
	// printk("ELF Flags:                           %w%x\n", VC_Silver, FileHeader->ElfFlags);
	// printk("Size of ELF Header:                  %w%d%r bytes\n", VC_Silver, FileHeader->ElfHeaderSize);
	// printk("Size of Program Header:              %w%d%r bytes\n", VC_Silver, FileHeader->ElfProgramHeaderEntrySize);
	// printk("Program Headers:                     %w%d\n", VC_Silver, FileHeader->ElfProgramHeaderEntries);
	// printk("Size of Section Header:              %w%d%r bytes\n", VC_Silver, FileHeader->ElfSectionHeaderEntrySize);
	// printk("Section Headers:                     %w%d\n", VC_Silver, FileHeader->ElfSectionHeaderEntries);
	// printk("Section Header String Table Index:   %w%d\n\n", VC_Silver, FileHeader->ElfStringTableIndex);




	// char* StringTable = 0;

	// uint64_t g = 0;
	// for(uint64_t x = 0; x < FileHeader->ElfSectionHeaderEntrySize * FileHeader->ElfSectionHeaderEntries; x += FileHeader->ElfSectionHeaderEntrySize)
	// {
	// 	ELF64SectionHeader_type* s = (ELF64SectionHeader_type*)((uintptr_t)addr + FileHeader->ElfSectionHeaderOffset + x);

	// 	if(g == FileHeader->ElfStringTableIndex)
	// 	{
	// 		StringTable = (char*)((uintptr_t)addr + s->SectionHeaderOffset);
	// 	}
	// 	g++;
	// }




	// // print sections
	// printk("%wSection Headers:               %r[%wType%r]    [%wSectionAddress%r]      [%wHeaderOffset%r]\n", VC_Orange, VC_Red, VC_Silver, VC_Cyan);
	// g = 0;
	// for(int k = 0; k < FileHeader->ElfSectionHeaderEntrySize * FileHeader->ElfSectionHeaderEntries; k += FileHeader->ElfSectionHeaderEntrySize)
	// {
	// 	ELF64SectionHeader_type* SH = (ELF64SectionHeader_type*)((uintptr_t)addr + FileHeader->ElfSectionHeaderOffset + k);

	// 	printk("\t[%w%2d%r] %w%s", VC_Chartreuse, g, VC_Magenta, (char*)((uintptr_t)StringTable + SH->SectionHeaderName));
	// 	uint64_t snl = String::Length((char*)((uintptr_t)StringTable + SH->SectionHeaderName));

	// 	for(uint64_t y = 20; snl < y; snl++)
	// 		PrintString(" ");


	// 	switch(SH->SectionHeaderType)
	// 	{
	// 		case SectionHeaderTypeNone:
	// 			printk("%wNothing        ", VC_DarkCyan);
	// 			break;
	// 		case SectionHeaderTypeProgramBits:
	// 			printk("%wProgramBits    ", VC_DarkCyan);
	// 			break;
	// 		case SectionHeaderTypeSymbolTable:
	// 			printk("%wSymbolTable    ", VC_DarkCyan);
	// 			break;
	// 		case SectionHeaderTypeStringTable:
	// 			printk("%wStringTable    ", VC_DarkCyan);
	// 			break;
	// 		case SectionHeaderTypeRelocation:
	// 			printk("%wRelocation     ", VC_DarkCyan);
	// 			break;
	// 		case SectionHeaderTypeHashTable:
	// 			printk("%wHashTable      ", VC_DarkCyan);
	// 			break;
	// 		case SectionHeaderTypeDynSymTable:
	// 			printk("%wDynSymbolTab   ", VC_DarkCyan);
	// 			break;
	// 		case SectionHeaderTypeNotes:
	// 			printk("%wNotes          ", VC_DarkCyan);
	// 			break;
	// 		case SectionHeaderTypeNoBits:
	// 			printk("%wNoBits         ", VC_DarkCyan);
	// 			break;
	// 	}

	// 	printk("<%w%#8x%r>            [%w%#6x%r]", VC_Yellow, SH->SectionHeaderAddress, VC_Orange, SH->SectionHeaderOffset);




	// 	printk("\n");
	// 	g++;
	// }
}


	printk("\n\n%wProgram Headers:                         %r[%wSegmentAddress%r]         [%wFileSize%r :  %wMemSize%r]\n", VC_Rose, VC_Chartreuse,  VC_DarkCyan, VC_Magenta);


	for(int k = 0; k < FileHeader->ElfProgramHeaderEntries; k++)
	{
		ELF64ProgramHeader_type* ProgramHeader = (ELF64ProgramHeader_type*)(addr + FileHeader->ElfProgramHeaderOffset + (k * FileHeader->ElfProgramHeaderEntrySize));


		// printk("\t[%w%2d%r] %w%s", VC_Orange, k, VC_Azure, ProgramHeader->ProgramType == ProgramTypeNull ?
		// 		"Null           " : (ProgramHeader->ProgramType == ProgramTypeLoadableSegment ?
		// 		"Load           " : (ProgramHeader->ProgramType == ProgramTypeDynamicLinking ?
		// 		"Dynamic        " : (ProgramHeader->ProgramType == ProgramTypeIntepreter ?
		// 		"Intepreter     " : (ProgramHeader->ProgramType == ProgramTypeAuxillaryInfo ?
		// 		"Auxillary      " : (ProgramHeader->ProgramType == ProgramTypeSectionHeader ?
		// 		"SectionHeader  " : (ProgramHeader->ProgramType == ProgramTypeProgramHeader ?
		// 		"ProgramHeader  " :
		// 		"Unknown        ")))))));

		// printk("                    ");
		// printk("<%w%#8x%r>            [%w%#8x%r : %w%#8x%r]\n", VC_Red, ProgramHeader->ProgramVirtualAddress, VC_Chartreuse, ProgramHeader->ProgramFileSize, VC_Blue, ProgramHeader->ProgramMemorySize);




		if(ProgramHeader->ProgramFileSize == 0 || ProgramHeader->ProgramMemorySize == 0 || ProgramHeader->ProgramVirtualAddress == 0)
			continue;

		for(int m = 0; m < (ProgramHeader->ProgramMemorySize / 0x1000 + 1); m++)
		{
			HAL::VMem::MapAddr(ProgramHeader->ProgramVirtualAddress + (m * 0x1000), HAL::PMem::AllocatePage_NoMap(), 0x07);
		}

		Memory::Copy((void*)ProgramHeader->ProgramVirtualAddress, (void*)(addr + ProgramHeader->ProgramOffset), ProgramHeader->ProgramFileSize);

		if(ProgramHeader->ProgramMemorySize > ProgramHeader->ProgramFileSize)
			Memory::Set((void*)(addr + ProgramHeader->ProgramFileSize), 0x00, ProgramHeader->ProgramMemorySize - ProgramHeader->ProgramFileSize);
	}





	// START TESTING AREA





	// asm volatile("sti");
	// GlobalKeyboard->Enable();
	// while(true)
	// {
	// 	Kernel::HAL::Devices::LFB::Console::PrintChar(GlobalKeyboard->ReadChar());
	// }








	void(*Entry1)() = (void(*)())(FileHeader->ElfEntry);
	Multitasking::CreateTask("Hello", Entry1, 0x01);
	asm volatile("sti");

	// void Idle3();
	// Multitasking::CreateTask("Idle3", Idle3, 0x01);

	// void Idle2();
	// Multitasking::CreateTask("Idle2", Idle2, 0x01);
	// asm volatile("sti");











	// double value = -1;
	// double x = value, newx = x;

	// while(true)
	// {
	// 	x = newx;
	// 	newx = 0.5 * (x + (value / x));

	// 	PrintFloat(newx, 15);
	// 	PrintString("\n");
	// }



	// return Value;











	// END TESTING AREA

	if(RUNTESTS)
	{
		printk("\n\n\n%wRunning tests...\n\n", VC_Orange);

		Internal::Tests::PrintTest(1);
		printk("\n");
		LFB::RefreshBuffer();
		Internal::Tests::MemoryTest(2);
		printk("\n");
		LFB::RefreshBuffer();
		Internal::Tests::StringTest(3);
		printk("\n%wTests complete\n", VC_Azure);
	}

	printk("\n\n%wNo more executable instructions, halting...\n\n", VC_Rose);
	LFB::RefreshBuffer();

	asm volatile(".haltlabel:; hlt; jmp .haltlabel");
}

void Idle2()
{
	while(true)
	{
		// asm volatile("mov $0x2, %r10; pop %rdi; int $0xAE");
	}
}

void Idle()
{
	while(true);
}

void Idle3()
{
	while(true)
	{
		// asm volatile("mov $0x2, %r10; mov $91, %rdi; int $0xAE");
		// asm volatile("xchg %bx, %bx");
	}
}

}







































extern "C" void kmain(uint32_t MultibootMagic, uint32_t MBTAddr)
{
	void KernelMain(uint32_t, uint32_t);
	Kernel::KernelMain(MultibootMagic, MBTAddr);
}

namespace Kernel
{
	namespace Internal
	{
		uint64_t GetLFBAddr()
		{
			return LFBAddr;
		}

		uint64_t _GetTrueLFBAddr()
		{
			return TrueLFBAddr;
		}

		uint32_t GetLFBLengthInPages()
		{
			return LFBLengthInPages;
		}

		void StopUsingOffscreenBuffer()
		{
			LFBAddr = TrueLFBAddr;
		}
		void StartUsingOffscreenBuffer()
		{
			LFBAddr = LFBBufferAddr;
		}
		HAL::Drivers::Keyboard* GetGlobalKeyboard()
		{
			return Kernel::GlobalKeyboard;
		}

		void PrintPCIDeviceInfo(uint8_t bus, uint8_t slot, uint16_t vendor, uint16_t device, uint8_t cl, uint8_t subclass, uint8_t h, uint8_t func)
		{
			printk("\t%s-> %w/dev/pci%d%w%s%s %r> %w%d%r:%w%d%r, %kv:%w%#4x%r, %kd:%w%#4x%r, %kc:%w%#2x%r:%w%#2x %kh:%w%#2x%r", (func > 0) ? "\t" : "",	VC_Green, (bus * 32 + slot), VC_Violet, (func > 0 ? "f" : ""), (func > 0 ? (Library::Utility::ConvertToString(func)) : ((char*)"")), VC_Blue, bus, VC_Red, slot, VC_Yellow, VC_Silver, vendor, VC_Cyan, VC_Blue, device, VC_Red, VC_Green, cl, VC_Silver, subclass, VC_Orange, VC_Violet, h);
		}

		bool PrintPCIDeviceInfo(PCI::PCIDevice* dev)
		{
			printk("\t%s-> %w/dev/pci%d%w%s%s %r> %w%d%r:%w%d%r, %kv:%w%#4x%r, %kd:%w%#4x%r, %kc:%w%#2x%r:%w%#2x %kh:%w%#2x%r", (dev->GetFunction() > 0) ? "\t" : "",	VC_Green, (dev->GetBus() * 32 + dev->GetSlot()), VC_Violet, (dev->GetFunction() > 0 ? "f" : ""), (dev->GetFunction() > 0 ? (Library::Utility::ConvertToString(dev->GetFunction())) : ((char*)"")), VC_Blue, dev->GetBus(), VC_Red, dev->GetSlot(), VC_Yellow, VC_Silver, dev->GetVendorID(), VC_Cyan, VC_Blue, dev->GetDeviceID(), VC_Red, VC_Green, dev->GetClass(), VC_Silver, dev->GetSubclass(), VC_Orange, VC_Violet, dev->GetHeaderType());

			return dev->GetIsMultifunction();
		}

		void PrintKernelMemoryMap()
		{
			// We do this so we can verify that our kernel memory map is good after copying it.
			uint16_t i = 0;
			while(i < K_MemoryMap->NumberOfEntries)
			{
				printk("%w\n\t->%w %16x%r to%w %16x %r->%w Type %d ", VC_White, VC_Yellow, K_MemoryMap->Entries[i].BaseAddress,
					VC_Green, K_MemoryMap->Entries[i].BaseAddress + K_MemoryMap->Entries[i].Length, VC_Violet, K_MemoryMap->Entries[i].MemoryType);


				switch(K_MemoryMap->Entries[i].MemoryType)
				{
					case 1:
					LFB::Console::SetColour(VC_Green);
					break;

					case 2:
					LFB::Console::SetColour(VC_Red);
					break;

					case 3:
					LFB::Console::SetColour(VC_Yellow);
					break;

					case 4:
					LFB::Console::SetColour(VC_Orange);
					break;

					case 5:
					LFB::Console::SetColour(VC_Red);
					break;
				}
				printk(" %s", K_MemoryTypes[K_MemoryMap->Entries[i].MemoryType]);
				LFB::Console::SetColour(VC_White);

				i++;
			}
		}

		void DoMultiboot1MemoryMap(Multiboot::Info_type* MBTStruct)
		{
			uint64_t TotalMapAddress = (uint64_t)MBTStruct->mmap_addr;
			uint64_t TotalMapLength = MBTStruct->mmap_length;

			K_MemoryMap->SizeOfThisStructure = sizeof(uint32_t) + sizeof(uint16_t);
			K_MemoryMap->NumberOfEntries = 0;


			// Check if the fields are valid:
			if(!(MBTStruct->flags & (1 << 6)))
			{
				printk("FLAGS: %d", MBTStruct->flags);
				HALT(ERR_NoMemoryMap);
			}

			Multiboot::MemoryMap_type *mmap = (Multiboot::MemoryMap_type*)TotalMapAddress;
			while((uint64_t)mmap < (uint64_t)(TotalMapAddress + TotalMapLength))
			{

				(K_MemoryMap->Entries[K_MemoryMap->NumberOfEntries]).BaseAddress = (uint64_t)mmap->BaseAddr_Low | (uint64_t)(mmap->BaseAddr_High) << 32;
				(K_MemoryMap->Entries[K_MemoryMap->NumberOfEntries]).Length = (uint64_t)(mmap->Length_Low | (uint64_t)(mmap->Length_High) << 32);
				(K_MemoryMap->Entries[K_MemoryMap->NumberOfEntries]).MemoryType = mmap->Type;
				K_MemoryMap->SizeOfThisStructure += sizeof(MemoryMapEntry_type);



				switch(K_MemoryMap->Entries[K_MemoryMap->NumberOfEntries].MemoryType)
				{
					case 1:
					case 3:
					K_SystemMemoryInBytes += (K_MemoryMap->Entries[K_MemoryMap->NumberOfEntries].Length);
					break;
				}


				K_MemoryMap->NumberOfEntries++;
				mmap = (Multiboot::MemoryMap_type*)((uint64_t)mmap + mmap->Size + sizeof(uint32_t));
			}
		}

		void PrintVersion()
		{
			printk("%wOrion-X3%r Version %w%s%r.%w%s%r.%w%s%r -- Build %w%d", VC_Chartreuse, VC_Orange, VER_MAJOR, VC_Violet, VER_MINOR, VC_Rose, VER_REVSN, VC_Cyan, (uint64_t)&X_BUILD_NUMBER);
		}

		void WriteStringToAddr(const char* str, uint64_t StartingAddr)
		{
			for(int i = 0, k = 0; i < String::Length(str) * 2; i += 2, k++)
			{
				memset((uint8_t*)StartingAddr + i, str[k], 1);
				memset((uint8_t*)StartingAddr + i + 1, 0xF, 1);
			}
		}
	}
}

















































































































































































