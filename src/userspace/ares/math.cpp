// ares/math.cpp
// Copyright (c) 2011-2013, <zhiayang@gmail.com>
// Licensed under Creative Commons Attribution 3.0 Unported.


#include <math.h>
#include <utility.h>

namespace Math
{
	double Power(double num, double pow)
	{
		uint64_t ret = 1;

		if(pow == 0)
			return 1;

		for(; pow > 0; pow--)
		{
			ret *= num;
		}
		return ret;
	}

	int64_t Round(double number)
	{
		return (number >= 0) ? (uint64_t)(number + 0.5) : (uint64_t)(number - 0.5);
	}

	double AbsoluteValue(double num)
	{
		if(num < 0)
			num = -num;

		return num;
	}

	uint64_t Log2(uint64_t num)
	{
		uint64_t ret = 0;
		while(num >>= 1)
		{
			ret++;
		}
		return ret;
	}

	double SquareRoot(double num, uint8_t iterations)
	{
		if(num < 0)
			return -1;

		else if(num == 0)
			return 0;

		double x = num, newx = x, prevx = x;


		for(int i = 0; i < iterations; i++)
		{
			prevx = x;
			x = newx;
			newx = 0.5 * (x + (num / x));


			if(prevx == newx)
				return newx;
		}

		return newx;
	}
}







