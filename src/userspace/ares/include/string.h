// ares/string.h
// Copyright (c) 2011-2013, <zhiayang@gmail.com>
// Licensed under Creative Commons Attribution 3.0 Unported.




#include <stdio.h>
#pragma once


namespace String
{
	uint64_t Length(const char* string);
	char* Copy(char* dest, const char* src);
	char* CopyLength(char* dest, const char* src, uint64_t length);

	char* Concatenate(char* dest, const char* src);
	char* ConcatenateLength(char* dest, const char* src, uint64_t n);

	bool Compare(const char* a, const char* b);
	char* Reverse(char* str, uint64_t length);
}
