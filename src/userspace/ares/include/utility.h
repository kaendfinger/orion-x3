// ares/utility.h
// Copyright (c) 2011-2013, <zhiayang@gmail.com>
// Licensed under Creative Commons Attribution 3.0 Unported.

#include <stdio.h>
#include <math.h>
#pragma once


namespace Utility
{
	int64_t ParseInteger(const char* str, char** endptr, int base);
	char* ConvertToString(int64_t num);
	int64_t ConvertToInt(char* str, uint8_t base);
}



































