// ares/math.h
// Copyright (c) 2011-2013, <zhiayang@gmail.com>
// Licensed under Creative Commons Attribution 3.0 Unported.

#pragma once
#include <stdio.h>

#define __HI(x) *(1 + (int*)&x)
#define __LO(x) *(int*)&x
#define __HIp(x) *(1 + (int*)x)
#define __LOp(x) *(int*)x

namespace Math
{
	double Power(double num, double pow);
	int64_t Round(double number);
	double AbsoluteValue(double num);
	uint64_t Log2(uint64_t num);
	double Sine(double Degrees);
	double SquareRoot(double Number, uint8_t iterations = 100);
}







