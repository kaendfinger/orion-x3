// ares/stdio.cpp
// Copyright (c) 2011-2013, <zhiayang@gmail.com>
// Licensed under Creative Commons Attribution 3.0 Unported.



#include <stdio.h>
#include <string.h>
#include <math.h>
#include <utility.h>

namespace StdIO
{
	void PrintString(const char *string, int64_t length)
	{
		for(int i = 0; i < String::Length(string); i++)
		{
			PrintChar(string[i]);
		}
	}

	void PrintF(const char* string, ...)
	{

	}

	void PrintChar(const char c)
	{
		asm volatile("movq $0x02, %%r10; movq %0, %%rdi; int $0xAE" :: "m"(c) : "%r10", "%rdi", "%rbx");
	}

	namespace Internal
	{
		void PrintHex_NoPrefix(uint64_t n, bool ReverseEndianness)
		{
			int tmp;

			int i = 0;


			// Mask bits of variables to determine size, and therefore how many digits to print
			if((n & 0xF000000000000000) == 0)
				if((n & 0xFF00000000000000) == 0)
					if((n & 0xFFF0000000000000) == 0)
						if((n & 0xFFFF000000000000) == 0)
							if((n & 0xFFFFF00000000000) == 0)
								if((n & 0xFFFFFF0000000000) == 0)
									if((n & 0xFFFFFFF000000000) == 0)
										if((n & 0xFFFFFFFF00000000) == 0)
											if((n & 0xFFFFFFFFF0000000) == 0)
												if((n & 0xFFFFFFFFFF000000) == 0)
													if((n & 0xFFFFFFFFFFF00000) == 0)
														if((n & 0xFFFFFFFFFFFF0000) == 0)
															if((n & 0xFFFFFFFFFFFFF000) == 0)
																if((n & 0xFFFFFFFFFFFFFF00) == 0)
																	if((n & 0xFFFFFFFFFFFFFFF0) == 0)
																		i = 0;
																	else
																		i = 4;
																else
																	i = 8;
															else
																i = 12;
														else
															i = 16;
													else
														i = 20;
												else
													i = 24;
											else
												i = 28;
										else
											i = 32;
									else
										i = 36;
								else
									i = 40;
							else
								i = 44;
						else
							i = 48;
					else
						i = 52;
				else
					i = 56;
			else
				i = 60;


			if(!ReverseEndianness)
			{
				for(; i >= 0; i -= 4)
				{
					tmp = (n >> i) & 0xF;


					if(tmp >= 0xA)
						PrintChar(tmp - 0xA + 'A');

					else
						PrintChar(tmp + '0');
				}
			}
			else
			{
				for(int z = 0; z <= i; z += 8)
				{
					tmp = (n >> z) & 0xFF;
					PrintHex_NoPrefix(tmp);
				}
			}
		}

		void PrintHex_Signed_NoPrefix(int64_t n)
		{
			// TODO: Print signed hex.
			PrintHex_NoPrefix(n);
		}



		void PrintHex(uint64_t n)
		{
			PrintString("0x");
			PrintHex_NoPrefix(n);
		}

		void PrintHex_Precision_NoPrefix(uint64_t n, int8_t leadingzeroes, bool ReverseEndianness)
		{
			if(leadingzeroes < 0)
			{
				return PrintHex_NoPrefix(n);
			}

			int tmp;


			int i = (leadingzeroes * 4) - 4;

			if(!ReverseEndianness)
			{
				for(; i >= 0; i -= 4)
				{
					tmp = (n >> i) & 0xF;

					if(tmp >= 0xA)
						PrintChar(tmp - 0xA + 'A');

					else
						PrintChar(tmp + '0');
				}
			}
			else
			{
				for(int z = 0; z <= i; z += 8)
				{
					tmp = (n >> z) & 0xFF;
					PrintHex_NoPrefix(tmp);
				}
			}
		}

		void PrintHex_Precision(uint64_t n, uint8_t leadingzeroes)
		{
			PrintString("0x");
			PrintHex_Precision_NoPrefix(n, leadingzeroes);
		}









		void PrintInteger(uint64_t num, int8_t Width)
		{
			PrintInteger_Signed(num, Width);
		}

		void PrintInteger_Signed(int64_t num, int8_t Width)
		{
			if(num == 0)
			{
				if(Width != -1)
				{
					for(int g = 0; g < Width; g++)
						PrintString("0");
				}
				else
					PrintString("0");

				return;
			}

			if(Width != -1)
			{
				uint64_t n = Math::AbsoluteValue(num);
				uint8_t k = 0;
				while(n > 0)
				{
					n /= 10;
					k++;
				}

				while(Width > k)
				{
					PrintString("0");
					k++;
				}
			}
			PrintString(Utility::ConvertToString(num));
		}

		void PrintBinary(uint64_t n)
		{
			// Number of bits to print.
			// Similiar 'tech' to puthex.

			unsigned int i = 0;
			if((n & 0xFFFFFFFF00000000) == 0)
			{
				if((n & 0xFFFF0000) == 0)
				{
					if((n & 0xFF00) == 0)
					{
						if((n & 0xF0) == 0)
						{
							i = 4;
						}
						else
						{
							i = 8;
						}
					}
					else
					{
						i = 16;
					}
				}
				else
				{
					i = 32;
				}
			}
			else
			{
				i = 64;
			}


			uint32_t bits[64];

			// Populate with invalid values (neither one nor zero) to tell us when to start.
			for(int f = 0; f < (64 - i); f++)
			{
				bits[f] = 0;
			}

			for(int f = i; f > 0; f--)
			{
				bits[f] = (n & (1 << f));
			}

			for(int f = i - 1; f >= 0; f--)
			{
				if(bits[f] == 0)
					PrintChar('0');

				else
					PrintChar('1');
			}
		}


		uint8_t PrintFloat(double fl, int8_t precision)
		{
			if(precision < 0)
			{
				precision = 15;
			}

			// Put integer part first
			PrintInteger_Signed((uint64_t)fl);

			if((uint64_t)fl == fl)
				return 0;

			PrintChar('.');

			// Get decimal part
			fl -= (uint64_t)fl;

			// return 0;

			uint32_t digit = 0;
			while(fl > 0 && precision > 0)
			{
				fl *= 10;

				if(precision == 1)
					digit = (uint32_t)Math::Round(fl);

				else
					digit = (uint32_t)fl;

				if(!(digit + '0' >= '0' && digit + '0' <= '9'))
				{
					PrintChar('0');
					return 0;
				}

				PrintChar(digit + '0');
				precision--;
				fl -= digit;
			}
			// Return the remaining number -- handle in printk() to print trailing zeroes
			return precision;
		}
	}
}
