# Documentation for Orion-X3 Subsystems

This file documents the design choices and subsystem functionality of Orion-X3.

Note:
> This file is not a definite reference
> for the implementation and functioning of various functions in the library.
> For a definite implementation specifics,
> Investigate src/kernel/lib, as well as stdlib.h and namespace.h

### Startup Sequence ###

Orion-X3 starts up in 2 phases.

1. The first phase initialises most low-level subsystems, including:
	- Memory map (from GRUB)
	- Base functions for IRQ and ISR handling
	- Serial logging (a mirror of console output) to COM1
	- The Paging tables (x64)
	- Physical and Dyanmic (heap) memory managers
	- PS/2 keyboard (starts diabled)
	- Enumerating of PCI devices (incomplete, but adequate at this stage)
	- BGA (Bochs Graphics Adapter) initialisation for video output
	- Bare-bones multithreading system
	- Reading the first partition on the first disk to get configuration options
>

2. The next stage is initialised as a thread running on the scheduler; task switches occur at 1kHz, or 1000 times a second, thereabouts. It is managed by the PIT timer. This stage initialises other things:
	- Identify ATA devices (up to 4 max, port I/O values are not from PCI)
	- Reading of MBR and GPT partitions
	- Initialisation of FAT32 (for now) filesystems.
>

3. The last part, while not exactly a 'phase' per-se, is simply a set of tests:
	- PrintK functionality
	- Memory allocation (physical pages)
	- Memory allocation (dyanmic chunks)


### Library Functions ###

Library functions are stored in the namespace Library.
They are further separated into several subtypes:

> - Types
> 	- List
> 	- String
> 	- Queue
> 	- Stack
>
> - Utility
> 	- int64_t ConvertToInt(str, base)
> 	- char* ConvertToString(n, base)
>
> - Math
> 	- uint64_t Power(num, pow)
> 	- int64_t Round(number)
> 	- uint64_t AbsoluteValue(num)
> 	- Log2(num)
>
> - Memory
> 	- void* Set/32/64(dest, value, length)
> 	- void* Copy/32/64(dest, src, len)
> 	- void* CopyOverlap/32/64(dst, src, n);
>
> - String
> 	- uint64_t Length(str);
> 	- char* Copy(dest, src);
> 	- char* Concatenate(dest, src);
> 	- char* ConcatenateLength(dest, src, n);
> 	- char* ConcatenateChar(dest, c);
> 	- bool Compare(a, b);
> 	- char* Reverse(str, length);
> 	- char* TrimWhitespace(str);
> 	- char* SubString(src, offset, length);
> 	- char* Clear(src);
> 	- char* Truncate(str, Length);
>
> - StandardIO
> 	- void PrintString(string, length);
> 	- void PrintHex_NoPrefix(n, REndian);
> 	- void PrintHex_Signed_NoPrefix(n);
> 	- void PrintHex(n);
> 	- void PrintHex_Precision_NoPrefix(n, lz, REndian);
> 	- void PrintHex_Precision(n, leadingzeroes);
> 	- void PrintInteger(num);
> 	- void PrintInteger_Signed(num);
> 	- void PrintBinary(n);
> 	- uint8_t PrintFloat(fl, precision);
> 	- void PrintGUID(High64, Low64);
> 	- void printk(string, ...);





