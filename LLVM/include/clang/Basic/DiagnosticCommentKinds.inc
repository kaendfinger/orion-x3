#ifdef COMMENTSTART
__COMMENTSTART = DIAG_START_COMMENT,
#undef COMMENTSTART
#endif

DIAG(note_doc_html_end_tag, CLASS_NOTE, diag::MAP_FATAL, "end tag", 0, true, false, false, false, 16)
DIAG(note_doc_html_tag_started_here, CLASS_NOTE, diag::MAP_FATAL, "HTML tag started here", 0, true, false, false, false, 16)
DIAG(note_doc_param_name_suggestion, CLASS_NOTE, diag::MAP_FATAL, "did you mean '%0'?", 0, true, false, false, false, 16)
DIAG(note_doc_param_previous, CLASS_NOTE, diag::MAP_FATAL, "previous documentation", 0, true, false, false, false, 16)
DIAG(warn_doc_block_command_empty_paragraph, CLASS_WARNING, diag::MAP_IGNORE, "empty paragraph passed to '\\%0' command", 90, true, false, false, false, 16)
DIAG(warn_doc_html_end_forbidden, CLASS_WARNING, diag::MAP_IGNORE, "HTML end tag '%0' is forbidden", 91, true, false, false, false, 16)
DIAG(warn_doc_html_end_unbalanced, CLASS_WARNING, diag::MAP_IGNORE, "HTML end tag does not match any start tag", 91, true, false, false, false, 16)
DIAG(warn_doc_html_start_end_mismatch, CLASS_WARNING, diag::MAP_IGNORE, "HTML start tag '%0' closed by '%1'", 91, true, false, false, false, 16)
DIAG(warn_doc_html_start_tag_expected_ident_or_greater, CLASS_WARNING, diag::MAP_IGNORE, "HTML start tag prematurely ended, expected attribute name or '>'", 90, true, false, false, false, 16)
DIAG(warn_doc_html_start_tag_expected_quoted_string, CLASS_WARNING, diag::MAP_IGNORE, "expected quoted string after equals sign", 90, true, false, false, false, 16)
DIAG(warn_doc_param_duplicate, CLASS_WARNING, diag::MAP_IGNORE, "parameter '%0' is already documented", 90, true, false, false, false, 16)
DIAG(warn_doc_param_invalid_direction, CLASS_WARNING, diag::MAP_IGNORE, "unrecognized parameter passing direction, valid directions are '[in]', '[out]' and '[in,out]'", 90, true, false, false, false, 16)
DIAG(warn_doc_param_not_attached_to_a_function_decl, CLASS_WARNING, diag::MAP_IGNORE, "'\\param' command used in a comment that is not attached to a function declaration", 90, true, false, false, false, 16)
DIAG(warn_doc_param_not_found, CLASS_WARNING, diag::MAP_IGNORE, "parameter '%0' not found in the function declaration", 90, true, false, false, false, 16)
DIAG(warn_doc_param_spaces_in_direction, CLASS_WARNING, diag::MAP_IGNORE, "whitespace is not allowed in parameter passing direction", 92, true, false, false, false, 16)
